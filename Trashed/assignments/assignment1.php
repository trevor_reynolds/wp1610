<?php
  $counter=1;
  $slideText='Assignment 1.';
?>
<article id='slides-assignment-1'>
  <section class=key>
    <header>WP Assignment 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>1. Instructions</h1>
      <p>Students are asked to work individually for this assignment, but are encouraged to seek help from classmates and other tutors. It is designed to be a short "confidence building" assignment to help you get set up  on the core teaching servers and make sure assignments 2 and 3 go smoothly.</p>
      <p>Your website resources should be available from the following links</p>
      <ul>
        <li><?php a('https://titan.csit.rmit.edu.au/~'.SID.'/wp/index.html'); ?></li>
        <li><?php a('https://titan.csit.rmit.edu.au/~'.SID.'/wp/photo.jpg'); ?></li>
        <li><?php a('https://titan.csit.rmit.edu.au/~'.SID.'/wp/a2/index.php'); ?></li>
        <li><?php a('https://titan.csit.rmit.edu.au/~'.SID.'/wp/a3/index.php'); ?></li>
      </ul>
      <blockquote>An official submission must be made at the start of <b>week 3 via Blackboard:<br>
        Monday 3rd August 2015, 5pm.</b><br>
        <br>
        Just the modified home page in lab 1 needs to be submitted,<br>
        your online version will be assessed once the submission has been made.<br>
        <br>
        <b>If you complete the assignment ahead of time, your lab tutors will mark you on the spot,<br>
        leaving you free to work on other assignments.</b>
      </blockquote>
    </section>
  </section>
  <section>
    <header>WP Assignment 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>2. Overview</h1>
      <p>In the first tute lab, you are asked to</p>
      <ol>
        <li>Connect to your account on the coreteaching servers</li>
        <li>Create public_html, wp and two assignment directories (a2 and a3)</li>
        <li>Give the above directories and your home directory correct permissions (ie 701)</li>
        <li>Download a starting HTML webpage and personalise the content, telling us a bit about yourself, your background and experience level in programming (particularly web programming), and what has brought you to RMIT</li>
        <li>Embed a photo of you that has been physically sized to 200px by 200px into the above page</li>
        <li>Create holding PHP pages for each of the assignment directories</li>
        <li>Give the above files correct permissions (ie 604)</li>
      </ol>
      <blockquote>When you do these things, Assignment 1 is complete!</blockquote>
    </section>
  </section>
  <section>
    <header>WP Assignment 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>3. Site Layout Diagram</h1>
      <figure>
        <img src='app/img/coreteaching.png' width='100%' alt='Coreteaching Directory Hierarchy' />
        <figcaption>Your web directory on Coreteching servers</figcaption>
      </figure>
    </section>
  </section>
  <section>
    <header>WP Assignment 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>4. Marks Allocation</h1>
      <p><b>Please read the first lab sheet carefully!</b></p>
      <p>For all marks to be awarded, page content must be well written, files must be correctly named and made publically viewable, the image must be <u>physically</u> the correct size (200px by 200px).</p>
      <table>
        <tr>
          <th>Website Structure</th><th>3 Marks</th>
        </tr><tr>
          <td>All directories and HTML home page created</td><td>1 mark</td>
        </tr><tr>
          <td>Two PHP holding pages (with a small message) in a2 and a3 directories</td><td>1 mark</td>
        </tr><tr>
          <td>All 3 files viewable at correct urls, ie permissions correct, files named correctly</td><td>1 mark</td>
        </tr><tr>
          <th>Page content</th><th>2 Marks</th>
        </tr><tr>
          <td>Homepage content is well written, student's background and experience is clear.</td><td>1 mark</td>
        </tr><tr>
          <td>Image named and embedded correctly, size is 200px by 200px</td><td>1 mark</td>
        </tr><tr>
          <th colspan=2 style='text-align:right'>Total: 5 marks (or 5% of your final grade)</th>
        </tr>
      </table>
    </section>
  </section>
</article>