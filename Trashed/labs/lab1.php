<?php
  $counter=1;
  $slideText='Lab 1, P.';
?>
<article id='slides-lab-1'>
  <section class='key'>
    <header>Lab 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Activities</h1>
      <ol>
        <li>Safety Procedures</li>
        <li>Introduction to the Lab</li>
        <li>Connecting to Coreteaching Servers (UNIX)</li>
        <li>Connecting to Coreteaching Servers</li>
        <li>FTP and HTTP Connections Diagram</li>
        <li>External Resources</li>
      </ol>
    </section>
  </section>
  
  <section>
    <header>Safety Procedures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>Each room is different at RMIT. Ask your tutor how to get out of the building in an emergency.</p>
    </section>
  </section>
  
  <section>
    <header>Introduction to the Lab<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>Please note: These exercises assume you have a coreteaching account, that you can log into coreteaching using your NDS password and that you have access to your student home directory.</p>
      <p>Your tutor should be able to help you if you run into difficulties, but if your account has not been set up correctly, you may have to contact ITS on 9925 8888, or from any landline at RMIT on 58888.</p>
      <p>If your account doesn’t get set up today, consider helping another student set up their account and take notes. It won’t take you long to repeat those same steps once your account is set up.</p>
      <p>For all other helpdesk enquiries, use the ITS webform: https://mytechsupport.rmit.edu.au/</p> 
    </section>
  </section>
  
  <section>
    <header>Connecting to Coreteaching Servers (UNIX)<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>Connect using PuTTY or Terminal etc, create directories outlined in the tute diagram using the <strong>mkdir</strong> command. Give them <strong>and your home directory</strong>  executable <strong>701</strong> permission (rwx-----x) using the <strong>chmod</strong> command.</p>
      <ul>
        <li><kbd>Text to enter is shown in strong yellow</kbd></li>
        <li><kbd><span>Text the computer prints is shown in yellow-grey</span></kbd></li>
      </ul>
      <pre><kbd><span>$</span> ssh <?php echo SID; ?>@titan.rmit.edu.au
<span><?php echo SID; ?>@titan.csit.rmit.edu.au's password:</span>_

<span>Last login: Thu Xxx xx xx:xx:xx xx from csitprdapXX.int.its.rmit.edu.au

                  *** PLEASE READ CAREFULLY ***
       ***** This service is for authorised users only *****

   Individuals using this computer system without authority, or in
   excess of their authority, 

   ... BLAH BLAH BLAH ...

*************************************************************************
*  WARNING: It is a criminal offence to:                                *
*       i.  Obtain access to data without authority                     *
*       ii. Damage, delete, alter or insert data without authority      *
*************************************************************************

   ... BLAH BLAH BLAH ...</span>


<span>$</span> mkdir public_html
<span>$</span> mkdir public_html/wp
<span>$</span> mkdir public_html/wp/a2
<span>$</span> mkdir public_html/wp/a3
<span>$</span> chmod 701 ~
<span>$</span> chmod 701 ~/public_html
<span>$</span> chmod 701 ~/public_html/wp
<span>$</span> chmod 701 ~/public_html/wp/a2
<span>$</span> chmod 701 ~/public_html/wp/a3</kbd></pre>
      <div class='cb'></div>
    </section>
  </section>
  
  <section>
    <header>Connecting to Coreteaching Servers<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>In the labs, we will be using an FTP client called WinSCP to connect to the coreteaching server of your choice and edit your web pages. You can use either naming system: planets or "coreteaching##".</p>
      <p>Any files saved in the public_html folder will be visible, as long as you give the files and directories the appropriate permissions:</p>
      <ul>
        <li>Your directories: <?php echo SID; ?>, public_html, wp etc must have <strong>701</strong> permission</li>
        <li>Your files: HTML, image, css, php etc must have <strong>604</strong> permission</li>
      </ul>
      <p>Outside RMIT you can use WinSCP (PC) or CyberDuck (MAC) to connect. Google these names and you should find their download pages. Alternatively, you can use RMIT's myDesktop and connect to a virtual windows desktop environment as if you were at RMIT.</p>
      <p><?php a('https://mydesktop.rmit.edu.au/vpn/index.html'); ?></p> 
      <p>PC users can download a copy of PuTTY to use at home which gives a unix terminal interface.</p>
      <p>Mac Users can connect to coreteaching using the built in terminal.app program.</p>
      <p>Linux Users (GUI) can connect directly to a remote location using the file manager and have a folder on their desktop. One Youtube video showing you how can be found here: <?php a('https://www.youtube.com/watch?v=tvyslako_60'); ?></p>
    </section>
  </section>
  
  <section>
    <header>FTP and HTTP Connections Diagram<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>Your tutor will step you through the following diagram, which you will need for the lab this week:</p>
      <p><img class='w1' src='app/img/coreteaching.png' alt='Diagram showing FTP and HTTP connections to a web server' /></p>
    </section>
  </section>

  <section>
    <header>External Resources<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>External resources, all free at time of writing:</p>
      <ul>
        <li>w3schools.com: excellent website for beginners</li>
        <li>w3.org: official standards for web documents</li>
        <li>Free code editors: Notepad++ (RMIT/PC), Brackets (MAC/PC)</li> 
        <li>Chinese language friendly editor D Builder: http://dcloud.io/</li>
        <li>FTP Clients: Filezilla (RMIT/MAC/PC), WinSCP (RMIT/PC), Cyberduck (MAC)</li> 
        <li>gimp.org: Free image editing software (RMIT/MAC/PC)</li>
        <li>apachefriends.org: Set up a web server on your desktop, laptop etc</li>
        <li>bitbucket.org: Back up your work and track changes with a repository.</li>
        <li>atlassian.com: A GUI for your repository (above).</li>
      </ul>
    </section>
  </section>

  
</article>