<?php
  $counter=1;
  $slideText='Tute 1, P.';
?>
<article id='slides-tute-1'>
  <section class='key'>
    <header>Tutorial 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Activities</h1>
      <ol>
        <li>Safety Procedures</li>
        <li>Writing Copy for your First Page</li>
        <li>Hosting Webpages on Coreteaching Servers</li>
        <li>Connecting to Coreteaching Servers</li>
        <li>FTP and HTTP Connections Diagram</li>
        <li>External Resources</li>
      </ol>
    </section>
  </section>
  
  <section>
    <header>Safety Procedures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>Each room is different at RMIT. Ask your tutor how to get out of the building in an emergency.</p>
    </section>
  </section>
  <section>
    <header>Writing Copy for your First Page<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>In the lab today, you will be downloading a very basic HTML5 web page template that you will modify with some information about yourself. Later on, in your main assignments, you will create a link to this page with the words "Website design by …". </p>
      <p>Some things to think about:</p>
      <ul>
        <li>How do you use the internet?</li>
        <li>What websites do you use? (NB: "family friendly" !)</li>
        <li>Have you any programming or web design experience?</li>
        <li>Do you see yourself as a programmer or designer, or both?</li>
        <li>What has brought you to this course at RMIT?</li>
        <li>What are you hoping to get out of the course?</li>
        <li>Do you have any interests or hobbies?</li>
      </ul>
      <p>Work with one or two others to flesh out your copy.</p>
      <p>This exercise is to get to know your classmates and yourself a little better. By the end of this exercise you should have some idea of what to put in your personal webpage.</p>
      <p>For administrative reasons, there will be a few other fields to fill in (name, student number, a personal photo, your tutorial and lab times etc). This will help the entire web programming team get to know you better.</p>
    </section>
  </section>
  
  <section>
    <header>Hosting Webpages on Coreteaching Servers<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p><img class='fl w25' src='app/img/CSIT%20Overview%20Rear.jpg' alt='Rear view of the coreteaching servers'/> The Coreteaching servers are real machines. They live at RMIT's data centre in the city campus.</p>
      <p>All coreteaching servers store your files on a common fileserver, so changes made via one machine will ripple through to other machines. It doesn't matter which machine you use to view your webpages, you will be able to see the same material from the following urls:</p>
      <li><?php a('https://jupiter.csit.rmit.edu.au/~'.SID.'/wp/'); ?> {your files}</li>
      <li><?php a('https://saturn.csit.rmit.edu.au/~'.SID.'/wp/'); ?> {your files}</li>
      <li><?php a('https://titan.csit.rmit.edu.au/~'.SID.'/wp/'); ?> {your files}</li>
      <p><img class='fl w50' src='app/img/CoreTeaching-Structure.png' alt='Tree diagram of file server connected to the web server' /></p>   
      <div class='cb'></div>
    </section>
  </section>
  
  <section>
    <header>Connecting to Coreteaching Servers<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>In the labs, we will be using an FTP client called WinSCP to connect to the coreteaching server of your choice and edit your web pages. You can use either naming system: planets or "coreteaching##".</p>
      <p>Any files saved in the public_html folder will be visible, as long as you give the files and directories the appropriate permissions:</p>
      <ul>
        <li>Your directories: <?php echo SID; ?>, public_html, wp etc must have <strong>701</strong> permission</li>
        <li>Your files: HTML, image, css, php etc must have <strong>604</strong> permission</li>
      </ul>
      <p>Outside RMIT you can use WinSCP (PC) or CyberDuck (MAC) to connect. Google these names and you should find their download pages. Alternatively, you can use RMIT's myDesktop and connect to a virtual windows desktop environment as if you were at RMIT.</p>
      <p><?php a('https://mydesktop.rmit.edu.au/vpn/index.html'); ?></p> 
      <p>PC users can download a copy of PuTTY to use at home which gives a unix terminal interface.</p>
      <p>Mac Users can connect to coreteaching using the built in terminal.app program.</p>
      <p>Linux Users (GUI) can connect directly to a remote location using the file manager and have a folder on their desktop. One Youtube video showing you how can be found here: <?php a('https://www.youtube.com/watch?v=tvyslako_60'); ?></p>
    </section>
  </section>
  
  <section>
    <header>FTP and HTTP Connections Diagram<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>Your tutor will step you through the following diagram, which you will need for the lab this week:</p>
      <p><img class='w1' src='app/img/coreteaching.png' alt='Diagram showing FTP and HTTP connections to a web server' /></p>
    </section>
  </section>

  <section>
    <header>External Resources<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1></h1>
      <p>External resources, all free at time of writing:</p>
      <ul>
        <li>w3schools.com: excellent website for beginners</li>
        <li>w3.org: official standards for web documents</li>
        <li>Free code editors: Notepad++ (RMIT/PC), Brackets (MAC/PC)</li> 
        <li>Chinese language friendly editor D Builder: http://dcloud.io/</li>
        <li>FTP Clients: Filezilla (RMIT/MAC/PC), WinSCP (RMIT/PC), Cyberduck (MAC)</li> 
        <li>gimp.org: Free image editing software (RMIT/MAC/PC)</li>
        <li>apachefriends.org: Set up a web server on your desktop, laptop etc</li>
        <li>bitbucket.org: Back up your work and track changes with a repository.</li>
        <li>atlassian.com: A GUI for your repository (above).</li>
      </ul>
    </section>
  </section>

  
</article>