/* Lecture 2: Multimedia, Canvas 
   Simulates a very crude paint program */
function canvasPaint(canvas,event) {
  var rect = canvas.getBoundingClientRect();
  var left = event.clientX - rect.left;
  var top = event.clientY - rect.top;
  var ctx = event.target.getContext("2d");
  var date = new Date();
  var hue = Math.round(date.getTime()/60)%360;
  var size = 2 + Math.round(date.getTime()/60)%11;
  console.log("size: %s hue: %s at (%d,%d)",size,hue,left,top);
  ctx.fillStyle = "hsl("+hue+",50%,50%)";
  ctx.beginPath();
  ctx.arc(left,top,size,0,2*Math.PI);
  ctx.fill();
}

/* Lecture 2: meter and progress */
var mpIX=0;
function mpTick() {
  if (++mpIX > 10) mpIX=0;
  document.getElementById('l2-meter').value=mpIX;
  document.getElementById('l2-progress').value=mpIX;
}
setInterval(mpTick, 1000);

/* Lecture 3: Color, Pixel Simulator 
   Simulates #RGB 4,092 colors, pixel and screen */
function pixelSimulator()
{
  var rNibble=Number($('#slider-r').val())
  var gNibble=Number($('#slider-g').val());
  var bNibble=Number($('#slider-b').val());
  var bright=rNibble+gNibble+bNibble;
  var rNibble=rNibble.toString(16).toUpperCase();
  var gNibble=gNibble.toString(16).toUpperCase();
  var bNibble=bNibble.toString(16).toUpperCase();
  var rColor='#'+rNibble+'00';
  var gColor='#0'+gNibble+'0';
  var bColor='#00'+bNibble;
  var rgbColor="#"+rNibble+gNibble+bNibble;
  //console.log('Color #%s%s%s Brightness #s',rNibble,gNibble,bNibble,bright);
  $('#pixel-r').css('background-color',rColor);
  $('#pixel-r').html(rColor);
  $('#pixel-g').css('background-color',gColor);
  $('#pixel-g').html(gColor);
  $('#pixel-b').css('background-color',bColor);
  $('#pixel-b').html(bColor);
  $('#pixel-rgb').css('background-color',rgbColor);
  $('#pixel-rgb').html(rgbColor);
  if (bright>24)
    $('#pixel-rgb').css('color','black');
  else
    $('#pixel-rgb').css('color','white');
}

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Converts h, s, and l from css hsl() into the set [0, 1] and
 * calculates r, g, and b in the set [0, 255].
 *
 */
function hsl2rgb(){
  //console.log('enter');
  var r, g, b, rgb, rColor, gColor, bColor, rgbColor;
  var h = $('#slider-h').val();
  var s = $('#slider-s').val();
  var l = $('#slider-l').val();
  hslColor='hsl('+h+', '+s+'%, '+l+'%)';
  var bright = (
    (h > 35 && h < 195) ? -15 : (
    (h > 210 && h < 285) ? 15 : 0   
  ));
  if (l>(50+bright)) 
    $('#pixel-hsl').css('color','black');
  else 
    $('#pixel-hsl').css('color','white');
  $('#pixel-hsl').html(hslColor);
  $('#pixel-hsl').css('background-color',hslColor);
  h = h/360;
  s = s/100;
  l = l/100;
  // console.log('HSL #%s,%s,%s',h,s,l);

  if(s == 0)
  {
    r = g = b = Math.round(l*255); // achromatic
  }
  else
  {
    var hue2rgb = function hue2rgb(p, q, t) {
      if(t < 0) t += 1;
      if(t > 1) t -= 1;
      var ret = (
        t < 1/6 ? p + (q - p) * 6 * t : (
        t < 1/2 ? q : (
        t < 2/3 ? p + (q - p) * (2/3 - t) * 6 : p
      )));
      return Math.round(ret*255);
    }
    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }
    rColor='rgb('+r+',0,0)';
    gColor='rgb(0,'+g+',0)';
    bColor='rgb(0,0,'+b+')';
    $('#pixel-r2').html(rColor);
    $('#pixel-g2').html(gColor);
    $('#pixel-b2').html(bColor);
    $('#pixel-r2').css('background-color',rColor);
    $('#pixel-g2').css('background-color',gColor);
    $('#pixel-b2').css('background-color',bColor);
}

/* Lecture 4: CSS Layout */
function toggleCenter(bP)
{  
  if (bP) {
    $('#demoboxmodel5').css('margin-left','auto');
    $('#demoboxmodel5').css('margin-right','auto');
  } else {
    $('#demoboxmodel5').css('margin-left','');
    $('#demoboxmodel5').css('margin-right','');    
  }
}

/* Lecture 6: Event Handling */
function inspectEvent() { 
  alert('Event of type "'+event.type+'" detected:\n- '+event.target.id+' (originating element)\n- '+event.currentTarget.id+' (calling element)\nView the console for more information');
  console.log(event);
}

/* Lecture 6: Case Block Demo */
function whichDay(thisP) { 
  var dayName=thisP.value;
  switch ( dayName ) {
    case "Monday":
    case "Tuesday":
    case "Wednesday":
    case "Thursday":
    case "Friday":
      document.getElementById('thisDayL6').innerHTML='Weekday';
      break; 
    case "Saturday":
    case "Sunday":
      document.getElementById('thisDayL6').innerHTML='Weekend';
      break; 
    default:
      document.getElementById('thisDayL6').innerHTML='Invalid';
  }
}


/* Lecture 7: Event Handling */
function calculatePrice() { 
  alert('6 items at $9 each is $42 ... in base 13');
}
function calculatePrice2() { 
  alert('6 items at $9 each is $54 ... in base 10');
}
/*
function bubbleUp() {
  inspectEvent(this,event);
  //$('#ef-div').append('<div class=pd>'+JSON.stringify(eP,replacer)+'</div>');
  // alert (JSON.stringify(eP,replacer));
  // eP.stopImmediatePropagation();
}
 taken from http://stackoverflow.com/questions/20256760/javascript-console-log-to-html
(function () {
    var old = console.log;
    var logger = document.getElementById('ef-div');
    console.log = function () {
      for (var i = 0; i < arguments.length; i++) {
        if (typeof arguments[i] == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
        } else {
            logger.innerHTML += arguments[i] + '<br />';
        }
      }
    }
})(); 
// Taken from http://www.json.org/js.html
function replacer(key, value) {
  if (typeof value != 'object') {
    return String(value);
  }
  return value;
}*/
var myButton = document.getElementById("price3");
document.getElementById("price2").onclick = calculatePrice2;
document.getElementById("price3").addEventListener("click", calculatePrice); 
document.getElementById("price3").addEventListener("click", calculatePrice2); 

/* Lecture 7: Event Bubbling */
document.getElementById("ef-div").addEventListener("click", inspectEvent); 
document.getElementById("ef-span").addEventListener("click", inspectEvent); 
 
document.getElementById("cuteKitten").addEventListener("wheel", function(eP) {
  document.getElementById("purr").play();
  eP.preventDefault();
});

/* Lecture 7: Regex */
function isSteve(thisP) {
  var patt = /^Steve [a-zA-Z ]+$/;
  if (patt.test(thisP.value)) 
    document.getElementById('steveErrorL7').innerHTML='Hey '+thisP.value+'!';
  else
    document.getElementById('steveErrorL7').innerHTML='Please be a Steve';
}

function makeSteve(thisP) {
  var patt = /^(Stephen|Steven|Stephan|Stephanie)/;
  thisP.value = thisP.value.replace(patt,"Steve");
}

function pwStr(thisP) {
  var patt2 = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(.{8,})$/; //(?=.*[A-Z]))
  if (patt2.test(thisP.value)) 
    document.getElementById('pwErrorL7').innerHTML="That's a good one!";
  else
    document.getElementById('pwErrorL7').innerHTML='Needs to be stronger ...';
}
