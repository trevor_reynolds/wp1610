<?php
  error_reporting(0);
  function nameVal($req,$s='',$e='')
  {
    foreach ($req as $name => $value) 
      if ( gettype($value) == 'array' )
        nameVal($value,$s.$name.'[',$e.']');
      else
        echo "<p>&lt;element name='<span>$s$name$e</span>' has value='<span>$value</span>' &gt;</p>";
  }
?>

<!doctype html>
<html lang="en">
<head>
  <title>Form Processing</title>
  <style>
    html {
      background-image:url('img/card-back-grey.png');
      padding:0px;
      margin:0px;
      font-family:monospace;
      font-size:1.2em;
      color:#111;
      height:100%;
    }
    body {
      background-color:#fff;
      width:70%;
      min-height:90%;
      min-width:700px;
      margin:10px auto;
      padding:20px;
      box-shadow: 0 0 10px #000;
      border-radius: 10px;
    }
    a, p span {color: #c00; font-weight:bold;}
    h1 {text-align: center;}
    h1, h2, form {margin:10px;}
    p {margin-left:30px;}
  </style>
</head>
<body ondblclick="window.scrollTo(0,0);">
<div>

  <h1>&star; Web Programming Form Tester &star;</h1>
  <?php
    if ($_SERVER['HTTP_REFERER']!="")
      echo "<p>Data received from the following url:<br/><a href='{$_SERVER['HTTP_REFERER']}'>{$_SERVER['HTTP_REFERER']}</a></p>";
  ?>
  <hr/>

  <h2>&lt;form method='post' ... &gt;</h2>
  <?php
  if (count($_POST) > 0)
    nameVal($_POST);
  else
    echo "<p><span>Nothing has been submitted using the post method.</span></p>";
  ?>
  <h2>&lt;/form&gt;</h2>
  <hr/>

  <h2>&lt;form method='get' ... &gt;</h2>
  <?php
  if (count($_GET) > 0) {
    nameVal($_GET);
  }
  else
    echo "<p><span>Nothing has been submitted using the get method.</span>";
  ?>
  <h2>&lt;/form&gt;</h2>
  <hr/>
  <h2>FILES</h2>
  <?php
  if (!empty($_FILES)) {
    echo "We will look at how files are sent to the server in a later lecture.";
    print_r($_FILES);
  }
  else
    echo "<p><span>No files have been submitted.</span>";
  ?>
  <hr style="margin-bottom:50px;"/>
</body>
</html>

