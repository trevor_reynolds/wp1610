<?php
  class SlideCounter {
    public $title;
    public $count;
    public $delim;

    public function __construct($title='Slide ',$delim=' ',$count=1) {
      $this->title = $title;
      $this->delim = $delim;
      $this->count = $count;
    }

    public function tnc() {
      print $this->$title.' '.$count++;
    }
    
  }
?>
