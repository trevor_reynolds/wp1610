<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $pageTitle.'@'.HOST; ?></title>

  <link rel="icon" href="app/img/favicon.ico" type="image/x-icon">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600,600italic,400italic,300italic,300' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Carter+One' rel='stylesheet' type='text/css'>

<!-- Production: use local copies 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
-->
  <script src="app/js/jquery.min.js"></script>
  <script src="app/js/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="app/css/jquery-ui.css"> 
  <link rel="stylesheet" href="app/css/colorborder.css">
  <link rel="stylesheet" href="app/css/layout.css">
  <link rel="stylesheet" href="app/css/widgets.css">
  <link rel="stylesheet" href="app/css/print.css">
  <link rel="stylesheet" href="app/css/testing.css">

</head>
<body<?php echo ( isset($_GET['q']) ? " load='search(\"".$_GET['q']."\")'" : '' ); ?> >
  <header id='masthead'>
    <h1><img src='app/img/rmit-orb-ghost.png' alt='RMIT Orb, translucent, glowing like a legless jellyfish'/> <span>Web Programming - <?php echo NAME; ?></span></h1>
  </header>
  <nav>
    <div><span><label for="search">Search:</label> <input type='search' id='search' oninput='search(this.value);' pattern='(.{0}|.{2,})' value='<?php echo $_GET['q']; ?>' /></span><span><input type=checkbox id="instructions"<?php echo ($information[HOST]==1) ? ' checked' : ''; ?>/><label for="instructions">Instructions</label></span><span id='zoomSmall'>aA</span><input type="range" id='zoom' min="-4" max="10" oninput='fontResize(event);' value=0 title='1em' pattern='0'><span id='zoomBig'>aA</span></div>
    <details id='navdetail' >
      <summary>Select Modules</summary>
      <div><?php
        if (!empty($lectures[HOST]['load'])) menuModule($lectures,'lecture');
        if (!empty($tutes[HOST]['load']))    menuModule($tutes,'tute');
        if (!empty($labs[HOST]['load']))     menuModule($labs,'lab');
      ?></div>
    </details>
  </nav>
  <main>
    <div id='overlayTR'><div id='overlayContent'></div></div>