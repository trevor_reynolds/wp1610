<?php

/* *****

Definitions of CONSTANTS

This document sets up many of the "magic values" to be used later on in the app.

All definitions are made right down the bottom as some need to be calculated first depending on user, teacher status, and what server the app is running on.

PHP 7 allows arrays to be defined as constants, but RMIT is running an older version of PHP that doesn't support this, so I will put $ARRAY_CONSTANTs in caps so it is obvious that any are set up in this document.

***** */


// Where are we hosted? RMIT or Local?
  $host = (preg_match('/rmit\.edu\.au$/', $_SERVER['SERVER_NAME']  ) == 1 ? 'rmit' : 'local');

// 20160604: Bookmarking, local storage feature
  echo "<script>console.log(localStorage);</script>";

// Lectures released for RMIT service (ie coreteaching servers)
  $information['rmit'] = 0; // off
  $lectures['rmit']['load'] = array(1,2,3,4,5,6,7); //
  $tutes['rmit']['load'] = array();
  $labs['rmit']['load'] = array();
  $lectures['rmit']['show'] = array();
  $tutes['rmit']['show'] = array();
  $labs['rmit']['show'] = array();

// Lectures released for development (ie local host etc)
  $information['local'] = 0; // off
  $lectures['local']['load'] = array(1,2,3,4,5,6,7); // 
  $tutes['local']['load'] = array();
  $labs['local']['load'] = array();
  $lectures['local']['show'] = array();
  $tutes['local']['show'] = array();
  $labs['local']['show'] = array();


// Kludge: manually match up loaded modules with includes in index-view.php

// Extract user details from session courtesy of Kerberos Authentication
  $staff = array(
    'e04128' => 'Adrian',
    'e18123' => 'Ahmed',
    'e69042' => 'Bing',
    'e16545' => 'Byron',
    'e09755' => 'Daniel',
    'e18300' => 'Dong',
    'e09962' => 'Dushan',
    'e57478' => 'Falk',
    'e02764' => 'Gavan',
    'e13322' => 'Hai',
    'e62892' => 'IanB³',
    'e19560' => 'Jianxin',
    'e28355' => 'Khang',
    'e18569' => 'Lachlan',
    'e59474' => 'Laurence',
    'e87149' => 'Matthew',
    'e46991' => 'Shekhar',
    'e15309' => 'Sutina',
    'e17820' => 'Xiaolu',
    'e28883' => 'Tim B',
    'e54061' => 'Trevor',
    'e52483' => 'Ying',
    'e00000' => 'StaffDummy'
  );

// Set up UID and SID for students and staff.
  $uid = (empty($_SERVER['PHP_AUTH_USER']) ? 'e00000' : $_SERVER['PHP_AUTH_USER']);
  $sid = ''; $email=''; $name='';
  $isStaff=array_key_exists($uid, $staff);
  if ($isStaff)
  {
    $sid = 's1234567';
    $name = $staff[$uid]." ($uid)";
    $email= $uid.'@rmit.edu.au';
  }
  else
  {
    $sid = $uid;
    $name = $uid;
    $email= $uid.'@student.rmit.edu.au';
  }

/* All definitions go here! */
  define('HOST',$host);
  define('UID',$uid);
  define('SID',$sid);
  define('NAME',$name);
  define('IS_STAFF',$isStaff);
  define('EMAIL',$email);

  define('A1_DEADLINE','Monday xxth XXXX, 4am');
  define('A2_DEADLINE','Monday xxth XXXX, 4am');
  define('A3_DEADLINE','Monday xxth XXXX, 4am');

?>
