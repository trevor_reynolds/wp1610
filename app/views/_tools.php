<?php

// Load and Show modules
// 20160604: Develop local storage method instead
if (isset($_GET['localStorageReset']))
  echo "<script>localStorage.clear();</script>";

function menuModule($moduleP,$nameP)
{
  echo "<div class='buttonset'>".ucfirst($nameP)."s<br> ";
  foreach ($moduleP[HOST]['load'] as $i) {
    echo "<input type=\"checkbox\" id=\"$nameP-$i\"".(in_array($i,$moduleP[HOST]['show']) ? " checked" : "")." /><label for=\"$nameP-$i\">$i</label>";  
  }
}

// Testing: page=title LITE processing
  $pageTitles = array(
    'index'=>'Web Programming',
    'test'=>'Test'
  );
  $pageTitle=$pageTitles['index'];
  if (isset($_GET['page']) && array_key_exists($_GET['page'],$pageTitles))
    $pageTitle=$pageTitles[$_GET['page']];

// For future GET processing, different page rendering
  function safeArray($vP)
  {
    if (is_array($vP))
    {
      $ret;
      foreach ($vP as $key => &$v )
        $ret[htmlentities($key,ENT_QUOTES)]=safeArray($v);
      return $ret;
    }
    else if(is_bool($vP)||is_string($vP)||is_numeric($vP))
      return htmlentities($vP,ENT_QUOTES);
  }
  // $safeGet=safeArray($_GET);
  //echo "<pre>";
  //print_r($safeGet);
  //echo "</pre>";

// For testing of search feature
  function randomContent()
  {
    $content=array('horse','cow','duck','pig','turtle','aardvark');
    return($content[rand(0,count($content)-1)]);
  }

// prints a hyperlink that opens in a new window/tab with hyperlink as text
  function a($hrefP,$textP='')
  {
    if (empty($textP))
      $textP=$hrefP;
    print "<a href='$hrefP' target='_blank'>$textP</a>";
  }

// prints an email hyperlink that opens in a new window/tab with hyperlink as text
  function e($emailP,$textP='')
  {
    if (empty($textP))
      $textP=$emailP;
    print "<a href='mailto:$emailP?Subject=WP:%20'>$textP</a>";
  }

// prints an embedded iframe video, eg Youtube
  function youtube($urlP) {
    print
     "<div class='youtube'>
       <iframe src='$urlP' frameborder='0' allowfullscreen></iframe>
      </div>";
  }

// print code, html entities etc
  function h($sP)
  {
    print htmlentities($sP);
  }
  function he($sP)
  {
    print '<pre><code>'.htmlentities($sP).'</code></pre>';
  }
  function heb($sP)
  {
    print '<strong><code>'.htmlentities($sP).'</code></strong>';
  }
  function cb($sP)
  {
    print '<strong><code>'.$sP.'</code></strong>';
  }

?>