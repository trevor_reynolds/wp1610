<?php 
  $counter=1;
  $slideText='Assignment 3.';
?><article id='slides-assignment-3'>
  <section class=key>
    <header>Web Programming <span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h3>1. Instructions</h3>
      <p>Students are asked to work in pairs or teams of 3 for this assignment, unless you have permission from your tutor to work alone. We recommend teaming with someone who has complementary skills, eg programmers should team with designers.</p>
      <p>Assignment tasks should be discussed each week to a tutor/marker during tute-lab sessions so that your progress can be tracked and help can be given. Formative advice will be given to you each week during class time to maximise your skill development and help you achieve the best result.</p>
      <blockquote><b>Students must make the homepage available from this url:</b><br>
        <u>titan.csit.rmit.edu.au/~s1234567/wp/a3/</u> <b>(index.php)</b><br>
        <i>where <b>s1234567</b> is your student id and <b>titan</b> can also be <b>jupiter</b> or <b>saturn</b>,<br>
depending on which server you are using.</i><br>
        <br>
        An official submission must be made by the group leader<br>at the start of <b>week 11 via Blackboard:<br>
        Monday 9th October 2015, 5pm.</b><br>
        <br>
        Files must be zipped into one file.<br>
        These files will be used for plagiarism testing and as a reference should problems arise.<br>
        <br>
        <b>If all these things are not done, you will receive zero marks!</b>
      </blockquote>
      <p><b>If you complete the assignment ahead of time, your tutor may be able to mark you on the spot, leaving you free to work on other assignments in other courses.</b></p>
    </section>
  </section>
  <section>
    <header>Web Programming <span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h3>2. Debugging Assistant</h3>
      <p>Please please please include this module near or inside the footer of your web pages to help you and your tutors debug your code:</p>
      <code>&lt;?php include_once("/home/eh1/e54061/public_html/wp/debug.php"); ?&gt;</code>
    </section>
  </section>
  <section class=key>
    <header>Web Programming <span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h3>19. Marks Allocation</h3>
      <p><i>Please note that there are currently <b>32 marks</b> available, so programmers and designers can get full marks without completing every option.</i></p>
      <table>
        <tr><th>1. PHP Conversion, Modularisation and SESSION</th><th>3 Marks</th></tr>
        <tr><td>All main pages converted to PHP, some modularisation</td><td>1 mark</td></tr>
        <tr><td>Modularisation is extensive and/or SPA is implemented well</td><td>1 mark</td></tr>
        <tr><td>SESSION information is maintained and available on every page</td><td>1 mark</td></tr>

        <tr><th>2 - AJAX Movies Selection Page</th><th>3 Marks</th></tr>
        <tr><td>Homepage content is well written, contains student details</td><td>1 mark</td></tr><tr><td>Image embedded correctly, is 200px by 200px</td><td>1 mark</td></tr>
        <tr><td>AJAX data is styled correctly and neatly using CSS</td><td>1 mark</td></tr>
        <tr><td>Trailer and screenings hidden and revealed in a "view more" section</td><td>1 mark</td></tr>

        <tr><th>3. Reservation Page or Facility</th><th>3 marks</th></tr>
        <tr><td>Some reservation information is being added to the session</td><td>1 mark</td></tr>
        <tr><td>Customer can book multiple seats and multiple seat types</td><td>1 mark</td></tr>
        <tr><td>Customer can make reservations for more than one movie</td><td>1 mark</td></tr>

        <tr><th>4. Shopping Cart Page or Facility</th><th>5 marks</th></tr>
        <tr><td>Cart page shows all reservations made by customer</td><td>1 mark</td></tr>
        <tr><td>Prices are calculated correctly server side</td><td>1 mark</td></tr>
        <tr><td>Above information laid out neatly with subtotals and totals</td><td>1 mark</td></tr>
        <tr><td>Seat quantities can be adjusted on the cart page</td><td>1 mark</td></tr>
        <tr><td>Customer can delete a reservation on the cart page</td><td>1 mark</td></tr>
        <tr><th>5. Meal and Movie Deal Voucher</th><th>2 marks</th></tr>
        <tr><td>Voucher code format validated client side using a simple pattern/regex</td><td>1 mark</td></tr>
        <tr><td>PHP function checks the voucher code is valid, applies a 20% discount
to cart if valid, and displays an error message if invalid</td><td>1 mark</td></tr>

        <tr><th colspan=2 style='text-align:right'>... continued on next page</th></tr>
      </table>
    </section>
  </section>
  <section class=key>
    <header>Web Programming <span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h3>19. Marks Allocation (continued)</h3>
      <p><i>Please note that there are currently <b>32 marks</b> available, so programmers and designers can get full marks without completing every option.</i></p>
      <table>
        <tr><th>6. Customer Details</th><th>2 marks</th></tr>
        <tr><td>Customer can enter name, phone & email details</td><td>1 mark</td></tr>
        <tr><td>Customer details are validated appropriately client side</td><td>1 mark</td></tr>
        <tr><th>7. Checkout and Reservation Completion</th><th>4 marks</th></tr>
        <tr><td>Reservation information is stored server side in a file or a database in an organised manner (eg JSON, XML, CSV)</td><td>1 mark</td></tr>
        <tr><td>Shopping cart is emptied once reservations are completed</td><td>1 mark</td></tr>
        <tr><td>Ticket information is presented to customer in a professional, user friendly and printable (or mobile) format</td><td>1 mark</td></tr>
        <tr><td>Ticket design very stylish, like a real cinema ticket</td><td>1 mark</td></tr>

        <tr><th>8. Extension Activities (xFactor)</th><th>10 marks</th></tr>
        <tr><td>Github or other versioning repository is being used</td><td>1 mark</td></tr>
        <tr><td>Pages are built according to an OOP or MVC template</td><td>1 mark</td></tr>
        <tr><td>Website passes Google's mobile friendly test:<br>https://www.google.com/webmasters/tools/mobile-friendly</td><td>1 mark</td></tr>
        <tr><td>Movies page is built and styled correctly using JSON web service</td><td>1 mark</td></tr>
        <tr><td>Design quality of reservation and cart pages is very high</td><td>1 mark</td></tr>
        <tr><td>View Ticket System: User can login using a 5 digital random number + emailto view their ticket</td><td>1 mark</td></tr>
        <tr><td>Ticket contains a working QR code for cinema to scan</td><td>1 mark</td></tr>
        <tr><td>GUI Reservation System: A GUI seat reservation system has been developed that lets customers select and book seats (eg seat E7)</td><td>1 mark</td></tr>
        <tr><td>Design quality of GUI reservation system is very high</td><td>1 mark</td></tr>
        <tr><td>Reservation system detects seats already booked, blocks double reservations for at least one movie session</td><td>1 mark</td></tr>

        <tr><th colspan=2 style='text-align:right'>Total: 20 marks (or 20% of your final grade)</th></tr>
      </table>
    </section>
  </section>
</article>