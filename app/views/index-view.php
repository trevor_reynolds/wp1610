<?php
  error_reporting(E_ERROR | E_WARNING | E_PARSE);
  
  require_once('_definitions.php');
  require_once('_tools.php');
  require_once('_atop.php');
  include_once('instructions.php');

// 20160604: Load modules (ie lectures)
  foreach($lectures[HOST]['load'] as $i) {
    $module="lectures/lecture$i.php";
    include_once($module);
  }

  require_once('_bottom.php');