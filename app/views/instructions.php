<article id='slides-instructions'>
  <section class=key>
    <header>Welcome to Web Programming</header>
    <section>
      <h3>Instructions</h3>
      <p>Select in the <b>Drop Down Navigation</b> which modules (ie lectures) you want to look at. These buttons are toggle buttons, press once to show the module, press again to hide. Your selections will be stored locally and remembered the next time you visit on this machine.</p>
      <p>Alternatively, you can <b>Search</b> for a word or a phrase (must be at least two letters). All slides in the course with that word or phrase will appear. <i>eg search for 'color' if you want to see all slides with the word 'color' in it.</i></p>
      <p>If you want to hide/show these instructions, click the <b>Instructions</b> checkbox.</p>
      <p>If you see something you like in these slides, <strong>right click</strong> and select <strong>inspect element</strong>. More often than not, you will be able to see the code that makes it work or style that makes it pretty.</p>
      <p>Finally, if you want to print these slides, they will restyle into a more printer-friendly format.</p>
    </section>
  </section>
  <section>
    <header>Emergency Procedure Information Student Statement</header>
    <section>
      <p>Before starting our session today, RMIT wishes to make you aware of the Emergency Procedures that are in place for your safety. Please look around and familiarize yourself with the emergency exits in this room and do this for each new venue in which you attend classes. Evacuation plans are located in all corridors.</p>
      <p><?php a('http://www1.rmit.edu.au/browse/Staff/Workplace%20essentials/Services%20and%20advice/Property%20Services/About/Facilities%20Services/Security/Emergency/What%20to%20do%20when%20the%20emergency%20alarm%20sounds/','Link to Official Instructions'); ?></p>
      
      <h1>Alert Tone (Beep Beep)</h1>
      <p><audio src="http://mams.rmit.edu.au/ch2o1f1rqkqsz.wav" id="beepbeep" type="audio/mpeg" controls></audio></p>
      <p>On hearing the Alert tone, you should:</p>
      <ul>
        <li>check your area for fire, smoke or other abnormal situations</li>
        <li>await further instructions relayed over the public address system, or from the floor warden or wardens (members of the emergency control organisation)</li>
        <li>prepare yourself to evacuate the building e.g. commence shutdown of work by saving any computer files you are working on, switch off electrical appliances, close gas valves and make the area safe to leave (if safe to do so)</li>
        <li>commence evacuation if there is immediate danger.</li>
      </ul>
      <p>You are not required to evacuate unless instructed to do so by an announcement made over the public address system, a direction from the floor warden or wardens (members of the Emergency Control Organisation), a direction from RMIT Security, or a direction from the attending Emergency Service.</p>
      
      <h1>Evacuate Tone (Whoop Whoop)</h1>
      <p><audio src="http://mams.rmit.edu.au/qf5tbjlsj1ysz.wav" id="whoopwhoop" type="audio/mpeg" controls></audio></p>
      <p>On hearing the Evacuate tone or if directed to evacuate by the floor warden or wardens (members of the Emergency Control Organisation) you should:</p>
      <ul>
        <li>proceed to evacuate the building via the marked exits and stairs</li>
        <li>proceed to evacuate in an orderly fashion to the Assembly Area indicated on the evacuation diagrams</li>
        <li>follow the directions from announcements made over the public address system, the directions from the floor warden or wardens (members of the Emergency Control Organisation), the directions from RMIT Security, or the direction from the attending Emergency Service.</li>
        <li>commence evacuation if there is immediate danger.</li>
      </ul>
      
      <h1>Do Not:</h1>
      <ul>
        <li>carry occupant(s) with disabilities down stairs or escalators</li>
        <li>use lifts or escalators to evacuate the building except when instructed to do so by the attending Emergency Service, RMIT Security or by a member of the Emergency Control Organisation</li>
        <li>congregate outside the building entrances unless instructed to do so by an announcement made over the public address system, a direction from the Floor Warden or Wardens (members of the Emergency Control Organisation), a direction from RMIT Security, or a direction from the attending Emergency Service</li>
        <li>re-enter the building until advised that it is safe to return to the building by the attending Emergency Service, RMIT Security or by a member of the Emergency Control Organisation</li>
        <li>carry on with business - this causes delays, which may put your life and those of others in danger if the emergency is not controlled</li>
        <li>try to contact anyone to find out what is going on - this congests the telephone system and hampers control of the emergency.</li>
      </ul>
      <p>RMIT thanks you for your cooperation.</p>
    </section>
  </section>
</article>