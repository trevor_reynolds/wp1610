<?php
  $counter=1;
  $slideText='Lect 1, P.';
?>
<article id='slides-lecture-1'>
  
  <section class='title'>
    <header>Welcome!<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Web Programming<br>COSC2413/2426/2453</p>
      <p>Lecture 1</p>
    </section>
  </section>
  
  <section class='title'>
    <header>Lecture 1.1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 1: Administrivia</p>
    </section>
  </section>
  
  <section class='key'>
    <header>Administrivia<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-importantdates'>Important Dates</a></li>
        <li><a href='#tp-consultsupport'>Consultation and Support</a></li>
        <li><a href='#tp-textbooks'>Textbooks</a></li>
      </ol>
    </section>
  </section>
  <?php /* ***** On Campus *****
  <section class='key campus'>
    <header>Course Staff<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ul>
        <li>Lecturer: <b>Falk Scholer</b> (falk.scholer@rmit.edu.au)</li>
        <li>Head Tutor: <b>Trevor Reynolds</b> (trevor.reynolds@rmit.edu.au)</li>
        <li>Tutors / Lab Assistants:
        <ul>
          <li><b>Adrian Luke</b></li>
          <li><b>Ahmed Mourad</b></li>
          <li><b>Byron Fisher</b></li>
          <li><b>Laurence Barnes</b></li>
          <li><b>Matthew Bolger</b></li>
          <li><b>Ying Zhang</b></li>
        </ul>
        </li>
      </ul>
    </section>
  </section>

  <section class='key'>
    <header>Course Structure<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ul>
        <li>Lecture</li>
        <ul>
          <li>2 hours every week</li>
          <li>80.02.07</li>
          <li>Echo 360 lecture recordings have been booked (but... this isn't a substitute for lectures, 
                *and* there seem to be problems with recordings nearly every semester!)</li>
        </ul>
        <li>Tutorials and Labs</li>
        <ul>
          <li>1 hour tutorial and 1 hour lab every week</li>
          <li>You can book into any session that suits you (via <a href="http://appinfo.rmit.edu.au/STS/">STS</a>)</li>
          <li>If you can't make your class for some reason, try to attend another class (there is usually space)</li>
        </ul>
      </ul>
    </section>
  </section>
*/ ?>
  <section class='key' id='tp-importantdates'>
    <header>Important Dates<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Bookmark this url ...</h1>
      <p><?php a('http://www1.rmit.edu.au/students/he2017'); ?> (on campus)</p>
      <p><?php a('http://www1.rmit.edu.au/browse;ID=rau7970fsl011'); ?> (OUA)</p>
      <p>Dates you need to be concerned about (in order):</p>
      <ul>
        <li>Last day to add a course.</li>
        <li>Last day to withdraw without paying or failing.</li>
        <li>Last day to withdraw without failing.</li>
        <li>Exam timetable release date.</li>
      </ul>
      <p>And last but not least, make sure you find out when and where your exams will be taking place!</p>
    </section>
  </section>
  
  <section class='key' id='tp-consultsupport'>
    <header>Consultation and Support<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Inside the Course</h1>
      <p>Blackboard Discussion Forums</p>
      <ul>
        <li><b>Don't be shy!</b> Any questions that will be of interest to others (i.e. anything not of a personal nature) should be posted here. <i>Your question may start a discussion thread that is far richer than a short answer given by a single staff member!</i></li>
      </ul>
      <p>Teaching Staff</p>
      <ul>
        <li>Tutorial/lab-related questions or general assignment-related questions during tutorial/lab session</li>
      </ul>
      <p>Head Tutor</p>
      <ul>
        <li>General subject questions, consultation about assignment and feedback</li>
      </ul>


      <p>Lecturer</p>
      <ul>
        <li>General subject questions, lecture related issues, serious but short assignment extension requests, and doubts relating to course as a whole</li>
		  <?php
 //       <li>General subject questions, consultation about assignment and feedback</li>
		  ?>
      </ul>
    </section>
  </section>
  
  <!--section>
    <header>Consultation and Support<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Inside the School</h1>
      <p>CSIT Advisors</p>
      <ul>
        <li>General problems, or issues with anything broader than the Web Programming course (for example, enrolment, study issues, program-related difficulties, ...)<br><?php e('csit.advisors@rmit.edu.au'); ?></li>
      </ul>
      <p>Mentoring Sessions</p>
      <ul>
        <li>Run by students who have taken the course before.</li>
        <li>They are there to help you with the content, and to guide you to find the answer.</li>
        <li>Mentoring will start in Week 3.</li>
        <li>The sessions will be on a “drop in” basis, there is no need to register.</li>
        <li>Details will be posted to the online course site.</li>
      </ul>
    </section>
  </section-->
  
  <section>
    <header>Consultation and Support<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Inside the University</h1>
      <p>A useful page explaining all student support services can be found here: <?php a('http://www1.rmit.edu.au/browse;ID=jas1bocqq6h9'); ?></p>
      <p>RMIT Equitable Learning Services</p>
      <ul>
        <li>The Equitable Learning Services unit assists students living with a disability, long term illness and/or mental health condition. Teaching staff are advised of your condition and are given advice on how to cater to your specific needs.</li>
        <li>Often you get a chance to sit exams with more favourable conditions (smaller room, rest breaks, scribe to write out your answers etc.)<br><?php a('https://www.rmit.edu.au/students/support-and-facilities/student-support/equitable-learning-services'); ?></li>
      </ul>
      <p>RMIT University Student Union (RUSU)</p>
      <ul>
        <li>The RMIT University Student Union will give you independent advice and can offer advice on a range of issues. The service is free, confidential and available to ALL RMIT students.<br><?php a('http://www.su.rmit.edu.au/help/tips-for-student-rights-rmit/'); ?></li>
      </ul>
      <p>RMIT Counselling</p>
      <ul>
        <li>The RMIT Counselling Service provides a high quality professional psychological service in a safe, friendly and <b>confidential</b> environment for you to talk and deal with personal issues and mental health.<br><?php a('http://www1.rmit.edu.au/counselling'); ?></li>
      </ul>
      <p>A lot of help is available. Do not leave things until the last minute. It is far easier to resolve problems when there is sufficient time!</p>
    </section>
  </section>
  
  <section class='key' id='tp-textbooks'>
    <header>Textbooks<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Textbooks</h1>
      <p>Textbooks covering all material for this course are by Jon Duckett and Matt Doyle. Books are available from the RMIT bookstore and also online:</p>
      <table>
        <tr>
          <th width=33%>HTML &amp; CSS<br>by Jon Duckett</th>
          <th width=33%>Javacript &amp; jQuery<br>by Jon Duckett</th>
          <th width=33%>Beginning PHP 5.3<br>by Matt Doyle</th>
        </tr>
        <tr>
          <td><img src='app/img/JonDuckettHTMLCSS.jpg' alt='HTML &amp; CSS by Jon Duckett'></td>
          <td><img src='app/img/JonDuckettJSJQ.jpg' alt='Javascript &amp; jQuery by Jon Duckett'></td>
          <td><img src='app/img/MattDoylePHP.jpg' alt='Beginning PHP 5.3 by Matt Doyle'></td>
        </tr>
        <tr>
          <td colspan=2>These 2 books are available as a package set:<br><?php a('http://www.amazon.com/Jon-Duckett/e/B001IR3Q7I','Jon Duckett on Amazon.com'); ?> or <?php a('http://au.wiley.com/WileyCDA/WileyTitle/productCd-1118907442.html','John Duckett at Wiley.com'); ?>.</td>
          <td>This book can be bought from:<br><?php a('http://au.wiley.com/WileyCDA/WileyTitle/productCd-0470413964.html','Wiley: Beginning PHP 5.3'); ?></td>
        </tr>
      </table>
    </section>
  </section>
  <?php /* ***** On Campus *****
  <section class='key'>
    <header>Assessment Structure<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ul>
        <li>The assessment for this course comprises practical project work (worth 50%) and a final exam (worth 50%)</li>
      </ul>
      <ul>
        <li>Practical Component (worth 50%) includes 3 assignments and 5 lab projects. The assignment tasks increase in complexity and difficulty</li>
        <ul>
          <li>Assignment 1 (HTML), worth 5 marks- due at the end of week 3 (with demos in week 4)</li>
          <li>Assignment 2 (HTML, CSS), worth 14 marks - due at the end of week 7 (with demos in week 8)</li>
          <li>Assignment 3 (Functioning back end of website using Javascript and PHP), worth 21 marks - due at the end of week 12 (with demos in week 12 and week 13)</li>
          <li>5 Lab projects, worth 2 marks each - due in weeks 5, 6, 7, 9 and 10</li>
        </ul>
      </ul>
      <ul>
        <li>Exam Component (worth 50%): 2 hour (+ 15 minutes reading time) closed-book exam</li>        
      </ul>
    </section>
  </section>
  */ ?>
  
  <section class='key'>
    <header>Assessment Structure<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h2>Course Assessment</h2>
      <p>Practical Component (worth 50%): includes 3 assignments and a participation and engagement component</p>
      <p>Exam Component (worth 50%): a final 2 hour (+ 15 minutes reading time) closed-book exam</p>
      <p></p>        
      <h3>Practical Component:</h3>
      <dl>
        <dt>HTML / Setup (5%)</dt>
        <dd>Assignment 1 - due at the end of week 4</dd>
        <dt>HTML / CSS / HCI (10%)</dt>
        <dd>Assignment 2 - due at the end of week 7</dd>
        <dt>Javascript / PHP (25%)</dt>
        <dd>Assignment 3 - due at the end of week 12</dd>
        <dt>Participation and Engagement (10%)</dt>
        <dd>Pre-submission and demonstration (2% A2, 3% A3); and class participation (5%)</dd>
      </dl>
		<p>Note that A1 is a solo assignment, while A2 and A3 need to be done in pairs</p>
    </section>
  </section>
  
  <section class='key'>
    <header>Plagiarism<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>
        <b>Plagiarism is an offence</b>
      </p>
      <p>To avoid plagiarism, you must give credit whenever you use:</p>
      <ul>
        <li>Another person’s idea, opinion, or theory</li>
        <li>Any facts, statistics, graphs, drawings, or any other pieces of information that are not common knowledge</li>
        <li>Quotations of another person’s actual spoken or written words</li>
        <li>Paraphrasing of another person’s spoken or written words</li>
      </ul>
      <p>
        <b>
          All submitted work without citation must be your own  
        </b>
      </p>
      <?php he("...
<!-- Original image below sourced for educational purposes: www.coke.com/coke-can.jpg -->
<img src='coke-can.jpg' alt='inviting cold coke can, dripping with condensation'/>
...
<!-- Original CSS code sourced and adapted for educational purposes: 
     http://www.lynda.com/CSS-tutorials/Creating-Responsive-Web-Design/110716-2.html -->
..."); ?>
    </section>
  </section>
  <section class='key'>
    <header>Course Overview<span><?php print $slideText.($counter++); ?></span></header>
    <section>
    <table>
      <tr>
        <td>Week 1</td><td>HTML [1]</td>
      </tr>
      <tr>
        <td>Week 2</td><td>HTML [2]</td>
      </tr>
      <tr>
        <td>Week 3</td><td>Cascading Style Sheets [1]</td>
      </tr>
      <tr>
        <td>Week 4</td><td>Cascading Style Sheets [2]</td>
      </tr>
      <tr>
        <td>Week 5</td><td>Human Computer Interface (HCI)</td>
      </tr>
      <tr>
        <td>Week 6</td><td>Javascript [1]</td>
      </tr> 
      <!--tr>
        <td colspan="2">Mid-semester break</td>
      </tr-->
      <tr>
        <td>Week 7</td><td>Javascript [2]</td>
      </tr>
      <tr>
        <td>Week 8</td><td>PHP [1]</td>
      </tr>
      <tr>
        <td>Week 9</td><td>PHP [2]</td>
      </tr>
      <tr>
        <td>Week 10</td><td>PHP [3]</td>
      </tr>
      <tr>
        <td>Week 11</td><td>PHP [4]</td>
      </tr>
      <tr>
        <td>Week 12</td><td>Course revision</td>
      </tr>
    </table>
    </section>
  </section>
  
  <section class='key'>
    <header>Course Topics<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>On completion of this course you should be able to:</p>
      <ul>
        <li>Explain the functions of clients and servers on the Web</li>
        <li>Build web pages using HTML, and style them using CSS</li>
        <li>Implement client-side scripts using JavaScript</li>
        <li>Implement server-side scripts using PHP</li>
        <li>Design and implement an interactive web site with consideration of issues relating to usability, accessibility and internationalisation</li>
      </ul>
    </section>
  </section>
  
  <section class='key'>
    <header>What Isn't In This Course?<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ul>
        <li>
          How to build full-fledged web database applications
          <ul><li>
            Advanced PHP functionality such as interacting with a DBMS is covered in Web Database Applications (ISYS1124/1126)    
          </li></ul>
        </li>
        <li>
          How to process XML documents
          <ul>
            <li>
              Document Markup Languages (COSC2104/2106) covers XML, XSLT and related technologies
            </li>
          </ul>
        </li>
        <li>
          Detailed descriptions of networking protocols 
          <ul>
            <li>
              Data Communication and Net-Centric Computing (COSC1111/2061) covers TCP/IP and related technologies
            </li>
          </ul>
        </li>
      </ul>
    </section>
  </section>
  
  <section class='key'>
    <header>Why HTML, Javascript and PHP?<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Very roughly speaking:</p>
      <ul>
        <li>HTML and CSS allow for the design of static web pages</li>
        <li>JavaScript adds client-side scripting</li>
        <li>PHP adds server-side scripting</li>
      </ul>
      <p>We will cover all of these ideas in detail in this course. The important point for now is that these three technologies work together to enable you implement extremely flexible web-based pages, sites, and applications.</p>
    </section>
  </section>
  
  <section class='key'>
    <header>Who Uses This Stuff?<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>People who want to build their own websites with personalized features such as:</p>
      <ul>
        <li>Personalized layouts</li>
        <li>Online forms</li>
        <li>User and document management systems</li>
        <li>Payment systems</li>
        <li>Specialized search functions</li>
        <li>A lot of advanced functions</li>
      </ul>
      <p>People who want to become a web developer:</p>
      <ul>
		<!-- link not working 1750
        <li>5 Reasons to learn web development<br><?php a('http://www.skilledup.com/articles/5-reasons-to-learn-web-development'); ?></li>
		-->
        <li>10 Reasons to learn web development<br><?php a('http://killer-web-development.com/blog/view/10-reasons-to-learn-web-development'); ?></li>
      </ul>
    </section>
  </section>
  
  <section class='key'>
    <header>Are These Technologies Used Commercially?<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Yes!</p>
      <p>Some popular sites that use PHP:</p>
      <ul>
        <li>Wikipedia</li>
        <li>Facebook</li>
        <li>Yahoo!</li>
        <li>Wordpress</li>
        <li>Tumblr</li>
        <li>Baidu</li>
        <li>...</li>
      </ul>
      <p>W3Techs estimate the percentage of websites using various client- and server-side programming languages:</p>
      <ul>
        <li>
          <a href="https://w3techs.com/technologies/overview/client_side_language/all">Usage of client-side programming languages</a>
        </li>
        <li>
          <a href="https://w3techs.com/technologies/overview/programming_language/all">Usage of server-side programming languages</a>
        </li>
      </ul>
    </section>
  </section>

  <section class='title'>
    <header>Lecture 1.2<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 2: HTML I</p>
      <p>History of HTML<br>From HTML 1 to HTML5 (via XHTML)<br>HTML in use Today</p>
    </section>
  </section>
  <section class='key'>
    <header>HTML I<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-www'>The World Wide Web</a></li>
        <li><a href='#tp-html1'>HTML Version 1</a></li>
        <li><a href='#tp-html23'>HTML Versions 2 &amp; 3</a></li>
        <li><a href='#tp-html4'>HTML Version 4</a></li>
        <li><a href='#tp-xhtml'>XHTML</a></li>
        <li><a href='#tp-html5'>HTML5</a></li>
        <li><a href='#tp-htmlelements'>HTML Elements</a></li>
        <li><a href='#tp-classid'>Identifying Attributes</a></li>
      </ol>
    </section>
  </section>
  <section class='key' id='tp-www'>
    <header>The World Wide Web<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>The Internet</h1>
      <ul>
        <li>The Internet is a system of millions of networks, including private, public, government, business and academic, are linked by various networking technologies, including electronic, wireless and optical</li>
        <li>The networks use particular protocols to communicate, such as the Internet protocol suite (TCP/IP)</li>
        <li>This theoretically allows any computer or device to communicate with another computer or device, as long as they're connected to the Internet</li>
      </ul>
      <h1>The World Wide Web</h1>
      <ul>
        <li>The World Wide Web (or just "the Web") is an information space built on top of the Internet</li>
        <li>Documents and other resources on the Web are identified by a <i>URI</i> (Universal Resource Identifier)</li>
        <li>These resources are linked together using <i>hyperlinks</i></li>
      </ul>
    </section>
  </section>
  
  <section>
    <header>The World Wide Web<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Invention of the World Wide Web</h1>
      <p><img class='fr' src='app/img/TBL2012.png' alt='Sir Tim berners-Lee at the 2012 Olymic Games opening ceremony'/>Sir Tim Berners-Lee is largely credited for pioneering <strong>Hypertext Markup Language</strong> (HTML), inventing the <strong>World Wide Web</strong> in 1989, and giving his invention away for free.</p>
      <p>Without his generosity, everytime you clicked on a hyperlink <?php a('https://www.w3.org/People/Berners-Lee/','like this one'); ?> (during the patent period), a small royalty fee would have to be paid to Tim or to <?php a('http://home.cern/topics/birth-web','CERN'); ?>, where he was employed at the time.</p>
		<p><a href="https://cacm.acm.org/magazines/2017/6/217732-weaving-the-web/fulltext">Tim Berners-Lee</a> was awarded the 2016 <a href="https://www.acm.org/media-center/2017/april/turing-award-2016">ACM <em>A.M. Turing Award</em></a>, sometimes called the "Nobel Prize in Computing".</p>
      <div class='modal-block cb'>
        <div>
          <p>Click here to see more of Tim at the 2012 London Olympic Games Opening Ceremony.</p>
        </div>
        <div>
          <?php youtube("https://www.youtube.com/embed/Yy9LN-8y96Q?start=319") ?>
          <p>Tim too tiny? <?php a('https://youtu.be/4As0e4de-rI?t=4642','Click here for a better view'); ?> or <?php a('https://www.ted.com/talks/tim_berners_lee_on_the_next_web','view a more in depth Ted Talk'); ?> about how it all came about.</p>
          <div>OK, thanks Tim!</div>
        </div>
      </div>
    </section>
  </section>
  
  <section>
    <header>The World Wide Web<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Hypertext</h1>
      <ul>
        <li>Conventional writing is linear, but humans think in an associative way by connecting things together</li>
        <li>Hypertext attempts to bridge the gap, by representing information in discrete pieces of data, called <b>nodes</b>, connected by <b>links</b></li>
        <li>The user controls the order in which the text is viewed, not the author</li>
        <li>The purpose of links is to suggest paths, rather than determine the viewing order</li>
        <li>The World Wide Web is a hypertext</li>
      </ul>
      <h2>HTML as Hypertext</h2>
      <ul>
        <li>The Hypertext Markup Language (HTML) is a high level markup language (set of rules) that describes the content of web pages</li>
        <li>Each web page can be considered a hypertext node.  Each node is written in HTML, and can contain embedded links to other nodes (web pages)</li>
        <li><code>We all study at &lt;a href="<a href="http://www.rmit.edu.au">http://www.rmit.edu.au</a>"&gt;RMIT University&lt;a&gt;</code></li>
      </ul>
    </section>
  </section>
  <section>
    <header>The World Wide Web<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>How Does the Web Work?</h1>
      <ul>
        <li>The World Wide Web uses a client-server model</li>
        <li>A web <b>server</b> is a program running on a computer whose purpose is to deliver documents to other computers on request. A web server runs continuously, waiting for new requests</li>
        <li>A web <b>client</b>, or browser, is a program that runs on a personal computer, smart-phone, or other device, and requests web pages from a server as the user asks for them. When a page has been received, the client then renders it for display</li>
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Client-server-model.svg/500px-Client-server-model.svg.png"/>
        <li>Web browsers and servers communicate with each other using HTTP, the HyperText Transfer Protocol</li>
        <li>The World Wide Web is the global body of information available via HTTP</li>
      </ul>
    </section>
  </section>
  
  <section>
    <header>The World Wide Web<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Client-Server Example</h1>
      <ol>
        <li>The user runs a web browser on their computer, which starts up showing the RMIT Computer Science home page</li>
        <li>The user clicks on an underlined link, "about RMIT" which is linked to <code><span>http://</span>www.rmit.edu.au<span><span>/</span></span></code></li>
        <li>The web browser connects via the Internet to the computer specified by the link <code>www.rmit.edu.au</code>, and using the HTTP protocol <code><span>http://</span></code>, asks that computer's web server for the web page <code><span><span>/</span></span></code></li>
        <li>The web server responds by sending the HTML text of that web page back to the web browser</li>
        <li>The web browser then processes the HTML text and displays the web page on the user's screen</li>
      </ol>
      <p>Every time you select a page, a transaction like this takes place.</p>
    </section>
  </section>
  <section class='key' id='tp-html1'>
    <header>HTML Version 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>HTML, Head and Body Elements</h1>
      <p>To get a sense of what was present in a very early web document, have a look at the source code of the very first web page ever written! <?php a('http://info.cern.ch/hypertext/WWW/TheProject.html', 'Browser'); ?> or <?php a('http://line-mode.cern.ch/www/hypertext/WWW/TheProject.html','Terminal'); ?></p>
      <p>What you will see is that the document is divided into two <strong>elements</strong>, each defined by opening and closing <strong>paired tags</strong>.</p>
      <ul>
        <li><code>&lt;HEADER&gt;<span> ... </span>&lt;/HEADER&gt;</code> (now written as <code>&lt;head&gt;<span> ... </span>&lt;/head&gt;</code>)</li>
        <li><code>&lt;BODY&gt;<span> ... </span>&lt;/BODY&gt;</code> (now written as <code>&lt;body&gt;<span> ... </span>&lt;/body&gt;</code>)</li>
      </ul>
      <p>These two areas designate what we would normally see in a book or a document:</p>
      <ul>
        <li><code>&lt;head&gt;</code> contains information about the book or document, publisher, author, title; more or less the 'cover' and 'covering pages', ie the first few pages and perhaps last pages in a book.</li>
        <li><code>&lt;body&gt;</code> contains the 'human readable' content of the book or document, sections, chapters, headings, paragraphs, images etc.</li>
      </ul>
      <p>Today, both of these elements are written in lowercase and are nested inside <code>&lt;html&gt;<span> ... </span>&lt;/html&gt;</code> tags with a HTML5 doctype declaration.</p>
      <pre><code><span>&lt;!doctype html&gt;</span>
&lt;html&gt;
  &lt;head&gt;<span> ... </span>&lt;/head&gt;
  &lt;body&gt;<span> ... </span>&lt;/body&gt;
&lt;/html&gt;</code></pre>
    </section>
  </section>
  
  <section>
    <header>HTML Version 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
    <h1>More Elements and Attributes</h1>
      <p>You will also see other elements, some defined by <strong>paired</strong> tags with content, some <strong>empty</strong>, some nested inside other elements, and some with <strong>attributes</strong>:</p>
      <ul>
        <li><code>&lt;NEXTID N="<span>55</span>"&gt;</code> (empty element, defined with a single tag, and with an attribute)</li>
        <li><code>&lt;H1&gt;<span> ... </span>&lt;/H1&gt;</code> (element with content, defined with paired tags)</li>
        <li><code>&lt;DL&gt;&lt;DT&gt;<span> ... </span>&lt;/DT&gt;&lt;/DL&gt;</code> (DT element nested inside a DL element)</li>
      </ul>
      <p>The <code>&lt;NEXTID&gt;</code> tag is no longer used (presumably Tim's NEXT computer had the serial number 55), but as discussed, the ground breaking tag in this document is the <strong>anchor</strong> tag that allows this document to reference other documents using hypertext links on what has become the <strong>World Wide Web</strong>.</p>
      <p><code>&lt;A NAME=<span>44</span> HREF="<span>../DataSources/Top.html</span>"&gt;<span>What's out there?</span>&lt;/A&gt;</code></p>
      <p>Today the anchor tag is considered to be an <strong>inline</strong> element and must be contained inside <strong>block</strong> elements such as a <strong>paragraph</strong> element:</p>
      <p><code>&lt;p&gt;&lt;a href='<span>../DataSources/Top.html</span>'&gt;<span>What's out there?</span>&lt;/a&gt;&lt;p&gt;</code></p>
    </section>
  </section>
  
  <section>
    <header>HTML Version 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Anchor Element</h1>
      <p>The anchor tag can be used to link to other documents:</p>
      <p><code>&lt;a href='<span>anotherpage.html</span>'&gt;<span>Go to a document in the same directory</span>&lt;/a&gt;</code></p>
      <p><code>&lt;a href='<span><span>subdirectory/</span>anotherpage.html</span>'&gt;<span>Go to a document in a sub-directory</span>&lt;/a&gt;</code></p>
      <p><code>&lt;a href='<span><span>../</span>anotherpage.html</span>'&gt;<span>Go to a document in the parent directory</span>&lt;/a&gt;</code></p>
      <p><code>&lt;a href='<span><span>/wp1710/</span>anotherpage.html</span>'&gt;<span>Go to a document in your wp1710 directory</span>&lt;/a&gt;</code></p>
      <p><code>&lt;a href='<span><span>http://anotherserver.com/</span>anotherpage.html</span>'&gt;<span>Go to a document on another server</span>&lt;/a&gt;</code></p>
      <p>Or jump to another position in the current document:</p>
      <pre><code>&lt;a href='<span>#tldr</span>'&gt;<span>Skip to the end</span>&lt;/a&gt;
      
... Long Story ...

&lt;p id='<span>tldr</span>'&gt;<span>The butler did it.</span>&lt;/p&gt;</code></pre>
      <p>Or even send someone an email:</p>
      <p><code>&lt;a href='<span>mailto:<?php echo EMAIL; ?></span>'&gt;<span>Mail Me</span>&lt;/a&gt;</code></p>
      <p><a href='mailto:<?php echo EMAIL; ?>'>Mail Me</a></p>
    </section>
  </section>
  <section>
    <header>HTML Version 1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Summary</h1>
      <p>By the time HTML version 1 had been finalised, there were enough HTML document elements that would allow a computer to render a simple word-processor generated document with</p>
      <ul>
        <li>meta tags containing author details, keywords, creation &amp; last modified dates etc,</li>
        <li>headings (<code>&lt;h1&gt;</code> - <code>&lt;h6&gt;</code>),</li>
        <li>paragraphs (<code>&lt;p&gt;</code>),</li>
        <li>lists (<code>&lt;ul&gt;, &lt;ol&gt;, &lt;dl&gt;</code>),</li>
        <li>and list items (<code>&lt;li&gt;</code>) <i>- just like these!</i></li>
      </ul>
      <p>Here is a comprehensive list of the tags and attributes, not all of which are still in use today!<br><?php a('http://www.w3.org/History/19921103-hypertext/hypertext/WWW/MarkUp/Tags.html'); ?></p>
    </section>
  </section>
  
  <section class='key' id='tp-html23'>
    <header>HTML Versions 2 &amp; 3<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Exponential Growth - 1990s</h1>
      <p>The World Wide Web Consortium (W3C) was founded in 1994 to develop, expand and advance HTML standards. It is an international community where member organizations, a full-time staff, and the public work together to develop Web standards. The W3C’s primary activity is to develop protocols and guidelines that ensure long-term growth for the Web</p>
      <p></p>
      <p>Around this time the World Wide Web began to grow exponentially, and computer operating systems with a Graphical User Interfaces (GUI) became commonplace.</p>
      <p>The W3C standards body were overwhelmed by this fast growing technological demand and a new group called the HTML Working group was set up in 1997 to take over progress and development, leaving W3C free to concentrate on maintaining the standards.</p>
      <p>New elements were created, some good and made part of the standard (eg <code>&lt;img&gt;</code> and <code>&lt;table&gt;</code> tags), others bad and not part of the standard (eg <code>&lt;blink&gt;</code> and <code>&lt;marquee&gt;</code> tags).</p>
      <p>Many new tags and attributes to control <strong>font, style, color, border</strong> and <strong>alignment</strong> were created, but these are have been deprecated and removed from both XHTML and HTML5.</p>
    </section>
  </section>
  
  <section>
    <header>HTML Versions 2 &amp; 3<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Exponential Growth - 1990s</h1>
      <p>Commercial interests dominated the web, and browsers from two key players -- Netscape Navigator (now Mozilla Firefox) and Microsoft Internet Explorer -- competed for web browser market dominance.</p>
      <p>Developers in this period were frustrated: code developed for one browser would not always work in another browser. Browser software developers were making their own rules and not following rules set down by the W3C.</p>
      <p>Microsoft made their web browser available for free as part of their operating system (Windows 95 Plus!), which eventually pushed their main competitor Netscape Navigator (a paid product) and other players out of the market. It was just easier to develop for IE.</p>
    </section>
  </section>
  
  <section>
    <header>HTML Versions 2 &amp; 3<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Stagnation</h1>
      <p>By the end of HTML3.2's useful life, HTML was in a state of flux and out of step with the key players and emerging web technology.</p>
      <div class='modal-block'>
        <div><p>Meanwhile a company called Macromedia (later acquired by Adobe) developed a web authoring program called Flash that ran in any browser. It allowed designers and developers to produce professional looking animated websites. Click here to view an example.</p></div>
        <div><p>Below is a screen capture video recording for those who do not have a Flash plugin</p><p><video controls width=676 height=548  src='app/video/translocation.mp4'></video></p><p>You should be able to access the website directly at <?php a('http://georgelogan.co.uk/translocationsite/'); ?></p><div>Click here to close</div>
        </div>
      </div>
      <p>Many commercial websites at this time used HTML to provide just the delivery platform for a website built purely in Flash. HTML was in danger of becoming a redundant or a second class programming language for commercial web sites. </p>
    </section>
  </section>
  <section class='key' id='tp-html4'>
    <header>HTML Version 4<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Separation of Style and Content</h1>
      <p>The first step towards modernising HTML3.2 was to separate style from content. The following changes were made in <?php a('http://www.w3.org/TR/html4/','HTML version 4.01 (strict)'); ?> which was adopted in 1999:</p>
      <ul>
        <li>Styling tags such as <code>&lt;center&gt;, &lt;font&gt;, &lt;strike&gt;, &lt;u&gt;</code> were removed / deprecated.</li>
        <li>Tags such as <code>&lt;b&gt;, &lt;i&gt;</code> were replaced with more meaningful (and more letter-ful) elements <code>&lt;strong&gt;, &lt;em&gt;</code>.</li>
        <li>Styling Attributes such as <code>align, color, height, width, border</code> were removed / deprecated.</li>
      </ul>
      <p>Today, we are expected to use <strong>Cascading Style Sheets</strong> as much as possible to style web pages (more in lectures 3 &amp; 4).</p>
      <p><i>If your tag or attribute is adding style, you are doing it wrong!</i></p>
    </section>
  </section>
  
  <section class='key' id='tp-xhtml'>
    <header>XHTML<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Adhering to a Strict XML Syntax</h1>
      <p>With the emergence of WAP enabled phones and other "underpowered" web devices around the turn of the century, there was a push to develop webpages with a very strict XML syntax. <?php a('http://www.w3.org/TR/xhtml1/','XHTML 1.0'); ?> was a reformulation of HTML4 in XML and was designed to allow web content on a wide range of less sophisticated devices, not just on personal computers.</p>
      <p>Let us not dwell too much on the details of this standard. <strong>Rules were strict!</strong></p>
      <ul>
        <li>All tags must be lowercase<br>
          <code>&lt;a href='<span> ... </span>'&gt;</code> and <code>&lt;body&gt;</code></li>
        <li>Tags must be closed, even the empty ones<br>
            eg <code>&lt;p&gt;<span> ... </span>&lt;/p&gt;</code> and <code>&lt;br <span>/</span>&gt;</code> etc</li>
        <li>Tags must be nested correctly<br>
          eg <code>&lt;strong&gt;<span>&lt;em&gt;<span> ... </span>&lt;/em&gt;</span>&lt;/strong&gt;</code> etc</li>
        <li>Attributes need a value in quotes, even when it is clear what they mean.<br>
            eg <code>href='<span>http://google.com</span>'</code></li>
        <li>Attributes need a value, even when the attribute makes it clear what is required<br>
            eg <code>checked='<span>checked</span>'</code></li>
      </ul>
      <p>Providing a simple <code>&lt;html&gt;</code> opening tag at the top of an XHTML document was no longer enough. We had to write this:</p>
      <pre><code>&lt;?xml version="<span>1.0</span>"?&gt;
&lt;!DOCTYPE html PUBLIC "<span>-//W3C//DTD XHTML 1.0 Strict//EN</span>"&gt;
&lt;html xmlns="<span>http://www.w3.org/1999/xhtml</span>" lang="<span>en</span>" xml:lang="<span>en</span>"&gt;</code></pre>
      <p>Rightly or wrongly, development of XHTML2.0 was abandoned in 2009 in favour of developing a new HTML5 standard <?php a('http://www.w3.org/2009/06/xhtml-faq.html'); ?> with a much simpler doctype declaration.</p>
      <pre><code>&lt;!doctype html&gt;
&lt;html lang="<span>en</span>"&gt;</code></pre>
    </section>
  </section>
  
  <section>
    <header>XHTML<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Interest rebounds</h1>
      <p>HTML as a web programming language made a comeback between 2000 - 2010 largely helped by these developments:</p>
      <ul>
        <li>In the early 2000s, Mozilla <em>Firefox</em> had emerged from the ashes of Netscape Navigator, with help from several companies and enthusiastic developers working for free, to provide the public with a viable free internet browser. They were joined by <em>Opera</em>, <em>Safari</em>, <em>Chrome</em> and other free browsers.<br>
		  <ul><li>
            <?php 
					// a('//gs.statcounter.com/#browser-ww-monthly-200807-201512','View global browers usage statistics from 2008 onwards here'); 
					a('https://www.w3counter.com/globalstats.php','View global brower usage statistics from 2008 onwards here'); 
				?>
				</li></ul>
        </li>
        <li>The <em>working group</em> and the <em>standards body</em> were working more coherently and keeping up with computer technology. Many more people were contributing work free of charge to develop the web technologies.</li>
        <li><em>Graphics</em> were now possible in HTML using <code>&lt;svg&gt;</code> and HTML5's <code>&lt;canvas&gt;</code> element.</li>
        <li>Apple created a smarter smart phone and <em>refused to support Flash</em> in its mobile devices, forcing developers to work with open-source graphic technologies.</li>
        <li>Internet users were relying more and more on <em>search engines</em> to find web content. Search engines started mining the actual HTML content in webpages pages to calculate more accurate page ranking, putting websites that had Flash web content at a disadvantage.</li>
        <li>More complex websites using Wordpress, Joomla and later frameworks such as Bootstrap and Angular generated HTML with an <em>improved front-end appearance</em>.</li>
      </ul>
    </section>
  </section>

  <section class='key' id='tp-html5'>
    <header>HTML5<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Rules Revision</h1>
      <p>By the time HTML5 was adopted in October 2014, HTML as a language had made a comeback.</p>
      <p>HTML5 adopted the more sensible strategies found in previous HTML standards (eg simpler doctype, lowercase tags, correct nesting of tags), but is not constrained by the overtly strict XML syntax found in XHTML:</p>
      <ul>
        <li>Empty tags don't need a closing slash<br>eg <code>&lt;br <span>/</span>&gt;</code> can be written as <code>&lt;br&gt;</code></li>
        <li>Values with no spaces don't need quotes<br>eg <code>value=<span>'</span>1<span>'</span></code> can be written as <code>value=1</code></li>
        <li>"Obvious" attributes can be minimised<br>eg <code>selected<span>='selected'</span></code> can be written as <code>selected</code></li>
      </ul>
      <p>The stylistic &lt;b&gt; and &lt;i&gt; were brought back into the standard with new / clarified semantic meaning:</p>
      <ul>
        <li><code>&lt;b&gt;</code>: <q>... represents a span of text to which attention is being drawn for utilitarian purposes without conveying any extra importance and with no implication of an alternate voice or mood ...</q></li>
        <li><code>&lt;i&gt;</code>: <q>... represents a span of text in an alternate voice or mood, or otherwise offset from the normal prose in a manner indicating a different quality of text ...</q></li>
      </ul>
      <p><i>Taken from: <?php a('https://www.w3.org/International/questions/qa-b-and-i-tags'); ?></i></p>
      <p>For this reason, this lecture series makes a departure from default styling. Text inside the <strong>&lt;em&gt;</strong> element will have an <em>underlined</em> style and text inside the <strong>&lt;i&gt;</strong> element will have an <i>italic</i> style.</p>
    </section>
  </section>

  <section>
    <header>HTML5<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>New Elements and Attributes</h1>
      <p>HTML5 also introduces more tags and attributes that are useful, including</p>
      <ul>
        <li>Semantic tags that help search engines analyse your page content and optimise your website ranking<br>eg <code>&lt;header&gt;, &lt;nav&gt;, &lt;main&gt;, &lt;article&gt;, &lt;section&gt;, &lt;footer&gt;</code> etc.</li>
        <li>Form input types and attributes that make web development easier<br>eg <code>email, date, color, range, pattern, required</code> etc.</li>
        <li>New <code>&lt;video&gt;</code> and <code>&lt;audio&gt;</code> elements that replace the need for third party plugins such as Quicktime&trade;, Flash&trade; etc.</li>
      </ul>
      <p><i>This course will focus on the HTML5 syntax, but you should be aware of syntax variations found in previous standards as you may be called on to maintain or upgrade pages from older HTML syntax in the work place.</i></p>
    </section>
  </section>

  <section class='key' id='tp-htmlelements'>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>A First Webpage</h1>
      <p>Let's have a look at a very simple webpage that demonstrates some of the elements we have encountered.</p>
      <div class='trev-tryit'>
<textarea rows=15 onblur='tryItBuffer(this.value)'><!doctype html>
<html>
  <head>
    <title>Goes in the tab</title>
    <meta charset="utf-8">
    <meta name='author' content='put your name here'>
    <meta name="description" content="how would you describe this page?">
  </head>
  <body>
<!-- I am a comment. Create h2, h3, H4, h5 and h6 elements below the h1 element -->
    <h1>I am a H1 Heading</h1>
    <p>Hello World. I am a simple paragraph.</p>
<!-- Delete the src attribute so that the image can't be found -->
    <p>I am another paragraph, but I contain an image that has a picture of Tim Berners-Lee in it.  The image tag is an <strong>empty</strong> tag with a <code>src</code> attribute where the source of the image is specified and an <code>alt</code> attribute where an <strong>alternative</strong> description is placed should the image not be viewable <img src='app/img/TBL2012.png' alt='Sir Tim Berners-Lee on stage at the 2012 Olymic Games Opening Ceremony' />. If it is not obvious, the image is an <strong>inline</strong> element and moves along with the text inside this paragraph and creates a lot of space between the line that it is on and the line above it.</p>
    <hr>
<!-- Modify the list to create a shopping list, eg <dt>dairy</dt> and <dl> dairy items -->
    <dl>
      <dt>dl</dt>
      <dd>dl is a <strong>description list</strong> and contains terms and description elements.</dd>
      <dt>dt</dt>
      <dd>dt is a term or heading, like the one above. There can be many dt elements inside a description list</dd>
      <dt>dd</dt>
      <dd>dd is data or item, like this one.</dd>
      <dd>You can have more than one dd element under each dt element.</dd>
    </dl>
    <hr>
<!-- Change the list below into an ordered list by replacing <ul> ... </ul> with <ol> ... </ol> -->
    <ul>
      <li>I am the first item in an unordered list</li>
      <li>I am the second item in an unordered list</li>
      <li> ... </li>
      <li>I am the last item in an unordered list</li>
    </ul>
    <hr>
<!-- Update the <b> ... </b> elements to <strong> ... </strong> and the <i> ... </i> elements to <em> ... </em> -->
    <p>I am another paragraph with <b>bold</b>, <i>italic</i> and <b><i>bold italic</i></b> text.</p>
    <hr>
  </body>
</html></textarea><br><input type='button' value='Try it'/> 
    </section>
  </section>
  <section>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Table Element</h1>
      <p>The <code>&lt;table&gt;</code> element allows us to output <strong>tabular data</strong> in a webpage.  The table rows <code>&lt;tr&gt;</code> contain <strong>table cells</strong>, either <strong>header</strong> cells <code>&lt;th&gt;</code> or <strong>data</strong> cells <code>&lt;td&gt;</code>, with content inside.</p>
      <p>A table can be divided into three semantic parts to contain <strong>table rows</strong>: a header <code>&lt;theader&gt;</code>, a body <code>&lt;tbody&gt;</code>, a footer <code>&lt;tfooter&gt;</code>. It can also have a caption <code>&lt;caption&gt;</code>.</p>
      <p>The table cells can span columns (ie expand <strong>vertically</strong>) with a <code>colspan</code> attribute, or span rows with a <code>rowspan</code> attribute.</p>
      <pre><code>&lt;table&gt;
  <span>&lt;caption&gt; ... &lt;/caption&gt;</span>
  &lt;theader&gt;
    <span>&lt;tr&gt;
      &lt;th&gt; ... &lt;/th&gt;
    &lt;/tr&gt;</span>
  &lt;/theader&gt;
  &lt;tbody&gt;
    <span>&lt;tr&gt;
      &lt;td&gt; ... &lt;/td&gt;
    &lt;/tr&gt;</span>
  &lt;/tbody&gt;
  &lt;tfooter&gt;  
    <span>&lt;tr&gt;
      &lt;th rowspan='3'&gt; ... &lt;/th&gt;
    &lt;/tr&gt;</span>
  &lt;tfooter&gt;  
&lt;/table&gt;</code></pre>      
    </section>
  </section>
    
  <section>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Table Example</h1>      
      <p></p>
      <div class='trev-tryit'>
<textarea rows=15 onblur='tryItBuffer(this.value)'><!doctype html>
<html>
  <head>
    <title>Table Example</title>
    <meta charset="utf-8">
  </head>
  <body>  
    <p>Here is a table element. It contains a <strong>caption</strong> element, 5 <strong>rows</strong> and each row contains a mix of <strong>table heading</strong> and <strong>table data</strong> cells.</p>
<!-- Please note border, sellspacing and cellpadding are outdated styling attributes and should not be used in a HTML5 document.  -->
    <table border=1 cellspacing=5 cellpadding=20>
      <caption>Maths is <s>fun</s> bad!!</caption>
      <thead>
        <tr>
          <th>X</th><th>1</th><th>2</th><th>3</th><th>4</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th><td>1</td><td>2</td><td>3</td><td>4</td>
        </tr>
        <tr>
          <th>2</th><td>2</td><td>4</td><td>6</td><td>8</td>
        </tr>
        <tr>
          <th>3</th><td>3</td><td>6</td><td>9</td><td>12</td>
        </tr>
        <tr>
          <th>4</th><td>4</td><td>8</td><td>12</td><td>16</td>
        </tr>
      </tbody>
      <tfooter>
<!-- create a totals row here using <tr> and <th> elements. We will cover rowspan and colspan in the tutes. -->        
      </tfooter>
    </table>
  </body>
</html></textarea><br><input type='button' value='Try it'/> 
      </div>
    </section>
  </section>  
  <section>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>HTML Entities</h1>
      <p>In a webpage, such as this one, if you want to refer to a paragraph tag like this: <code>&lt;p&gt;</code>, you may fall into the trap of coding just <strong>"&lt;p&gt;"</strong>. Unfortunately, if you do this, the browser will interpret the tag as markup and not render it the way you want because the <code>&lt;</code> and <code>&gt;</code> characters are reserved to mark out tags. We call these sorts of characters <strong>meta-characters</strong> because they 'mean something'.</p>
      <p>If you want to use the <strong>literal</strong> versions of these characters in a webpage, you need to use <strong>HTML Entities</strong> which are escape sequences beginning with an ampersand character <code>&amp;</code> and ending with a semi colon character <code>;</code> .</p>
      <p>If you view the source code of this slide, you will see many examples of these:</p>
      <p><img src='app/img/htmlentitycode.png' alt='screenshot of this slide'><p>
      <p>In the source code, you will also see that to display</p>
      <ul>
        <li><code>&amp;</code> you need the code <code>&amp;amp;</code></li>
        <li><code>&amp;amp;</code> you need the code <code>&amp;amp;amp;</code></li>
        <li><code>&amp;amp;amp;</code> you need the code <code>&amp;amp;amp;amp;</code></li>
        <li> ... </li>
        <li>This list will continue on to <code>&infin;</code> (infinity).</li>
      </ul>
    </section>
  </section>
    
  <section>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>HTML Entities</h1>
      <p><a href="https://dev.w3.org/html5/html-author/charref">HTML entites</a> are also used to encode unusual characters not found on keyboards such as:</p>
      <dl>
        <dt>Bullet points and arrows</dt>
        <dd><code>&darr; &varr; &bull; &rtrif;<br>&amp;<span>darr</span>; &amp;<span>varr</span>; &amp;<span>bull</span>; &amp;<span>rtrif</span>;</code></dd>
        <dt>Mathematical Symbols</dt>
        <dd><code>&pi; &conint; &sime; &sube;<br>&amp;<span>pi</span>; &amp;<span>conint</span>; &amp;<span>sime</span>; &amp;<span>sube</span>;</code></dd>
        <dt>Commercial marks</dt>
        <dd><code>&trade; &copy; &reg; &oS;<br>&amp;<span>trade</span>; &amp;<span>copy</span>; &amp;<span>reg</span>; &amp;<span>oS</span>;</code></dd>
        <dt>Accented and unusual letters</dt>
        <dd><code>&Ccedil; &Euml; &Oslash; &THORN;<br>&amp;<span>Ccedil</span>; &amp;<span>Euml</span>; &amp;<span>Oslash</span>; &amp;<span>THORN</span>;</code></dd>
        <dt>Fractions</dt>
        <dd><code>&half; &frac34; &frac13; &frac25;<br>&amp;<span>half</span>; &amp;<span>frac34</span>; &amp;<span>frac13</span>; &amp;<span>frac25</span>;</code></dd>
      </dl>
    </section>
  </section>
  <section>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>UTF-8 Character Encoding</h1>
      <p>However, special character encoding is becoming less of a problem as character encoding in HTML5 is UTF-8 by default. This means that all unicode characters (currently over 1.1 million) can be represented in a web document.</p>
      <dl>
        <dt>One Byte (128 characters)</dt>
        <dd>Includes original ASCII keyboard characters</dd>
        <dt>Two Bytes (1,920 characters)</dt>
        <dd>Includes most European and Middle Eastern characters</dd>
        <dt>Three Bytes (63,488 characters)</dt>
        <dd>Includes most Asian language characters</dd>
        <dt>Four Bytes (can accomodate 2,031,616 characters)</dt>
        <dd>Includes everything else, obscure Asian language characters, ancient Mayan and Egyptian hieroglyphs; and recently emojies were added to the list! We can now turn <code>:-D</code> into <code>😋</code>.</dd>
      </dl>
      <div class='modal-block'>
        <div><p>Click here to watch Tom Scott from Computerphile explain how the Unicode and UTF-8 character encoding standards work in detail and the problems that they solved when the World Wide Web took off.</p></div>
        <div><?php youtube('//www.youtube.com/embed/MijmeoH9LT4'); ?><p>In this next video, Tom explains why Microsoft&trade; smart quotes and other characters donâ€™t always copy and paste correctly ... 😠: <?php a('youtube.com/watch?v=qBex3IDaUbU'); ?></p><div>Click here to close</div></div>
      </div>
      <p><strong>Extra:</strong> Click <a href="http://unicode.org/standard/supported.html" target="_blank">here</a> to read more about Unicode, or to view a graph of estimated encoding usage on the web, click <a href="https://w3techs.com/technologies/overview/character_encoding/all" target="_blank">here</a>.</p>
    </section>
  </section>
  <section>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Block and Inline Elements</h1>
      <p title='This paragraph is a block element, as is the one below'>By now you should be aware that some HTML elements are <strong>block</strong> elements, like <strong>headings</strong>, <strong>paragraphs</strong>, <strong>horizontal rules</strong>, <strong>lists</strong> and <strong>list elements</strong>, <strong>tables</strong> and <strong>table rows</strong>. These are generally recognised as 'taking up the whole width of the window', for example this paragraph.</p>
      <p>Other elements are <strong title='This strong is an inline element'>inline</strong> elements, such as <strong title='This strong is an inline element'>strong text like this</strong>, <em title='This em is an inline element'>emphasised text like this</em>, <a title='This anchor is an inline element' href=#>anchor elements like this</a>, <strong>table cells</strong>, <strong>images</strong>, <strong>line breaks</strong> are rendered <strong>inline</strong> and many can occupy the same line, perhaps even breaking across lines if they reach the end of the window. <em title='This em is an inline element and may be long enough to break over a line'>NB: &lt;th&gt; and &lt;td&gt; elements don't break across lines, but will share a line with other &lt;th&gt; and &lt;td&gt; elements inside a &lt;tr&gt; element.</em>.</p>
      <blockquote>Some of the elements above have <code>title</code> attributes that explain their block / inline status. Move your mouse over the elements to see more.</blockquote>
    </section>
  </section>
  <section>
    <header>HTML Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Grouping Elements</h1>
      <p><img class='fr' src='app/img/qualitystreet.png' alt='Quality Street chocolate box, demonstrating that the box is like a div, wrappers are like spans and chocolates are like the content'/>By the time the HTML4 standard was in development, it was clear that web documents had evolved beyond simple "word processing" documents. Two <strong>context-free containers</strong> were introduced called <code>&lt;div&gt;</code> and <code>&lt;span&gt;</code> so that content could be grouped together in logical areas within a webpage.</p>
      <p><i>In some ways, a <code>&lt;div&gt;</code> is like a <strong>box of chocolates</strong> and <code>&lt;span&gt;</code>s are like <strong>individually wrapped chocolates</strong> (see image).</i></p>
      <p><code>&lt;div&gt;</code> is a <strong>block</strong> element that can be used to group other block and inline elements inside. Divs can be targeted with CSS or Javascript. Click on the div element below to see more:</p>
      <div onclick="this.style.backgroundColor='#FAF';">Hello, I am a div. Click on me to find out how wide I really am!</div>
      <p>Whereas <code>&lt;span&gt;</code> is an <strong>inline</strong> element that can be used to group other inline elements inside. Spans can be targeted with CSS or Javascript. Click on the span elements below to see more:</p>
      <p>Hello, I am a paragraph and I contain a number of span elements. Here is the first one: "<span onclick="this.style.backgroundColor='#FAF';">Hello, I am span number 1. Click on me to see where I begin and where I end!</span>", thanks span#1, here is the second span: "<span onclick="this.style.backgroundColor='#FAF';">Just like Buzz Aldrin, I come right after number 1. Click on me to see where I begin and end!</span>", thanks span#2, and here is the third span: "<span onclick="this.style.backgroundColor='#FAF';">Thanks for making it this far. I feel a bit like like Michael Collins, a vital part of the Apollo 11 mission, but I didn't get to walk on the moon :-( Click on me anyway!</span>", thanks span#3.</p>
    </section>
  </section>
  
  <section id='tp-classid' class='key'>
    <header>Identifying Attributes<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Attributes: id &amp; class</h1>
      <p>If you view the source code, you will see that this slide has an <code>id</code> and a <code>class</code> attribute.</p>
      <p><img src='app/img/classid.png' alt='Sourcecode example of class and id' /></p>
      <p>The <code>id</code> attribute is an optional attribute designed to uniquely identify an element.</p>
      <ul>
        <li>An element should only have one id.</li>
        <li>No two elements in a document should have the same id.</li>
      </ul>
      <p>Think about your student ID. You should only have one, and no two students at RMIT should ever share the same SID:</p>
      <p><code>&lt;p id='<span><?php echo SID; ?>'</span>&gt;There can only be one!&lt;/p&gt;</code></p>
      <p>The <code>class</code> attribute, also optional, identifies a <strong>set</strong> of elements, and elements can belong to more than one class.</p>
      <ul>
        <li>An element can have more than one class.</li>
        <li>Elements in a document can share the same class.</li>
      </ul>
      <p>Think about your classes and class-mates. Students should have many classes and many class-mates:</p>
      <pre><code>&lt;p id='<span><?php echo SID; ?>'</span> class='<span>COSC2413 COSC1078 COSC2625</span>'&gt;I know you from somewhere ...&lt;/p&gt;
&lt;p id='<span>s1234568'</span> class='<span>COSC2413 COSC1078 COSC2625</span>'&gt;We are in Intro to IT too&lt;/p&gt;
&lt;p id='<span>s1234569'</span> class='<span>COSC2413 COSC1078 COSC2625</span>'&gt;Don't forget BITS!&lt;/p&gt;</code></pre>
      <p><i>The need for these identifying attributes will become clear when using <strong>CSS</strong> and <strong>javascript</strong>.</i></p>
    </section>
  </section>  
</article>
