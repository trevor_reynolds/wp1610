<?php
  $counter=1;
  $slideText='Lect 3, P.';
?>
<article id='slides-lecture-3'>

  <section class='title'>
    <header>Welcome!<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Web Programming<br>COSC2413/2426/2453</p>
      <p>Lecture 3</p>
    </section>
  </section>
  
  <section class='title'>
    <header>Lecture 3.1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 1: Cascading Style Sheets</p>
    </section>
  </section>

  <section class='key'>
    <header>Cascading Style Sheets<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-csshistory'>History of CSS</a></li>
        <li><a href='#tp-cssorigins'>CSS Origins</a></li>
        <li><a href='#tp-cssconcept'>CSS Concepts</a></li>
        <li><a href='#tp-cssinclude'>Including CSS in a Web Document</a></li>
      </ol>
    </section>
  </section>

  <section class='key' id='tp-csshistory'>
    <header>History of CSS<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Structure v/s Presentation</h1>
      <p>In the early days of the Web, the main concern was delivery of information, with substance very much more important than style.</p>
      <p>The early design of HTML reflected that authors had very little control over presentation. Most documents were scientific and technical documents.</p>
      <p>Various techniques were tried to make Web pages look better.</p>
      <ul>
        <li>Converting text to images.</li>
        <li>Using images, empty tags, non-breaking white space for whitespace control and spacing.</li>
        <li>Using tables for page layout.</li>
        <li>Writing programs instead of using HTML.</li>
        <li>Using <strong>style / presentation</strong> tags to change color, font, weight and text position.</li>
      </ul>
      <p class='c r'><strong>These techniques are bad!<br>Please do **NOT** do this in your assignments!</strong></p>
    </section>
  </section>

  <section>
    <header>History of CSS<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Structure v/s Presentation</h1>
      <p>Even simple styling such as <strong>spacing</strong>, <strong>bold</strong>, <i>italic</i> and <strong>centered</strong> text should be done using CSS when not using semantic elements such as <strong>&lt;strong&gt;</strong> and <strong>&lt;em&gt;</strong>.</p>
      <p>This is important from a programming and maintainenance perspective: mixing <strong>structure</strong> with <strong>presentation</strong> has long been considered bad programming practice and can create "spagetti" like code.</p>
      <p>CSS makes it easy to specify all the presentation: positioning, white-space, padding, margin; font family, weight, size; colors used for text, background; hover states etc.</p>
      <p>A single stylesheet (or a small number of stylesheets) can be applied to an entire website and create a uniform look and feel across all pages.</p>
      <p>Designing a webpage to adapt to different devices (PC, tablet, phone etc) is made simpler using customised stylesheets for each type of device.</p>
      <p>Client browsers, particularly those used by people with disabilities, can filter out irrelevant style if not included as HTML markup. For example, <code>COLOR=<span>RED</span></code> is a deprecated style attribute that is not useful for a blind person's text reader.</p>
    </section>
  </section>

  <section>
    <header>History of CSS<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Browser Rendering Differences</h1>
      <p><img class='fl' src='app/img/wrongplug.jpg' alt='wrong electrical plug' />Internet Explorer, Opera and Firefox were all guilty of ignoring the standard set out by the W3C and did things “their own way” before 2000.</p>
      <p>But by 2010, adhering to W3C standards had made a comeback, thanks to competition from Safari and Chrome. Content today is being rendered more reliably in all browsers, but differences still exist.</p>
      <p>Web browsers are constantly evolving and do not support all CSS3 styles, particularly the advanced CSS3 styles. To make sure advanced styles work effectively in the maximum number of browsers, a developer should add <strong>vendor-prefixed</strong> styles as a fall back, as not all users have access to the most up to date versions of the browser.</p>
      <p>For example, when using the new <code>flex</code> layout style, it is advisable to include all vendor prefixes before the compliant version:</p>
      <pre><code>#element {
 <span>     -o-</span>flex: 1; <span>/* Opera */</span>
 <span>    -ms-</span>flex: 1; <span>/* Microsoft */</span>
 <span>   -moz-</span>flex: 1; <span>/* Firefox (Mozilla) */</span>
 <span>-webkit-</span>flex: 1; <span>/* Chrome and Safari */</span>
         flex: 1; <span>/* Fully compliant browsers! */</span>
</code></pre>
      <p>Even today, it is still important to check cross-browser compatibility of HTML and even simple CSS across a range of browsers before final deployment.</p>
    </section>
  </section>

  <section class='key' id='tp-cssorigins'>
    <header>Stylesheet Origins<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Stylesheet Origins</h1>
      <p>Style sheets may have three different origins: <strong>user-agent</strong>, <strong>author</strong>, <strong>user</strong>.</p>
      <ul>
        <li>The <strong>user-agent</strong>, ie the web browser manufacturer, must provide a default style sheet. This is usually black text on a white background, a larger bolder font for headings, simple dot points for lists, blue and purple underlines for hyperlinks. These settings are the first to be overwritten.</li>
        <li>The <strong>author</strong>, ie the developer, specifies style and stylesheets for a source document, defining a preferred presentation. This is the only area of CSS we have any control over and is the focus of this course.</li>
        <li>The <strong>user</strong>, ie the client, may specify a style sheet. For example, people with  vision impairment will use a high contrast stylesheet that overwrites any styles we supply.</li>
      </ul>
    </section>
  </section>  

  <section>
    <header>Stylesheet Origins<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>User-Agent v/s Author Styles</h1>
      <p>We will look at <strong>embeded stylesheets</strong> shortly, but for demonstration purposes, the following webpage contains an embedded stylesheet that removes the default <strong>user-agent</strong> styles in the browser you are using and replaces them with one uniform <strong>author</strong> style.</p>
      <p>To view the plain user-agent styles, delete all of the <strong>author</strong> style contained between the <code>&lt;style&gt;</code> tags in the <code>&lt;head&gt;</code> element in the text area below before clicking the "Try it" button.</p>
      <div class='modal-block'>
        <div><p>Alternatively, right-click the pop-up page, select "inspect element" and turn on and off the styles by clicking the style text boxes. <i>Click here for a screenshot.</i></p></div>
        <div>
          <p><img src='app/img/inspect-user-agent-css.png' alt='screenshot of console and reset styles' /></p>
          <div>Ok, got it! Ready to try it.</div></div>
      </div>
      <div class='trev-tryit'>
<textarea rows=15 onblur='tryItBuffer(this.value)'><!doctype html>
<html>
  <head>
    <title>User-Agent Example</title>
    <meta charset="utf-8">
    <style>
      /* Start of Embedded Author Stylesheet */
      * {
        font-size:1em;
        font-weight:normal;
        line-height:1;
        text-decoration:none;
        color:black;
        padding:0px;
        margin:0px;
      }
      /* End of Embedded Author Stylesheet */
    </style>
  </head>
  <body>  
    <h1>H1</h1>
    <h2>H2</h2>
    <h3>H3</h3>
    <h4>H4</h4>
    <h5>H5</h5>
    <h6>H6</h6>
    <p>Paragraph with a <a href=''>hyperlink</a> in it.</p>
    <ol>
      <li>Ordered list element</li>
      <li>Ordered list element</li>
      <li>Ordered list element</li>
      <li>Ordered list element</li>
    </ol>
    <ul>
      <li>Unordered list element</li>
      <li>Unordered list element</li>
      <li>Unordered list element</li>
      <li>Unordered list element</li>
    </ul>
  </body>
</html></textarea><br><input type='button' value='Try it'/> 
      </div>
    </section>
  </section>  

  <section>
    <header>Stylesheet Origins<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>User Styles</h1>
      <p>Below is an example of what <strong>Google's</strong> home page looks like using a high contrast <strong>user</strong> stylesheet. <strong>User</strong> styles override all styles set down by the <strong>user-agent</strong> (the browser) and the <strong>author</strong> (the developer).</p>
      <p><img src='app/img/vision-impaired-css.png' alt='high contrast screenshot' /></p>
      <p>It is important to make sure that your website always caters for people with vision difficulties. <strong><i>Do not assume the visually impaired will have access to their own stylesheet!</i></strong> Even knowledgable users will have to use un-customised computers periodically.</p>
    </section>
  </section>  
  
  <section class='key' id='tp-cssconcept'>
    <header>CSS Concepts<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>A CSS Property</h1>
      <p>CSS properties are set by specifying a property and a value pair, followed by a semi-colon:</p>
      <pre><code>property: <span>value</span>;</code></pre>
      <p>Examples (most effects are self-explanatory):</p>
      <ul>
        <li><code>color: <span>green</span>;</code> (text becomes green)</li>
        <li><code>background-color: <span>cyan</span>;</code></li>
        <li><code>font-size: <span>xx-large</span>;</code></li>
        <li><code>text-align: <span>center</span>;</code></li>
        <li><code>border: <span>thick red dotted</span>;</code></li>
        <li><code>padding: <span>2px 10px</span>;</code></li>
      </ul>
    </section>
  </section>

  <section>
    <header>CSS Concepts<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Selector, Property and Value</h1>
      <p>A <strong>selector</strong> in a style sheet contains one or more CSS properties within a pair of curly braces:</p>
      <pre><code>selector { <span>property: <span>value</span></span>; ... }</code></pre>
      <p>Example (styles all paragraphs in a document):</p>
      <pre><code>p {<span>
  color: <span>green</span>;
  background-color: <span>khaki</span>;
  font-size: <span>xx-large</span>;
  text-align: <span>center</span>;
  width: <span>80%</span>;
  border: <span>thick red dotted</span>;
  padding: <span>2px 10px</span>;</span>
}</code></pre>
      <p class='cssconcept'>The above styles would normally be applied to all paragraphs, but to demonstrate the concept, only this paragraph is styled.</p>
    </section>
  </section>

  <section>
    <header>CSS Concepts<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Inheritance</h1>
      <p>Property values are normally inherited by child elements, but this is not true for all styles. More often than not, if it "makes sense", styles are inherited.</p>
      <p>An example: suppose there are several <strong>&lt;em&gt;</strong> elements inside the paragraph on the previous slide. If no styles have been specified for the <strong>&lt;em&gt;</strong> element, they will inherit many of the parent paragraph's styles such as size, color, font and alignment:</p>
      <p class='cssconcept'>The above styles would <em>normally</em> be applied to <em>all</em> paragraphs, but to <em>demonstrate</em> the concept, only <em>this</em> paragraph is styled.</p>
      <p>To force an element to inherit a particular style from the parent, use the <code>inherit</code> property value. As we will see soon, color is not always inherited:</p>
      <pre><code>strong { color: <span>inherit</span>; }</code></pre>
    </section>
  </section>

  <section>
    <header>CSS Concepts<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Grouping</h1>
      <p>When several selectors share the same declarations, they may be grouped as a comma-separated list:</p>
      <pre><code>h1 {<span>
  font-family: <span>sans-serif</span>;</span>
}
h2 {<span>
  font-family: <span>sans-serif</span>;</span>
}
h3 {<span>
  font-family: <span>sans-serif</span>;</span>
}</code></pre>
      <p>Can be grouped like this:</p>
      <pre><code>h1, h2, h3 {<span>
  font-family: <span>sans-serif</span>;</span>
}</code></pre>
      <p>Each of the three largest heading elements will be styled with the plain "sans-serif" font.</p>
    </section>
  </section>

  <section>
    <header>CSS Concepts<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Styling id and class Elements</h1>
      <p>Styling an element with a particular id is possible using the <code>#</code> character and the id value:</p>
      <pre><code>#<?php echo SID; ?> {
  <span>color: red;</span> 
}</code></pre>
      <pre><code>&lt;p id='<span><?php echo SID; ?></span>'&gt;"Your" paragraph will have red text.&lt;/p&gt;</code></pre>
      <p class='r'>"Your" paragraph will have red text.</p>
      <p>Styling elements with a particular class is possible using the <code>.</code> character and the class value:</p>
      <pre><code>.strike {
  <span>text-decoration: line-through;</span> 
}</code></pre>
      <pre><code>&lt;p class='<span>strike</span>'&gt;This paragraph will be struck out.&lt;/p&gt;</code></pre>
      <p class='strike'>This paragraph will be struck out.</p>
      <pre><code>&lt;p id='<span><?php echo SID; ?>'</span> class='<span>strike</span>'&gt;Both styles are applied: red and struck out!&lt;/p&gt;</code></pre>
      <p class='r strike'>Both styles are applied: red and struck out!</p>
    </section>
  </section>

  <section>
    <header>CSS Concepts<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Comments</h1>
      <p>You can have multi-line comments inside a CSS by placing comments between <code>/*</code> and <code>*/</code> markers.</p>
      <pre><code>/* <span>Code sourced and adapted from:
w3schools.com/css/css_syntax.asp</span> */
p {
  color: red;
  text-align: center;
}</code></pre>
      <p>Unfortunately, commonly used single line comments found in other languages are not supported in CSS:</p>
      <pre><code>// <span>This doesn't work and will generate an error in CSS.</span> 
 # <span>Same is true for this attempt at a single line comment.</span></code></pre>
    </section>
  </section>

  <section class='key' id='tp-cssinclude'>
    <header>Including CSS in a Web Document<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>CSS - Locations</h1>
      <p>There are three ways of adding CSS information to your web page:</p>
      <ul>
        <li><strong>INLINE</strong>: no better than markup, <em>avoid it!</em></li>
        <li><strong>EMBEDDED</strong>: applies to one page only.</li>
        <li><strong>EXTERNAL</strong>: applies to many pages, <em>use it!</em></li>
      </ul>
    </section>
  </section>

  <section>
    <header>Including CSS in a Web Document<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>CSS: Inline</h1>
      <p>Style directives can be attached directly to a single HTML element using the <code>style=<span>'...'</span></code>attribute:</p>
      <pre><code>&lt;p&gt;The next paragraph will be styled using &lt;strong&gt;inline&lt;/strong&gt; CSS:&lt;/p&gt;
&lt;p style='<span>font-size: xx-large; font-weight: bold; color: red;</span>'&gt;
   Styled using inline style.
&lt;/p&gt;</code></pre>
  <p>The next paragraph will be styled using <strong>inline</strong> CSS:</p>
  <p style="font-size: xx-large; font-weight: bold; color: red;">Styled using inline style.</p>
    </section>
  </section>

  <section>
    <header>Including CSS in a Web Document<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>CSS: Embedded</h1>
      <p>Style directives can be attached directly to a single page and apply to all HTML elements in that page.</p>
      <p>The style sheet is <strong>embedded</strong> inside the <strong>&lt;head&gt;</strong> element of a document between <code>&lt;style&gt;</code> tags:</p>
      <pre><code>&lt;head&gt;
  ...<span>
  &lt;style&gt;<span>
  
  /* Style directives go here */
  
    p { color:red; }
    #<?php echo SID; ?> { ... }
    ...
  /* End of stylesheet */</span>
  
  &lt;/style&gt;</span>
  ...
&lt;/head&gt;</code></pre>
    </section>
  </section>    

  <section>
    <header>Including CSS in a Web Document<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>CSS: External</h1>
      <p>The best way to include style into a web document is using <strong>external</strong> stylesheets in  <strong>separate files</strong>. The recognised file extension for a stylesheet is <code>.css</code>.</p>
      <p>The stylesheet is <strong>included</strong> inside the <strong>&lt;head&gt;</strong> element with a <code>&lt;link&gt;</code> element:</p>
      <pre><code>&lt;head&gt;
  ...<span>
  &lt;link rel='<span>stylesheet</span>' href='<span>mystyle.css</span>' type='<span>text/css</span>' /&gt;</span>
  ...
&lt;/head&gt;</code></pre>

      <p>This allows you to link common stylesheets to all of your pages and create a uniform look and feel across all webpages in your website.</p>
      <p>For example, these slides include five external stylesheets:</p>
      <ul>
        <li><?php a('app/css/colorborder.css','colorborder.css'); ?>: for color and border styles.</li>
        <li><?php a('app/css/layout.css','layout.css'); ?>: for styles that affect layout.</li>
        <li><?php a('app/css/widgets.css','widgets.css'); ?>: to style demos / widgets in slides.</li>
        <li><?php a('app/css/print.css','print.css'); ?>: for styles that are applied when using a printer (A4 size).</li>
        <li><?php a('app/css/testing.css','testing.css'); ?>: to test new styles, or debug problematic styles.</li>
      </ul>
    </section>
  </section> 

  <section>
    <header>Including CSS in a Web Document<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>External CSS: A Common Mistake</h1>
      <p>When you create an external stylesheet, just write simple declarations inside that file. Do not embed them inside <strong>&lt;style&gt;</strong> tags:</p>
      <pre><code><span class='strike'>&lt;style&gt;</span><span> ← Don't put this tag in a .css file</span>
  h1 { font-weight: bold; }
  p { font-size: 12pt; }
  span { color: red; }
<span class='strike'>&lt;/style&gt;</span><span> ← Don't put this tag in a .css file</span>
      </code></pre>
    </section>
  </section>   

  <section class='title'>
    <header>Lecture 3.2<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 2: Advanced Selectors &amp; Color</p>
    </section>
  </section>    
    
  <section class='key'>
    <header>Cascading Style Sheets<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-csscontext'>CSS Contextual Selectors</a></li>
        <li><a href='#tp-csspseudo'>CSS Pseudo Selectors</a></li>
        <li><a href='#tp-cssadvanced'>CSS Advanced Selectors</a></li>
        <li><a href='#tp-csscascade'>Cascading Nature of CSS</a></li>
        <li><a href='#tp-csscolor'>Color Models</a></li>
      </ol>
    </section>
  </section>        
    
  <section class='key' id='tp-csscontext'>
    <header>Contextual Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Descendent Selectors</h1>
      <p><strong>Contextual</strong> or <strong>Combinator</strong> selectors allow us to select smaller groups of elements rather than styling all elements of a particular type.</p>
      <p>The <strong>descendent</strong> selector starts with the <strong>topmost</strong> element and is followed by one or more <strong>descendent</strong> elements. For example:</p>
      <pre><code>nav a {<span> color: <span>red</span>;</span> }</code></pre>
      <p>All <strong>&lt;a&gt;</strong>s inside a <strong>&lt;nav&gt;</strong> element have red text color.</p>
      <pre><code>main a { <span>color: <span>green</span>;</span> }</code></pre>
      <p>All <strong>&lt;a&gt;</strong>s inside a <strong>&lt;main&gt;</strong> element have green text.</p>
      <pre><code>main li em {<span> color: <span>purple</span>;</span> }</code></pre>
      <p>All <strong>&lt;em&gt;</strong>s inside <strong>&lt;li&gt;</strong> elements  inside the <strong>&lt;main&gt;</strong> element will have a purple text.</p>
      <p><i>In these slides, the code examples are colored* by nested spans:</i></p>
      <pre><code>code {<span> color: <span>blue</span>;</span> }
code span {<span> color: <span>red</span>; </span>}
code span span {<span> color: <span>green</span>; </span>}</code></pre>
      <p><i>*Colors are approximate, we will look at colors in more detail shortly!</i></p>
    </section>
  </section>

  <section>
    <header>Contextual Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Direct Descendent Selectors</h1>
      <p>We can also select <strong>direct</strong> descendents, that is just the <strong>child</strong> elements and not their child elements using the <code>&gt;</code> character instead of a single space:</p>
      <pre><code>p > em {<span> color: <span>red</span>;</span> }
</code></pre>
      <p>Will style <code>&lt;em&gt;this element&lt;/em&gt;</code> but not <code>&lt;strong&gt;&lt;em&gt;this element&lt;/em&gt;&lt;/strong&gt;</code> because it is a child of a <strong>&lt;strong&gt;</strong> element.</p>
    </section>
  </section>
    
  <section>
    <header>Contextual Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Sibling Selectors</h1>
      <p>We can select all elements that are <strong>preceeded</strong> by a <strong>sibling</strong> element using the <code>~</code> character:</p>
      <pre><code>section pre ~ p {<span> color: <span>saddlebrown</span>;</span> }</code></pre>
      <p class='sb'>Because this <strong>paragraph</strong> is in a <strong>section</strong>, and has a <strong>preformatted</strong> sibling element before it, the text will be <strong>saddlebrown</strong>.</p>
      <p class='sb'>The same applies to this subsequent paragraph!</p>
    </section>
  </section>    

  <section>
    <header>Contextual Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Adjacent Selectors</h1>
      <p>We can also select an element that directly follows another element using the <code>+</code> character:</p>
      <pre><code>p + p {<span> color: <span>purple</span>;</span> }
p + p + p {<span> color: <span>red</span>;</span> }</code></pre>
      <p>This paragraph won't be styled because it follows a <strong>&lt;pre&gt;</strong> element.</p>
      <p class='p'>This paragraph will have purple text because it follows a paragraph.</p>
      <p class='r'>This paragraph follows two paragraphs, so the "more specific" <strong>adjacent</strong> selector is applied and text will be red.</p>
      <p class='r'>This paragraph also follows two paragraphs, so text will be red, unless a more specific <strong>adjacent</strong> selector is present.</p>
    </section>
  </section>    
    
  <section class='key' id='#tp-csspseudo'>
    <header>Pseudo Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Pseudo Class: Styling Hyperlinks</h1>
      <p>Pseudo selectors select <strong>entire elements</strong> depending on their <strong>state</strong>. This is referred to as a <strong>pseudo class</strong> selector.</p>
      <p>The <code>:</code> character is re-used to seperate the element from the state. <i>Make sure you don't use a space!</i></p>
      <pre><code>selector:<span>state</span> { <span>property: <span>value</span>;</span> }</code></pre>
      <p>The most common application is to style <a href=''>hyperlinks</a> to respond to user mouse movements.</p>
      <p>For example, <strong>unvisited</strong> and <strong>visited</strong> links in the main part of these slides are styled with a darker red color:</p>
      <pre><code>a:<span>link</span>, a:<span>visited</span> {
  <span>color: <span>darkred</span>;</span>
}</code></pre>
      <p>And when a link has a mouse <strong>hovering</strong> over it or when it is <strong>clicked</strong> are styled with a brighter red color:</p>
      <pre><code>a:<span>hover</span>, a:<span>active</span> {
  <span>color: <span>red</span>;</span>
}</code></pre>
      <p><i>Please note that touch screens typically cannot detect a finger-based hover state.</i></p>
    </section>
  </section>

  <section>
    <header>Pseudo Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Styling Position</h1>
      <p>Pseudo class selectors can also select elements depending on where they are <strong>located</strong> in the document. For example, a first, a last, an 'nth' or every ‘nth’, odd and even elements (useful for "striped" table rows).</p>
      <pre><code>li:<span>first-child</span> { ... }
li:<span>last-child</span> { ... }
li:<span>nth-child(7)</span> { ... } ← 7th list item
li:<span>nth-child(2n)</span> { ... } ← even numbered items
li:<span>nth-child(2n+1)</span> { ... } ← odd numbered items</code></pre>
      <div class='trev-tryit'>
<textarea rows=15 onblur='tryItBuffer(this.value)'><!doctype html>
<html>
  <head>
    <title>Styling an Ordered List</title>
    <meta charset="utf-8">
    <style>
      /* Put your CSS styles in this inline style sheet */
      li:hover {
        background-color:yellow;
      }
      /* Contextual selector: select only ordered list items */
      ol li:first-child {
        color:red;
      }
    </style>
  </head>
  <body>  
    <h3>Ordered List and Unordered List.</h3>
    <p>This webpage has an <strong>inline</strong> stylesheet (above). You should put all your styles between the style tags</p>
    <p>All list elements have a hover state, but you should try to style <strong>only</strong> the ordered list elements with the styles you have covered so far.</p>
    <p><i>To get you started, the first element has been styled with a first-child pseudo style in the inline style sheet above.</i></p>
    <p></p>
    <ol>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
      <li>Ordered List Element</li>
    </ol>
    <ul>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
      <li>Unordered List Element</li>
    </ul>
  </body>
</html></textarea><br><input type='button' value='Try it'/> 
      </div>
    </section>
  </section>        

  <section>
    <header>Pseudo Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Pseudo Elements</h1>
      <p>What we have seen are <strong>pseudo classes</strong>, that is where pseudo style is applied to an <strong>entire</strong> element based on its state.</p>
      <p>There are times where we apply pseudo style to an element and in doing so we create a new <strong>sub-element</strong>. These <strong>sub-elements</strong> are called <strong>pseudo elements</strong>.</p>
      <p>Developers should use two colons in CSS3 <code>::</code> when designating a pseudo element selector, but this was unnecessary in previous versions of CSS:</p>
      <pre><code>selector::<span>sub-element</span> { <span>property: <span>value</span>;</span> }</code></pre>
      <p>Why the developers of CSS3 made a decision to create a distinction between <strong>pseudo classes</strong> and <strong>pseudo elements</strong> is a puzzling question, especially when it was not required before. Most browsers are smart enough and kind enough to render pseudo element styles when only one colon is used. Hopefully this decision will be reversed in a later version of CSS.</p>
    </section>
  </section>        

  <section>
    <header>Pseudo Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>First line and First letter</h1>
      <p>To style the first line of a paragraph use <code>::first-line</code>:</p>
      <pre><code>p<span>::first-line</span> {
<span><span>  color: darkcyan;
  font-style: italic;
  font-size: 120%;</span></span>
}</code></pre>
      <p class='csspseudofl'>Why the developers of CSS3 made a decision to create a distinction between <strong>pseudo classes</strong> and <strong>pseudo elements</strong> is a puzzling question, especially when it was not required before. Most browsers are smart enough and kind enough to render pseudo element styles when only one colon is used. Hopefully this decision will be reversed in a later version of CSS.</p>
            <p>To style the first letter of a paragraph use <code>::first-letter</code>:</p>
      <pre><code>p<span>::first-letter</span> {
<span><span>  color: darkcyan;
  font-size: 200%;</span></span>
}</code></pre>
      <p class='csspseudoflt'>Why the developers of CSS3 made a decision to create a distinction between <strong>pseudo classes</strong> and <strong>pseudo elements</strong> is a puzzling question, especially when it was not required before. Most browsers are smart enough and kind enough to render pseudo element styles when only one colon is used. Hopefully this decision will be reversed in a later version of CSS.</p>
    </section>
  </section>  
  
  <section>
    <header>Pseudo Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Selection</h1>
      <p>To change the <strong>selected</strong> text color in a paragraph, use <code>::selection</code>:</p>
      <pre><code>p<span>::selection</span> { <span><span>
  color: rgba(0,0,0,0);</span></span>
}</code></pre>
      <p class='cssdontcopy'>Try selecting text in this paragraph. Can you see it disappearing? Do you think this style will stop people copying your work?</p>
      <p><i>We will look at <code>rgba(<span>...</span>)</code> color later in the lecture</i>.</p>
    </section>
  </section>  
  
  <section>
    <header>Pseudo Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Before and After, plus Content and Url</h1>
      <p>To insert content <strong>before</strong> and <strong>after</strong> a paragraph, use <code>::before</code>, <code>::after</code> and <code>content: <span>" ... "</span></code>:</p>
      <pre><code>p<span>::before</span>, p<span>::after</span> { <span>
  content: "<span> \" </span>"; <span>
  color: darkcyan;
  font-weight: bold;
  font-size: 150%;</span></span>
}</code></pre>
      <p class='csspseudob4a'>The opening and closing quotes are inserted using CSS.</p>
      <p>If using meta characters such as quote characters in content, escape the character or unicode hex sequence with a <code>\</code> character.</p>
      <pre><code>p<span>::after</span> { <span>
  content: "<span> \2764 </span>"; <span>
  color: darkred;
  font-weight: bold;
  font-size: 150%;</span></span>
}</code></pre>
      <p class='csspseudosmile'>This paragraph uses CSS to end with a dark red heart ...</p>
      <p class='csspseudoshape'>Finally, content such as an image can be imported using <code>url(<span>...</span>)</code>:</p>
      <pre><code>p<span>::before</span> { <span>
  content: <span>url('shapeRMIT.png')</span>;
</span>
}</code></pre>                
    </section>
  </section>  

  <section id='tp-cssadvanced' class='key'>
    <header>Advanced Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Attribute Selectors</h1>
      <p>Elements can be styled by looking at their <strong>attributes</strong>. This is done using square brackets:</p>
      <pre><code>selector [ <span>attribute</span> ] { <span> ... </span> }</code></pre>
      <p>For example: <code>input[<span>required</span>] { ... }</code> will style input fields with the <strong>required</strong> attibute set so that users know they are important fields that must be filled in.</p>
      <p>You can also look at the value of the attribute using an operator and optional quotes. For example:</p>
      <p><code>input [<span>type='<span>text</span>'</span>]</code> styles just the inputs that are text boxes.</p>
      <p><code>a [<span>target='<span>_blank</span>'</span>]</code> styles  hyperlinks that will open in a new page.</p>
      <p><code>a [<span>href^='<span>http</span>'</span>]</code> styles external hyperlinks, ie hyperlinks that <strong>begin</strong> with the characters 'http'.</p>
      <p><code>a [<span>href*='<span>~<?php echo SID; ?></span>'</span>]</code> styles your website hyperlinks, ie hyperlinks that <strong>contain</strong> the characters '~<?php echo SID; ?>'.</p>
      <p><code>img [<span>alt='<span>duck</span>'</span>]</code> styles images that contain a <strong>whole word</strong> in the <strong>alt</strong> attribute (ie <strong>duck</strong> will be styled, but <strong>ducks</strong> will not).</p>
    </section>
  </section>  
    <section>
    <header>Advanced Selectors<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>HTML5 State Selectors</h1>
      <p>More pseudo states were introduced in HTML5 to make styling form elements easier, particularly form elements. For example:</p>
      <p><code>input<span>:invalid</span>, select<span>:invalid</span></code> styles input and  select elements that have invalid selections.</p>
      <details class='qna'><summary>The search box in these slides has an invalid style. Can you work out what it is and when it is triggered?</summary>One character is invalid but zero characters or more than one character is valid. Background becomes red when invalid.</details>
      <p><code>input<span>:valid</span>, select<span>:valid</span></code> ie the opposite of above, styles input and  select elements that have valid selections.</p>
      <p><code>input<span>:checked</span></code> styles radio buttons and checkboxes that have been selected. <i>Will style future elements that implement the checked attribute too.</i></p>
      <p><code>input [<span>type='<span>number</span>'</span>]<span>:out-of-range</span></code> styles number inputs with values outside the min and max range. <i>There is also a new <strong>:in-range</strong> pseudo selector you can use</i>.</p>
    </section>
  </section> 

  <section class='key' id='tp-csscascade'>
    <header>Cascading Nature of CSS<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Style Conflicts</h1>
      <p>When applying styles in a stylesheet, a browser needs to make a decision as to which style to apply. For example:</p>
      <p><code>* { <span>color:red;</span> }</code><br>
      Will render all text red.</p>
      <p><code>p { <span>color:orange;</span> }</code><br>
      Will render all paragraph text orange.</p>
      <p><code>.approved { <span>color:green;</span> }</code><br>
      Will render text in all elements of class "approved" green.</p>
      <p><code>#<?php echo SID ?> { <span>color:blue;</span> }</code><br>
      Will render text in your unique element blue.</p>
      <details class='qna'><summary><strong>Question: What color will the text be in the element below?</strong></summary>Answers are on the next slide!
      </details>
      <p><code>&lt;p class='<span>approved</span>' id='<span><?php echo SID; ?></span>'&gt;<span><span>What color will I be?</span></span>&lt;/p&gt;</code></p>      
    </section>
  </section>

  <section>
    <header>Cascading Nature of CSS<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Style Order</h1>
      <p>Ignoring the <strong>user-agent</strong> and <strong>user</strong> style settings, cascading style is determined by how <strong>specific</strong> the selector is. The more precise, the more priority the style will have:</p>
      <p><code>* { <span>color:red;</span> }</code><br>
      <strong>Very general</strong>: Will render all text red as a starting point.</p>
      <p><code>p { <span>color:orange;</span> }</code><br>
      <strong>Somewhat precise</strong>: All paragraph text will become orange, text outside paragraphs stays red.</p>
      <p><code>.approved { <span>color:green;</span> }</code><br>
      <strong>Quite precise</strong>: Text in "approved" elements will be rendered green, eg paragraphs.</p>
      <p><code>#<?php echo SID ?> { <span>color:blue;</span> }</code><br>
      <strong>Specific</strong>: One element is specified, so the element's text will be rendered blue.</p>
      <p>Links to a visually friendly calculator and the exact W3C rules for specificity are below:</p>
      <p><?php a('https://www.w3.org/TR/css3-selectors/#specificity'); ?><br><?php a('https://specificity.keegan.st/'); ?></p>

    </section>
  </section>

  <section>
    <header>Cascading Nature of CSS<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Style Order</h1>
      <p>Test it out for yourself! Delete (or comment out) different styles.</p>
      <div class='trev-tryit'>
<textarea rows=15 onblur='tryItBuffer(this.value)'><!doctype html>
<html>
  <head>
    <title>Cascade Order</title>
    <meta charset="utf-8">
    <style>
      /* Start of Embedded Author Stylesheet */
      * { color: red; }
      p { color:orange; }
      .approved { color:green; }
      #<?php echo SID; ?> { color:blue; }
      }
      /* End of Embedded Author Stylesheet */
    </style>
  </head>
  <body>  
    <h1>Plain h1 Heading. I should be red as per the * { ... } style as there are no h1 styles.</h1>
    <h2 class='approved'>Approved class h2 heading. I should be green as .approved { ... } overrides * { ... }.</h2>
    <p>Plain paragraph. I should be orange as p { ... } overrides * { ... }.</p>
    <p id='<?php echo SID; ?>'>I am the unique p element with the <?php echo SID; ?> id. I should be blue as #<?php echo SID; ?> { ... } overrides * { ... } and p { ... } styles. <i>Make p style !important and all paragraphs will become orange!</i></p>
    <p>
      <select class='approved' size=3>
        <option><strong>Select</strong> class "approved"</option>
        <option>But will green these <strong>options</strong> be?</option>
        <option>Inheritance fails :-(</option>
      </select>
    </p>
    <p id='<?php echo SID; ?>'>If HTML was a compiled language like Java, you would be seeing a compiler error right now as two elements should not have the same id. But because browsers will do their best to render your dodgy code, you will most likely see blue text here. Moral of the story: <span style='color: white; background-color:#c00;'>Just because the browser is doing what you want, doesn't mean you are doing it right!</span> Always debug your code as best you can, do not rely on the browser to guess what you want it to do, it will end in tears.</p>
    <p style='color:rgba(0,0,0,0); font-size:300%;'>This text is invisible because the inline style overrides most styles. Unfortunately, if you use a <strong>strong</strong> or <em>em</em> element, the inline size style is inherited, but the inline color style is not. To correct this, add another inline style <code>style="color:inherit;"</code>.</p>
  </body>
</html></textarea><br><input type='button' value='Try it'/>       
    </section>
  </section>
    
  <section>
    <header>Cascading Nature of CSS<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Inline, Embedded, External, Inherit and !important Styles</h1>
      <p>In the previous example, you can see an example of <strong>inline</strong> style taking precedence over <strong>id</strong> style. Inline style is very specific as it only applies to the element it is wrapped around. Ideally, you should avoid inline style as much as possible on production servers.</p>
      <p><code>&lt;p style='<span>...</span>'&gt;<span>Try not to use inline style ...</span>&lt;/p&gt;</code></p>
      <p>Likewise, whenever there is an identical declaration in an embedded stylesheet or an external stylesheet, the embedded stylesheet usually takes priority. This is another reason why you should avoid embedded stylesheets if developing a consistant look and feel across all website pages.</p>
      <p>As discussed in an earlier slide, most often simple styles are inherited, but not always. To make the <strong>&lt;options&gt;</strong> the same color as their parent (ie green <strong>&lt;select&gt;</strong>), use <code>inherit</code>:</p>
      <p><code>option { <span>color:inherit;</span> }</code></p>
      <p>Finally, in an absolute emergency (eg if it is 3.55am and you have a 4am deadline), you can use the <code>!important</code> declaration to give a style precedence over all others. The following declaration will make all text red in the previous tryit example:</p>
      <p><code>* { color: red <span>!important</span>; }</code></p>
    </section>
  </section>

  <section class='key' id='tp-csscolor'>
    <header>Color Models<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>How Humans See Color</h1>
      <div>
      <figure class='fl'>
        <a title="By Wing-Chi Poon [CC BY-SA 2.5 (http://creativecommons.org/licenses/by-sa/2.5)], via Wikimedia Commons" target="_blank" href="https://commons.wikimedia.org/wiki/File%3AWhereRainbowRises.jpg"><img width="256" alt="WhereRainbowRises" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/WhereRainbowRises.jpg/256px-WhereRainbowRises.jpg"/></a>
      </figure></div>
      <p><figure class='fr'><img src='app/img/color-octave.gif' alt='one octave on piano keyboard represented with keys colored from colors of the rainbow'/></figure>Humans are not very good at seeing colors. For those who understand music and frequency concepts, the rainbow in the photograph on the left represents just one octave of color on the elecro-magnetic spectrum, illustrated in the graphic on the right. Birds and insects see a wider and more colorful rainbow than humans.</p>
      <p class='cl'><figure class='fr'><img class='fr' src='app/img/color-octave-rgb.gif' alt='three piano keyboard keys representing human visible colors of the rainbow'/></figure>Not only that, even though we humans think that we can see all of these colors, we can only see three of these colors: <strong>red, green and dark blue (indigo)</strong>, ie the primary colors, illustrated in the image on the right (see image right), and less if we are colorblind.</p>
      <p>All other colors on the rainbow, ie <strong>orange, yellow, cyan and violet</strong>, can be created (or "spoofed") by a combination of these three primary colors.</p>
      <p>All other colors and shades are not on the rainbow, eg <strong>grey, white, pink, brown and purple</strong>. These are "made up" by our brains when we see real colors mixed together.</p>
      <div class='cb'></div>
    </section>
  </section>
  
  <section>
    <header>Color Models<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Fooling the Human Brain</h1>
      <div>
      <figure class='fl'>
        <img alt='iPhone 4s placed under a microscope' src='app/img/iPhoneMicroscope-R.jpg'/><br>
        <img alt='iPhone 4s viewed under a microscope' src='app/img/iPhoneMicroscopeView.jpg'/>
      </figure></div>
      <p>It is easy to fool ourselves into thinking we are seeing a full color spectrum. Stadium screens, televisions, computer monitors and projectors, touch pads and smart phone screens can be built by putting down a close-knit grid of three repeating red, green and blue lights.</p>
      <p>Each group of three lights, or <strong>channels</strong>, form a <strong>picture element</strong>, otherwise known as a <strong>pixel</strong>.</p>
      <p>In the images left, we are looking at a white iPhone 4S screen through a microscope and we can see that there are no "white" pixels, just thin red green and blue LEDs that form square shaped pixels.</p>
      <p>On the next slide we will look at a very simple <strong>Red Green Blue</strong> or <strong>RGB</strong> model that allows 16 different intensities in each channel, producing <strong>4,096</strong> different colors.</p>
      <p>Values are hexadecimal (ie 0, 1, 2, ... 9, A, B, C, D, E, F).</p>
      <div class='cb'></div>
    </section>
  </section>
  
  <section>
    <header>Color Models<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Pixel Color Simulator</h1>
      <p>The following simulator modifies the <code>background-color</code>of various div elements:</p>
      <div id='pixel-sim'>
        <div id='pixel-r'>#000</div>
        <div id='pixel-g'>#000</div>
        <div id='pixel-b'>#000</div>
        <p>
        <input id='slider-r' type='range' min=0 max=15 value=0 oninput="pixelSimulator()"><br>
        <input id='slider-g' type='range' min=0 max=15 value=0 oninput="pixelSimulator()"><br>
        <input id='slider-b' type='range' min=0 max=15 value=0 oninput="pixelSimulator()"></p>
        <div id='pixel-rgb'>#000</div>
      </div>
      <p><code>div { background-color: <span>#RGB</span>; }</code></p>
      <p>The 3 div / rectangles on the left represent the 3 channels that make up each pixel. The rounded div / rectangle on the bottom represents the unmagnified iPhone screen. Between the two are range inputs / sliders that increase the brightness of the LEDs using Javascript and jQuery.</p>
      <p>Try to match the following ridiculously named colors with the iPhone screen and note their hex value.</p>
      <p style='margin-bottom:2em;'><span style='background-color:aquamarine; padding:1em'>Aquamarine</span><span style='background-color:burlywood; padding:1em'>Burlywood</span><span style='background-color:darkgoldenrod; padding:1em'>Darkgoldenrod</span><span style='background-color:dimgray; padding:1em'>Dimgray</span><span style='background-color:hotpink; padding:1em'>Hotpink</span><span style='background-color:thistle; padding:1em'>Thistle</span></p>
      <p><i>If you have some form of color blindness, please work with some else and share your perception of color. All developers need to know what color combinations do not work for those with color blindess.</i></p>
      <div class='cb'></div>
    </section>
  </section>
  
  <section>
    <header>Color Models<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>21st Century Color</h1>
      <p><img class='fr' src='app/img/color-picker01.png' alt='color picker with eyedrop tool'/>In the inspector (image right), you can examine an element's color with great precision. Most browsers will also let you select colors on screen with an eyedropper tool. A color logo can be sampled to create a coherent color palette.</p>
      <p>Notice that the colors are represented in a more modern <code>#<span>RRGGBB</span></code> format. This model offers <strong>256</strong> intensity levels in each channel, producing over <strong>16.7 million</strong> colors and shades:</p>
      <ul class='mono'>
        <li><strong>7F</strong><sub>16</sub> = <strong>&nbsp;7</strong> x16 + <strong>16</strong> = 127<sub>10</sub> (half power)</li>
        <li><strong>FF</strong><sub>16</sub> = <strong>16</strong> x16 + <strong>16</strong> = 255<sub>10</sub> (full power)</li>
        <li><strong>D4</strong><sub>16</sub> = <strong>13</strong> x16 +  <strong>&nbsp;4</strong> = 212<sub>10</sub> (~83% power)</li>
      </ul>
      <p>Most humans cannot detect small incremental changes in each channel. <i>Can you see any color difference below?</i></p>
      <p style='margin-bottom:2em;'><span style='background-color:#7FFFD4; padding:1em'>#7FFFD4</span> <span style='background-color:#7EFED5; padding:1em'>#7<strong>E</strong>F<strong>E</strong>D<strong>5</strong></span></p>
      <p>In CSS3, a more user friendly color system was introduced to allow decimal or percentage numbers:</p>
      <p><code>color: <span>rgb(255,255,0)</span>;</code>creates yellow text.</p>
      <p><code>color: <span>rgb(100%,0%,100%)</span>;</code>creates purple text.</p>
      <p>The inspector lets you know how to generate a color in any of these models.</p>
      <div class='cb'></div>
    </section>
  </section>
  
  <section>
    <header>Color Models<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Hue Saturation Lightness Model</h1>
      <p>A more intuitive way of selecting colors is to first pick the color or <strong>hue</strong>. Hue is a number between <strong>0</strong> and <strong>360</strong> and represents the angle on the color wheel in degrees (below right). All of these colors are "on the rainbow", except for magenta (ie purple) and rose.</p>
      <div class='fr'>
        <a title="DanPMK at English Wikipedia [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ARBG_color_wheel.svg" target='_blank'><img width="400px" alt="RBG color wheel" src="https://upload.wikimedia.org/wikipedia/commons/a/ab/RBG_color_wheel.svg"/></a>
      </div>
      <div class='iqb'>What hue value is red? [ <span class='r' id='huequiz1'></span> ]<br>
        <select oninput='$("#huequiz1").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Correct!'>0</option>
          <option value='Incorrect. 210 is Azure.'>210</option>
          <option value='Correct again!'>360</option>
        </select>
      </div>
      <div class='iqb'>What hue value is yellow? [ <span class='r' id='huequiz2'></span> ]<br>
        <select oninput='$("#huequiz2").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect. 0 is red.'>0</option>
          <option value='Incorrect. This is not a clock!'>2 o'clock</option>
          <option value='Correct!'>60</option>
        </select>
      </div>
      <div class='iqb'>What hue value is cyan? [ <span class='r' id='huequiz3'></span> ]<br>
        <select oninput='$("#huequiz3").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect. 120 is green.'>120</option>
          <option value='Correct!'>180</option>
          <option value='Incorrect, 270 is Violet.'>270</option>
        </select>
      </div>
      <p>Once the <strong>hue</strong> is selected, we then select a percentage of how <strong>saturated</strong> it is (0% = grey, washed out; 100% = vibrant, strong color) and how <strong>light</strong> it is (0% = black, 50% = pure, 100% = white).</p>
      <pre><code>selector { color: <span>hsl(270, 100%, 50%);</span> )</code></pre>
      <p>The above produces a strong vibrant violet text color.</p>
      <p>The following simulator modifies the <code>background-color</code>of various div elements using the HSL color model:</p>
      <div class='cb'></div>          
    </section>
  </section>    

  <section>
    <header>Color Models<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Hue Saturation Lightness Model</h1>
      <p>This time the range inputs control <strong>hue</strong>, <strong>saturation</strong> and <strong>lightness</strong>.</p>
      <div id='pixel-sim2'>
        <div id='pixel-r2'>rgb(0,0,0)</div>
        <div id='pixel-g2'>rgb(0,0,0)</div>
        <div id='pixel-b2'>rgb(0,0,0)</div>
        <p>
        <input id='slider-h' type='range' min=0 max=360 value=0 oninput="hsl2rgb()"><br>
        <input id='slider-s' type='range' min=0 max=100 value=0 oninput="hsl2rgb()"><br>
        <input id='slider-l' type='range' min=0 max=100 value=0 oninput="hsl2rgb()"></p>
        <div id='pixel-hsl'>hsl(0,0%,0%)</div>
      </div>
      <p><code>div { background-color: <span>hsl(hue, sat, light)</span>; }</code></p>
      <details class='qna'><summary><strong>Activity 1:</strong> Adjust just the top two sliders. What colors can you create?</summary>Just Black. Without light, there is no color.</details>
      <details class='qna'><summary><strong>Activity 2:</strong> Set the bottom slider to full and adjust just the top two sliders. What colors can you create? </summary>Just White. Too much light washes out all color.</details>
      <details class='qna'><summary><strong>Activity 3:</strong> Set the middle slider to 100%, the bottom slider to 50% and adjust just the top slider. What colors can you create? </summary>These colors should be "rainbow" colors (plus magenta and rose). You will notice that the LEDs levels fluctuate between 0 and 255.</details>
      <details class='qna'><summary><strong>Activity 4:</strong> Set both the middle and bottom sliders to 50% and adjust just the top slider. What colors can you create? </summary>These colors are similar to Activity 3, except that the colors are not as intense or <strong>saturated</strong>. They are <em>shades</em> of the saturated colors we saw before and you will notice that the LEDs levels only fluctuate between 64 and 191.</details>
      <details class='qna'><summary><strong>Activity 5:</strong> Set the middle slider to 50% and the bottom slider to 90% and adjust just the top slider. What colors can you create? </summary>These colors are similar to Activity 3, except that they are a very light <strong>pastel</strong> shade. You will notice that the LEDs are almost on full power, fluctuating between 217 and 242.</details>
      <p><i>Again, if you have some form of color blindness, please work with some else and share your perception of color.</i></p>
      <div class='cb'></div>          
    </section>
  </section>    

  <section>
    <header>Color Models<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Transparency, Opacity and Alpha</h1>
      <p><strong>Transparency</strong> is a computer intensive operation and was introduced in CSS3. The browser must keep a record of what objects are laid out below a transparent element with transparency (eg the <strong>body</strong> and <strong>html</strong> elements) and perform a color calculation for each pixel.</p>
      <p>The term <strong>alpha</strong> and <strong>opacity</strong> mean the same thing but have the opposite meaning of transparency. Alpha can be declared as part of the <code>rgb<strong>a</strong>()</code> and <code>hsl<strong>a</strong>()</code> color models.</p>
      <p>For example, these slides have a slight transparency, allowing the background to show through.</p>
      <pre><code>section > header { background-color: hsl<span>a</span>(0, 85%, 90%, <span>0.85</span>); }</code></pre>
      <p><i>Light pink with 15% transparency</i>.</p>
      <pre><code>section > section { background-color: rgb<span>a</span>(255, 255, 255, <span>0.9</span>); }</code></pre>
      <p><i>White with 10% transparency, ie less transparent than the slide header</i>.</p>
      <p>Opacity can be declared independently of color and can be applied to other elements such as the <strong>&lt;img&gt;</strong> element.</p>
      <pre><code>img.watermark { <span>opacity: 0.05;</span> }</code></pre>
      <p><i>Will make all images of class 'watermark' 95% transparent.</i></p>
      <div class='cb'></div>          
    </section>
  </section>    
    
</article>