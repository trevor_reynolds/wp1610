<?php
  $counter=1;
  $slideText='Lect 4, P.';
?>
<article id='slides-lecture-4'>

  <section class='title'>
    <header>Welcome!<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Web Programming
        <br>COSC2413/2426/2453</p>
      <p>Lecture 4</p>
    </section>
  </section>

  <section class='title'>
    <header>Lecture 4.1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 1: Styling Text &amp; Borders, Understanding the Box Model</p>
    </section>
  </section>

  <section class='key'>
    <header>Cascading Style Sheets<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-csstext'>CSS Text Styling</a></li>
        <li><a href='#tp-csstxalgn'>CSS Text Alignment</a></li>
        <li><a href='#tp-cssboxmdl'>CSS Box Model</a></li>
        <li><a href='#tp-csscompac'>CSS Compact Declaration</a></li>
      </ol>
    </section>
  </section>

  <section class='key' id='tp-csstext'>
    <header>CSS Text Styling<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Font Family</h1>
      <p>To render text with a particular font, we use the <code>font-family</code> declaration. Multiple fonts can be declared, separated with commas. Quotation marks are optional, but are required for fonts with spaces in the name.</p>
      <pre><code>font-family: <span>firstchoice</span>, "<span>second choice</span>", <span>serif</span>;</code></pre>
      <p>A list of fonts is supplied so that if the client does not have the particular font we want pre-installed, text can be rendered in alternative more general fonts. The last font in a list should be the most general and be one of the following:</p>
      <ul>
        <li><strong>serif</strong>: <span style='font-family: serif'>A font with curly ends like this one.</span></li>
        <li><strong>sans-serif</strong>: <span style='font-family: sans-serif'>A font without curly ends like this one.</span></li>
        <li><strong>monospace</strong>: <span style='font-family: monospace'>A font with equal width characters like this one.</span></li>
      </ul>
      <p>Font sizing is generally based on the height of a font. As you can see, different fonts have different letter widths and spacing, so alternative fonts can affect layout.</p>
    </section>
  </section>

  <section>
    <header>CSS Text Styling<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Supplying Fonts</h1>
      <p>In CSS3, we can provide the client with the actual font to avoid subtle layout changes with a <code>@font-face</code> declaration. We provide a name for the font we will be using in stylesheets and the location of the font.</p>
      <pre><code>@font-face {
  font-family: <span>startrek</span>;
  src: url("<span>fonts/Star Trek_future.ttf</span>");
}
...
&lt;p&gt; style='<span>font-family: startrek</span>;'&gt;<span>Now I can use the Star Trek font.</span>&lt;/p&gt;</code></pre>
      <p style='font-family: startrek; font-size:200%; margin:0.5em'>Now I can use the Star Trek font.</p>
    <p>This can cause copyright issues, so make sure that you have sufficient rights. There are many free fonts available for commerical and non-commercial use. For example, in these slides I have used <?php a('http://dafont.com','dafont.com'); ?> to source the Star Trek and Museo fonts used in headings.</p>
    </section>
  </section>

  <section>
    <header>CSS Text Styling<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Web Fonts</h1>
      <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
      <p>Google has created a resource of hosted free web fonts at <?php a('//fonts.google.com','fonts.google.com'); ?>. Whenever you use these fonts, the client will request the font directly from google, ie you do not have to host these font files directly.</p>
      <p>You can include the font using a <code>&lt;link&gt;</code> element in the <code>&lt;head&gt;</code> element</p>
      <pre><code>&lt;link href='<span>//fonts.googleapis.com/css?<span>family=Indie+Flower</span></span>' rel='stylesheet' type='text/css'&gt;</code></pre>
<pre><code> 
&lt;span style='<span>font-family: "Indie Flower"</span>;'&gt;<span>Now I can use this linked indie flower font.</span>&lt;/span&gt;</code></pre>
      <p style='font-size:150%;'><span style='font-family: "Indie Flower";'>Now I can use the imported indie flower font</span>.</p>
      <blockquote><p>NB: When you go to Google's <strong>quick use</strong> instructions, you will see '<strong>https://</strong>' at the start of their links.</p><p>Servers and browsers often have security concerns when mixing '<strong>http://</strong>' and '<strong>https://</strong>' resources in one page, resulting in blocked content and no fonts. To solve this problem, use '<strong>//</strong>' as shown above which allow the server and browser to select the best transmission protocol.</p></blockquote>
    </section>
  </section>

  <section>
    <header>CSS Text Styling<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Font Size</h1>
      <p>The size of a font can be set using the <code>font-size</code> property. Some units (<code>px, cm, mm</code>) are <strong>absolute</strong> and do not change, others are <strong>relative</strong> and their final size is determined by the size of their ancestor elements (<code>em, %, rem</code>) or the size of the viewport / screen (<code>vw, vh, vmin, vmax</code>).</p>
      <pre><code>font-size: <span>20px</span>;</code></pre>
      <p>There are also absolute and relative size words: <code>small, medium, large, smaller, larger</code>. If you love lazy clothing size labels, these are for you! <span class='g i'>&larr; NB: sarcasm</span></p>
      <p>Below is a series of nested divs with fonts of equal unit size. To change the font-sizes, make a selection from this select box:
      <select oninput="$('.fontsizedemo').css('font-size',this.value)">
        <option value=''>Please Select</option>
        <optgroup label='Absolute sizes'>
          <option value='16px'>16px (pixels)</option>
          <option value='20px'>20px</option>
          <option value='16pt'>16pt (points)</option>
          <option value='20pt'>20pt</option>
          <option value='10mm'>10mm (printer accurate, not screen)</option>
          <option value='2cm'>2cm (printer accurate, not screen)</option>
        </optgroup>
        <optgroup label='Relative to parent'>
          <option value='1em'>1em (initially 16pt)</option>
          <option value='0.8em'>0.8em</option>
          <option value='1.5em'>1.5em</option>
          <option value='100%'>100% (initially 16pt)</option>
          <option value='80%'>80%</option>
          <option value='150%'>150%</option>
        </optgroup>
        <optgroup label='Relative to html element'>
          <option value='1rem'>1rem (initially 16pt)</option>
          <option value='0.8rem'>0.8rem</option>
          <option value='1.5rem'>1.5rem</option>
        </optgroup>
        <optgroup label='Relative to viewport / screen'>
          <option value='5vw'>5vw (5% viewport width)</option>
          <option value='10vw'>10vw (10% viewport width)</option>
          <option value='5vh'>5vh (5% viewport height)</option>
          <option value='10vh'>10vh (10% viewport height)</option>
          <option value='5vmin'>5vmin (5% viewport short side)</option>
          <option value='5vmax'>5vmax (5% viewport large side)</option>
        </optgroup>
        <optgroup label='Absolute words'>
          <option value='xx-small'>XX-Small</option>
          <option value='x-small'>X-Small</option>
          <option value='small'>Small</option>
          <option value='medium'>Medium (normal size)</option>
          <option value='large'>Large</option>
          <option value='x-large'>X-Large</option>
          <option value='xx-large'>XX-Large</option>
        </optgroup>
        <optgroup label='Relative words'>
          <option value='smaller'>Smaller</option>
          <option value='larger'>Larger</option>
        </optgroup>
      </select>
      <div class='demobox fontsizedemo'>Outer Div.<div class='fontsizedemo'>Middle Div<div class='fontsizedemo'>Inner Div</div></div></div>
      <p>These slides use <code>relative</code> font-size units as much as possible so that text can be resized in the navigation menu.</p>
      <p><img src='app/img/navmagnifier.png' alt='screenshot of navigation magnifying slider'/></p>      
    </section>
  </section>

  <section>
    <header>CSS Text Styling<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Font Style and Weight</h1>
      <p>Fonts can be given <code>normal, italic, oblique</code> styles using the <code>font-style</code> property.</p>
      <pre><code>p { font-style: <span>italic;</span> }</code></pre>
      <p><span id='fontstyledemo'>To change the font-style of just this span</span>, make a selection from this select box:
      <select oninput="$('#fontstyledemo').css('font-style',this.value)">
        <option value='normal'>Normal</option>
        <option value='italic'>Italic</option>
        <option value='oblique'>Oblique</option>
      </select></p>
      <p>Font weight can be "fine-tuned" using the <code>font-weight</code> property by providing words such as <code>normal, bold, lighter, bolder</code> or by providing a number between <code>100</code> and <code>900</code>, but ending in '00' (ie it's just nine numbers and not all fonts will have 9 distinct weights).</p>
      <pre><code>p { font-weight: <span>bold;</span> }</code></pre>
      <p><span id='fontweightdemo'>To change the font-weight of just this span</span>, make a selection from this select box:
      <select oninput="$('#fontweightdemo').css('font-weight',this.value)">
        <option value='normal'>Normal</option>
        <optgroup label='words'>
          <option value='lighter'>Lighter</option>
          <option value='bold'>Bold</option>
          <option value='bolder'>Bolder</option>
        </optgroup>
        <optgroup label='numbers'>
          <option value='100'>100</option>
          <option value='200'>200</option>
          <option value='300'>300</option>
          <option value='400'>400</option>
          <option value='500'>500</option>
          <option value='600'>600</option>
          <option value='700'>700</option>
          <option value='800'>800</option>
          <option value='900'>900</option>
        </optgroup>
        </select></p>
    </section>
  </section>

  <section>
    <header>CSS Text Styling<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Line Height and Letter Spacing</h1>
      <div id='demoletterlinespacing'>
        <p>The height of a line can be adjusted using <code>line-height</code> property. Default line-height varies from font to font, but most fonts have a default height around 1.2 or 120%, leaving a 20% gap between lines.</p>
        <pre><code>p { line-height: <span>normal;</span> }</code></pre>
        <p><input type='range' min=-1 max=5 step=0.2 value=1.2 oninput="$('#demoletterlinespacing').css('line-height',this.value)"> This slider will set the line spacing on this slide between 0 and 5.</p>
        <p>Distance between letters can be adjusted using <code>letter-spacing</code> property. Although letter spacing varies from font to font, the browser recognises 0 as the default letter spacing. Positive values increase space, negative values decrease space.</p>
        <pre><code>p { letter-spacing: normal; }</code></pre>
        <p><input type='range' min=-10 max=10 value=0 step=1 oninput="$('#demoletterlinespacing').css('letter-spacing',this.value+'pt')"> This slider will set the letter spacing on this slide between -3em and 3em.</p>
      </div>
    </section>
  </section>

  <section>
    <header>CSS Text Styling<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Text Decoration</h1>
      <p>Text can be given <code>none, underline, overline, line-through</code> styles using the <code>text-decoration</code> property.</p>
      <pre><code>p { text-decoration: <span>underline;</span> }</code></pre>
      <p>Why do we call it text-decoration and not font-decoration? <i>That's a really good question ...</i></p>
      <p><span id='textdecorationdemo'>To change the text-decoration of just this span</span>, make a selection from this select box:
      <select oninput="$('#textdecorationdemo').css('text-decoration',this.value)">
        <option value='none'>None</option>
        <option value='overline'>Overline</option>
        <option value='line-through'>Line-Through</option>
        <option value='underline'>Underline</option>
        </select>
      </p>
    </section>
  </section>

  <section class='key' id='tp-csstxalgn'>
    <header>CSS Text Alignment<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Horizontal Alignment</h1>
      <p>Text can be horizontally aligned <code>initial, left, right, center, justify</code> using the <code>text-align</code> property.</p>
      <pre><code>div { text-align: <span>initial;</span> }</code></pre>
      <div id='textaligndemo' class='demobox'>
        <p>To change the alignment of the text in this resizable div, make a selection from this select box: <select oninput="$('#textaligndemo').css('text-align',this.value)">
          <option value='initial'>Initial</option>
          <option value='left'>Left</option>
          <option value='right'>Right</option>
          <option value='center'>Center</option>
          <option value='justify'>Justify</option>
        </select></p>
        <p>Most of you should be familiar with <strong>left</strong> and <strong>right</strong> alignment, but are you just as familiar with the <strong>inital</strong> and <strong>justify</strong> options? What do they do? Why are they there? I'm sure that some of you know the answers already and can see that this is just more paragraph padding text, but let me describe those two properties in more detail and in an unordered list so you can see what happens to bullet points:</p>
        <ul>
          <li><strong>Initial</strong>: Text will be left aligned for languages that are written left to right (ltr) and right aligned for languages that are written right to left (rtl).</li>
          <li><strong>Justify</strong>: Text will be lined up neatly left and right with stretched spaces between words. The last line has 'initial' alignment and spaces will not be stretched.</li>
        </ul>
      </div>
    </section>
  </section>
  
  <section>
    <header>CSS Text Alignment<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Vertical Alignment</h1>
      <p>Inline elements within a block (eg spans, small images) can be vertically aligned using the <code>vertical-align</code> property. It has many "fine-tuning" options to remove small spaces, but it is not as powerful as the block-level text-align property above. One practical application is to align images with inline text, eg logos.</p>
      <pre><code>img { vertical-align: <span>middle;</span> }</code></pre>
      <p>To align the image with the text below, make a selection from this select box: <select oninput="$('#verticalaligndemo').css('vertical-align',this.value)">
          <option value='initial'>Initial</option>
          <option value='baseline'>Baseline</option>
        <optgroup label='Simple'>
          <option value='top'>Top</option>
          <option value='middle'>Middle</option>
          <option value='bottom'>Bottom</option>
        </optgroup>
        <optgroup label='Small Gaps'>
          <option value='text-top'>Text-top</option>
          <option value='text-bottom'>Text-bottom</option>
        </optgroup>
        <optgroup label='Science / Maths'>
          <option value='super'>Super (superscript)</option>
          <option value='sub'>Sub (subscript)</option>
        </optgroup>
        <optgroup label='Percentage of Line Height'>
          <option value='100%'>100% (one line up)</option>
          <option value='-50%'>-50% (half a line down)</option>
        </optgroup>
        <optgroup label='Distance (px, em, cm etc)'>
          <option value='55px'>55px</option>
          <option value='-5px'>-5px</option>
        </optgroup>
      </select></p>
      <p class='demobox'>School of Science at <img id='verticalaligndemo' src='app/img/rmit-orb-small.png' alt='rmit orb' /> RMIT University</p>  
    </section>
  </section>
  
  <section>
    <header>CSS Text Alignment<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Text Indentation</h1>
      <p>The first line of a text-block (eg a paragraph) can be indented using the <code>text-indent</code> property.</p>
      <pre><code>p { text-indent: <span>1em;</span> }</code></pre>
      <!--<p><i>This can also be achieved using the <strong>::first-line</strong> pseudo selector</i></p>-->
      <p id='textindentdemo'>To indent or outdent the first line of text in this paragraph, make a selection from the select box that is located at the end of this paragraph, ie after all of these words which are only here to pad out this paragraph and make sure it flows onto two or more lines:
      <select oninput="$('#textindentdemo').css('text-indent',this.value)">
        <option value='0'>0 (no indentation)</option>
        <option value='2em'>2em (indent)</option>
        <option value='-10px'>-10px (outdent)</option>
      </select>
      </p>
    </section>
  </section>

  <section>
    <header>CSS Text Alignment<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Floating Content</h1>
      <p class='mug'><img class='fl mug' src='app/img/css-is-awesome.png' alt='css is awesome mug, with text escaping assigned boundaries' />Within a block element, such as a paragraph or div, we can <code>float</code> elements either <code>left, right</code> but not <strong>center</strong>. <i>For example, the text in this paragraph and the next two follow a <strong>floating</strong> mug image.</i></p>
      <pre><code>img.mug { float: <span>left</span>; }</code></pre> 
      <p class='mug'><img class='fl mug' src='app/img/css-is-awesome.png' alt='css is awesome mug, with text escaping assigned boundaries' />All content that follows is  <strong>floated around</strong> the floating content. This starts off as a nice effect, but can quickly go wrong when more floating elements are introduced, as this slide demonstrates!</p>
      <p class='mug'><img class='fl mug' src='app/img/css-is-awesome.png' alt='css is awesome mug, with text escaping assigned boundaries' />In order to "stop the floats", an element after the floating element must have a <code>clear</code> property which can have values <code>left, right, both</code>, <strong>both</strong> clearing left and right sides together.</p>
      <pre><code>p, blockquote { clear: <span>both</span> };</code></pre>
      <blockquote class='mug'>Many page layouts are crippled by uncaught floating elements such as logos, navigation links etc. Resize this window, see how float works, how it can upset the layout, then play with the slide options below to fix the floating layout.</blockquote>
      <fieldset><legend>Slide Options</legend><input type=checkbox onchange='if (this.checked) $("p.mug, blockquote.mug ").css("clear","both"); else $("p.mug, blockquote.mug").css("clear","");' /><i> Attach <strong>clear:both</strong> styles to paragraph and blockquote elements.</i><br>
      <input type=checkbox onchange='if (this.checked) $("img.mug").hide(1000); else $("img.mug").show(1000);' /><i> Remove mug images.</i></fieldset>
    </section>
  </section>
     
  <section class='key' id='tp-cssboxmdl'>
    <header>CSS Box Model<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Box Dimensions</h1>
      <p>Every page element is considered to be a rectangular "box" and is rendered in an appropriate place on the screen. this applies to both <strong>block</strong> and <strong>inline</strong> elements.</p>
      <p>Each box has internal <strong>content</strong> area, surrounded by <strong>padding</strong> to keep it away from the <strong>border</strong>.  Finally, there is some <strong>margin</strong> to keep it away from the parent element's sides.</p>
      <pre><code>#demoboxmodel1 {
  <span>width:</span> 200px;
  <span>height:</span> 100px;
  <span>padding</span>: 20px;
  <span>border</span>: 10px #B63 ridge;
  <span>margin</span>: 1em;
}
...
&lt;div id='<span>demoboxmodel1</span>'&gt;Text inside demoboxmodel1&lt;/div&gt;</code></pre>
      <div id=demoboxmodel1>Text inside demoboxmodel1</div>
    </section>
  </section>
     
  <section>
    <header>CSS Box Model<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Box Dimensions Example</h1>
      <p>Below is a screenshot of RMIT's balloon and the inspector's graphical rendition of its box model dimensions. It has been placed inside a larger box with geen dotted outline so you can see the margin clearly.</p>
      <div class='demobox'><img class="plain" style='padding: 10px; border: 5px #c00 solid; margin:15px;' src='app/img/BoxModel.png' alt='RMIT Balloon picture and inspector' /></div>
      <p>Right-click on the image above and use the inspector to discover how wide or thick the following properties are. NB: width means actual content width:</p>
      <div class='iqb'>width: [ <span class='r' id='boxmodelquiz1'></span> ]<br>
        <select oninput='$("#boxmodelquiz1").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect'>10px</option>
          <option value='Incorrect'>120px</option>
          <option value='Correct!'>700px</option>
        </select>
      </div>
      <div class='iqb'>padding: [ <span class='r' id='boxmodelquiz2'></span> ]<br>
          <select oninput='$("#boxmodelquiz2").html(this.value)'>
            <option value=''>Please select</option>
            <option value='Incorrect'>5px</option>
            <option value='Correct!'>10px</option>
            <option value='Incorrect'>15px</option>
          </select></div>
      <div class='iqb'>border: [ <span class='r' id='boxmodelquiz3'></span> ]<br>
          <select oninput='$("#boxmodelquiz3").html(this.value)'>
            <option value=''>Please select</option>
            <option value='Correct!'>5px</option>
            <option value='Incorrect'>10px</option>
            <option value='Incorrect'>15px</option>
          </select></div>
      <div class='iqb'>margin: [ <span class='r' id='boxmodelquiz4'></span> ]<br>
          <select oninput='$("#boxmodelquiz4").html(this.value)'>
            <option value=''>Please select</option>
            <option value='Incorrect'>5px</option>
            <option value='Incorrect'>10px</option>
            <option value='Correct!'>15px</option>
          </select></div>
        <div class='iqb'>Red Box: [ <span class='r' id='boxmodelquiz5'></span> ]<br>
          <select oninput='$("#boxmodelquiz5").html(this.value)'>
            <option value=''>Please select</option>
            <option value='Incorrect'>120px</option>
            <option value='Correct!'>730px</option>
            <option value='Incorrect'>740px</option>
            <option value='Incorrect'>750px</option>
            <option value='Incorrect'>760px</option>
          </select>
      </div>
    </section>
  </section>
     
  <section>
    <header>CSS Box Model<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Absolute v/s Relative Sizes</h1>
      <p>How <strong>absolute</strong> sizes (px, cm, etc) are applied to the box properties is clear, but how <strong>relative</strong> sizes (%, em etc) are applied is less clear and varies. The style properties of the paragraphs below can be adjusted:</p>
      <div class='iqb'>Set width:<br>
        <select onchange="$('#absvsrel p').css('width',this.value)">
          <option value=''>Remove Style</option>
          <option value='50%'>50%</option>
          <option value='100%'>100%</option>
        </select>
      </div>
      <div class='iqb'>Set height:<br>
        <select onchange="$('#absvsrel p').css('height',this.value)">
          <option value=''>Remove Style</option>
          <option value='50%'>50%</option>
          <option value='100%'>100%</option>
        </select>
      </div>
      <div class='iqb'>Set padding:<br>
        <select onchange="$('#absvsrel p').css('padding',this.value)">
          <option value=''>Remove Style</option>
          <option value='2%'>2%</option>
          <option value='5%'>5%</option>
        </select>
      </div>
      <div class='iqb'>Set border:<br>
        <select onchange="$('#absvsrel p').css('border-width',this.value)">
          <option value=''>Remove Style</option>
          <option value='0.5em'>0.5em</option>
          <option value='1em'>1em</option>
        </select>
      </div>
      <div class='iqb'>Set margin:<br>
        <select onchange="$('#absvsrel p').css('margin',this.value)">
          <option value=''>Remove Style</option>
          <option value='2%'>2%</option>
          <option value='5%'>5%</option>
        </select>
      </div>      
      <div id='absvsrel'>
        <p><strong>width</strong>: Relative to parent element's width.</p>
        <p><strong>height</strong>: Relative to the parent element's height, but <em>only</em> if the immediate parent element has been explicitly sized.</p>
        <p><strong>padding</strong>: Relative to the parent element's width. All 4 sides will have the same thickness.</p>
        <p><strong>border</strong>: Has no inherited or percentage options. All 4 sides will have the same thickness if using em or rem units.</p>
        <p><strong>margin</strong>: Relative to the parent element's width with one difference: <strong>left</strong> and <strong>right</strong> margins of sibling elements will <strong>add</strong>, but <strong>top</strong> and <strong>bottom</strong> margins will <strong>collapse</strong>. This means horizontal spacing will be double that of vertical spacing. <i>You can see this with the red boxes above. Just resize the window so that they flow onto two lines.</i></p>
      </div>
    </section>
  </section>
     
  <section>
    <header>CSS Box Model<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Box Sizing Model</h1>
      <p>The <strong>box sizing</strong> model sets the content width to the <code>width</code> property. As <strong>padding</strong> and <strong>border</strong> are added, the box gets wider.</p>
      <p>When using relative width, eg percentage sizes, the content is resized to be the same size as the parent's content. This makes layout design difficult when using relative sizes. For example:</p>
      <pre><code>#demoboxmodel2 {
  width: <span>100%</span>;
  padding: <span>20px</span>;
  border-width: <span>10px</span>;
}</code></pre>
      <p id='demoboxmodel2'>These properties will create a box <strong>60px</strong> wider than the parent. This means <strong>60px</strong> of the content is outside the parent's content area, more if <strong>margin</strong> is added.</p>
      <p>In CSS3 a new property <code>box-sizing</code> was introduced which has <code>content-box, border-box</code> values. The <strong>content-box</strong> setting is still the default, but <strong>border-box</strong> sets the outside (ie border) to be the width and shrinks the content to fit.</p> 
      <pre><code>div {
  ... same as above
  <span>box-sizing: border-box;</span>
}</code></pre>
      <div id='demoboxmodel3'>The <strong>border-box</strong> setting reduces the width so that it fits neatly inside the parent element, <i>as long as the child element has no margin!</i></div> 
    </section>
  </section>
     
  <section>
    <header>CSS Box Model<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Calc function</h1>
      <p>In CSS3, the need for developers to  <strong>calculate</strong> relative widths was recognised. <i>Please note: Spaces are required inside the <code>calc()</code> function.</i></p>
      <p>In the next example, we will add a 30px margin to the child element. We will then need to reduce the child width by 2 x 30px = 60px.</p>
      <pre><code>#demoboxmodel4 {
  <span>margin: <strong>30px</strong>;
  width: calc(100% - 2 * <strong>30px</strong>)</span>;
  padding: 20px;
  border-width: 10px;
  box-sizing: border-box;
}</code></pre>
      <p id='demoboxmodel4'>The <strong>calc</strong> function has reduced the box width to compensate for its margin. It is centered neatly inside the parent element.</p>      
    </section>
  </section>
     
  <section>
    <header>CSS Box Model<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Min and Max Suffixes, auto margin</h1>
      <p>Many dimension styles also have a <code>min-</code> and a <code>max-</code> option that can be used in conjunction with proportionate dimensions, for example percentages.</p>
      <pre><code>#demoboxmodel5 {
  width: <span>50%</span>;
  <span>min-</span>width: <span>300px</span>;
  <span>max-</span>width: <span>600px</span>;
}</code></pre>
      <div id='demoboxmodel5'>This demo box is will resize to be 50% of it's parent's width (ie the slide), but will never be wider than 600px or narrower than than 300px.</div>  
      <p>Finally, a box can be centered if left and right margin dimensions are set to <code>auto</code>.</p>
      <pre><code>.boxcenter {
  margin-left: <span>auto</span>;
  margin-right: <span>auto</span>;
}</code></pre>
      <p><label><input type='checkbox' onclick="toggleCenter(this.checked)">Click to apply auto margin to the demobox above</label>.</p>
    </section>
  </section>
     
  <section class='key' id='tp-csscompac'>
    <header>CSS Compact Declarations<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Setting Multiple Dimensions (1)</h1>
      <p>Most dimension styles can be set using individual <strong>top, right, bottom, left</strong>  settings, or by setting <strong>many</strong> at once. For example:
      <pre><code>element {
  padding-<span>top</span>: <span>10px</span>;
  padding-<span>right</span>: <span>10px</span>;
  padding-<span>bottom</span>: <span>10px</span>;
  padding-<span>left</span>: <span>10px</span>;
}</code></pre>      
      <p>Can be set with one declaration:</p>
      <pre><code>element {
  padding: <span>10px</span>;
}</code></pre> 
      <p><i>The same rule applies to the element's <strong>margin</strong> and <strong>border-width</strong>. Replace the word <strong>padding</strong> with the word <strong>margin</strong> or <strong>border-width</strong>.</i></p>
    </section>
  </section>
     
  <section>
    <header>CSS Compact Declarations<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Setting Multiple Dimensions (2)</h1>
      <p>Sometimes we want our <strong>vertical</strong> margin to be different to our <strong>horizontal</strong> margin. For example:
      <pre><code>element {
  margin-<span>top</span>: <span>10px</span>;
  margin-<span><span>right</span></span>: <span><span>20px</span></span>;
  margin-<span>bottom</span>: <span>10px</span>;
  margin-<span><span>left</span></span>: <span><span>20px</span></span>;
}</code></pre>      
      <p>Two values will set the <strong>vertical</strong> sides, then the <strong>horizontal</strong> sides:</p>
      <pre><code>element {
  padding: <span>10px <span>20px</span></span>;
}</code></pre>   
      <p><i>The same rule applies to the element's <strong>padding</strong> and <strong>border-width</strong>. Replace the word <strong>margin</strong> with the word <strong>padding</strong> or <strong>border-width</strong>.</i></p>
    </section>
  </section>
     
  <section>
    <header>CSS Compact Declarations<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Setting Multiple Dimensions (3 &amp; 4)</h1>
      <p>Three values set the <strong>top</strong>, then the <strong>sides</strong>, then the <strong>bottom</strong></p>
      <pre><code>element {
  border-<span>top</span>-width: <span>10px</span>;
  border-<span><span>right</span></span>-width: <span><span>20px</span></span>;
  border-<span><span><span>bottom</span></span></span>-width: <span><span><span>30px</span></span></span>;
  border-<span><span>left</span></span>-width: <span><span>20px</span></span>;
}</code></pre>      
      <p>Becomes:</p>
      <pre><code>element { 
  border-width: <span>10px <span>20px <span>30px</span></span></span>;
}</code></pre>      
      <p>Four values set the dimensions in clockwise order: <strong>top, right, bottom, left</strong>:</p>
      <pre><code>element {
  border-width: <span>12px <span>3px</span></span> <span>6px <span>9px</span></span>;
}</code></pre>      
    </section>
  </section>    

  <section>
    <header>CSS Compact Declarations<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Setting Border Color, Style and Width</h1>
      <p>Individual dimension rules apply to the <code>border-color</code>, and the <code>border-style</code> properties. <strong>Border-style</strong> has <code>solid, dotted, dashed; groove, ridge; inset, outset; none, hidden</code> values. For example:</p>
      <pre><code>element {
  border-<span>top</span>-color: <span>orange</span>;
  border-<span>right</span>-style: <span>dotted</span>;
}</code></pre>  
      <p>But it is also possible to combine <strong>color, border &amp; style</strong> if combining a single style of each. For example:</p> 
      <pre><code>element {
  border: <span>dotted orange 1px</span>;
}</code></pre>  
      <p>The above declaration will style all 4 sides with the same <strong>style, color and thickness</strong>, instead of making <strong>12</strong> individual declarations.</p>
    </section>
  </section>

  <section class='title'>
    <header>Lecture 4.2<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 2: Layout and Advanced Style</p>
    </section>
  </section>
  
  <section class='key'>
    <header>Layout and Advanced Style<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-csscorner'>CSS Corners</a></li>
        <li><a href='#tp-cssbckgnd'>CSS Background</a></li>
        <li><a href='#tp-cssshadow'>CSS Shadows</a></li>
        <li><a href='#tp-csslayout'>CSS Layout</a></li>
        <li><a href='#tp-cssposition'>CSS Position</a></li>
      </ol>
    </section>
  </section>

  <section class='key' id='tp-csscorner'>
    <header>CSS Corners<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Border Radius</h1>
      <p>Each corner can be curved with individual <code>border-radius</code> declarations or using a compact declaration as per the <strong>dimension</strong> rules we have just seen.</p>
      <pre><code>div { 
  border-<span>top-left</span>-radius: 10px;
  border-<span>top-right</span>-radius: 10px;
  border-<span>bottom-right</span>-radius: 10px;
  border-<span>bottom-left</span>-radius: 10px;
}</code></pre>
      <pre><code>div { <span>border-radius</span>: 10px; }</code></pre>
      <div id='democorners'>Make sure you put in extra padding!</div>
      <p>When supplying multiple values in a compact declaration, the <strong>top-left</strong> corner is always first, but remaining order differs.</p>
      <div class='iqb'>Adjust corners on box above:<br>
        <select onchange="$('#democorners').css('border-radius',this.value).html('border-radius: '+this.value+' ;')">
          <option value='20px'>1 Value: All sides (no change)</option>
          <option value='20px 80px'>2 Values: Opposite Corners</option>
          <option value='20px 50px 80px'>3 Values: Z-like order</option>
          <option value='20px 40px 60px 80px'>4 Values: Back to clock order</option>
        </select>
      </div> 
    </section>
  </section>

  <section>
    <header>CSS Corners<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Round and Oval Elements</h1>
      <p>Values can be <strong>static</strong> (eg pixels) or <strong>relative</strong> (eg percentages).</p>
      <pre><code>div { <span>border-radius</span>: 50% }</code></pre>
      <div id='democorners2'>RESIZABLE<br>50% = Circle or Oval</div>
    </section>
  </section>

  <section class='key' id='tp-cssbckgnd'>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Background Images</h1>
      <p>Background images can be included using the <code>background-image</code> style which takes a <code>url()</code> value. The default setting is to <strong>repeat</strong> the background to cover the background.</p>
      <pre><code>div {
  <span>background-image</span>: url('<span>parchment.jpg</span>');
}</code></pre>
      <div class='demoboxVH demobg' id='demobgimg1'>Resizable</div>
      <p>Images with transparency are rendered above any flat <strong>background-color</strong> of the element. A compact declaration is used to set both below:</p>
      <pre><code>div {
  <span>background</span>: <span>#FF9</span> url('<span>rmit-orb-ghost.png</span>');
}</code></pre>
      <p>Change the background color of the demo below:<br><select onchange="$('#demobgimg2').css('background-color',this.value);">
          <option value='#ff9'>Yellow</option>          
          <option value='#060'>Green</option>
          <option value='#009'>Blue</option>
        </select></p>
      <div class='demoboxVH demobg' id='demobgimg2'>Resizeable</div>
    </section>
  </section>  
  
  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Background Repeating and Position</h1>
      <p>Background repeating can be turned off by using the <code>background-repeat</code> style which takes values <code>initial, repeat-x, repeat-y, no-repeat</code>:</p>
      <pre><code>div {
  <span>background-repeat</span>: <span>no-repeat</span>;
}</code></pre>
      <p>Background images can be fixed using the <code>background-position</code> style which takes values  <code>left, right, center, top, bottom</code> or <strong>numeric</strong> values.</p>
      <pre><code>div {
  <span>background-position</span>: <span>center</span>;
}</code></pre>
      <ul>
        <li><strong>If using 1 word</strong>: The image will be placed there and centered in the other direction.</li>
        <li><strong>If using 2 words</strong>: The image will be placed in the desired vertical and horizontal placement. <i>Make sure you specify a vertical and a horizontal position!</i></li>
      </ul>
      <p>Change the repeating style of the demo below:<br><select onchange="$('#demobgimg3, #demobgimg4 ').css('background-repeat',this.value);">
          <option value='initial'>Inital</option>          
          <option value='repeat-x'>Repeat-X</option>
          <option value='repeat-y'>Repeat-Y</option>
          <option value='no-repeat' selected>No Repeat</option>
        </select></p>
      <p>Change the background position in the demo below:<br><select onchange="$('#demobgimg3').css('background-position',this.value);">
          <option value='initial' selected>Initial</option>          
          <option value='left'>Left</option>          
          <option value='right'>Right</option>
          <option value='center'>Center</option>
          <option value='top'>Top</option>
          <option value='bottom'>Bottom</option>
          <option value='bottom left'>Bottom Left</option>
          <option value='bottom right'>Bottom Right</option>
          <option value='20px 20px'>20px 20px</option>
          <option value='calc(100% - 20px) calc(100% - 20px)'>calc(100% - 20px) calc(100% - 20px)</option>
        </select></p>
      <div class='demoboxVH demobg' id='demobgimg3'>Resizeable</div>
    </section>
  </section>
  
  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Background Size and Origin</h1>
      <p>The size of a background image can be controlled with the <code>background-size</code> style which takes values <code>cover, contain, inital</code> or <strong>numeric</strong> values such as percentages, pixels etc. <i>If only one numeric value is provided, the width is set and the height is resized in the correct proportion.</i></p>
      <pre><code>div {
  <span>background-size</span>: <span>cover</span>;
}</code></pre>
      <p>When <strong>background-size</strong> is combined with <strong>background-position</strong>, we can change the <strong>resize focus point</strong> of an image. For example, I would like the background to focus on the "CSS is Awesome" box on the mug. The top corner is located about 60% in from the left side and 45% down from the top.</p>
      <p>Change the <strong>background size</strong> in the demo below:<br><select onchange="$('#demobgimg4').css('background-size',this.value);">
          <option value='initial'>initial</option>          
          <option value='contain'>Contain</option>
          <option value='cover' selected>Cover</option>
          <option value='50%'>50%</option>
          <option value='50% 50%'>50% 50%</option>
          <option value='150px'>150px</option>
          <option value='150px 150px'>150px 150px</option>
        </select></p>
      <p>Change the background <strong>focus point</strong> in the demo below:<br><select onchange="$('#demobgimg4').css('background-position',this.value);">
          <option value='initial' selected>Initial</option>          
          <option value='left'>Left</option>          
          <option value='right'>Right</option>
          <option value='center'>Center</option>
          <option value='top'>Top</option>
          <option value='bottom'>Bottom</option>
          <option value='60% 45%'>60% 45%</option>
        </select></p>
      <div class='demoboxVH demobg' id='demobgimg4'>Resizable</div>
    </section>
  </section>
  
  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Background Compact Declaration</h1>
      <p>If declaring many background styles in one <strong>compact</strong> declaration, the order is advisable and a <code>/</code> must be used to seperate <strong>position</strong> from <strong>size</strong>. This compact declaration combines <strong>color</strong>, <strong>url</strong>, <strong>position</strong> and <strong>size</strong> into one:</p>
      <pre><code>div {
  <span>background: #393 url('css-is-awesome.png') 60% 45% <span>/</span> cover;</span>
}</code></pre>
      <p>Below is a very tiny mug for you to resize:</p>
      <div id='demobgimg5' class='demoboxVH demobg'></div>
    </section>
  </section>
  
  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Multiple Background Images</h1>
      <p>Why stop at one? Multiple background images can be placed in a single element, for example in these slides ...</p>
      <p id='demobgimg6'><img src='app/img/orb-cres.png' alt='RMIT cresent orb fragment'/> + <img src='app/img/orb-step.png' alt='RMIT steps orb fragment'/> + <img src='app/img/building80-cube.jpg' alt='abstract art background'/> = <img src='app/img/background-multiple.png' alt='Combined background images'/></p>
      <pre><code>html {
  background-image: 
    <span>url('app/img/orb-cres.png'), 
    <span>url('app/img/orb-step.png'), 
    <span>url('app/img/abstract.jpg');</span></span></span>
  background-repeat: <span>no-repeat, <span>no-repeat, <span>no-repeat;</span></span></span>
  background-attachment: <span>fixed, <span>fixed, <span>fixed;</span></span></span>
  background-position: <span>left, <span>right, <span>center;</span></span></span>
  background-size: <span>17vw, <span>17vw, <span>cover;</span></span></span>
}</code></pre>
      <p><i>Note that images declared later (eg abstract image) are placed under images declared earlier (eg orb fragment images).</i></p>
    </section>
  </section>

  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Linear Color Gradient</h1>
      <p>We have already seen the simple flat background color declaration:</p>
      <pre><code>element { background-color: <span>white;</span> }</code></pre>
      <p>But we can also create color gradients for the <strong>background</strong>. The <strong>linear</strong> color gradient style needs a comma delimited list of parameters; a direction followed by a list of colors to blend:</p>
      <pre><code>p {
  background: <span>linear-gradient(<span>left, red, orange, yellow, green, cyan, blue, violet</span>)</span>;
}</code></pre>
      <p id='demolineargradient1' class='demobg'>This paragraph background should have a rainbow starting with red on the <strong>left</strong> and violet on the <strong>right</strong>.</p>
      <p>Here is a less vibrant example of using one opaque color and a semi-transparent color.</p>
      <pre><code>p {
  background: <span>linear-gradient(<span>top, hsla(180,50,100,1), hsla(180,50,100,0)</span>)</span>; 
}</code></pre>
      <p id='demolineargradient2' class='demobg'>This paragraph background should have a solid cyan color starting at the <strong>top</strong> and fading  <strong>downwards</strong>.</p>
    </section>
  </section>

  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Tilted Color Gradients</h1>
      <p>Instead of specifying the <strong>incoming side</strong>, we can replace it with an <strong>angle</strong>. <i>The angle is a little counter-intuitive: <strong>180deg</strong> and <strong>top</strong> are equivalent</i>.</p>
      <pre><code>ul { background: linear-gradient( <span>23deg</span>, ...); }</code></pre>
      <p>Change the angle here: 
        <select onchange="$('#demolineargradient3').css('background','linear-gradient('+this.value+', hsla(180,100%,50%,1), hsla(180,100%,50%,0))')">
          <?php
            for ($i=0; $i<=360; $i=$i+45) 
              echo "<option value='{$i}deg'".($i==180 ? ' selected' : '').">$i degrees</option>";
          ?>
        </select>
      </p>        
      <ul id='demolineargradient3'>
      <?php
        $dirn=['Bottom','Bottom Left','Left','Top Left','Top','Top Right','Right','Bottom Right','Bottom (again!)'];
        for ($i=0; $i<=8; $i++) 
          echo "<li><strong>".($i*45)."deg</strong>: {$dirn[$i]}</li>";
      ?>
      </ul>
      <p><i>NB: Only the compliant browser style is being updated. Non-compliant browsers will see a static vendor gradient instead.</i></p>
    </section>
  </section>
  
  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Repeating Color Gradients</h1>
      <p>Gradients can be used to set up using the <code>repeating-linear-gradient</code> style. It is very similar to the previous style, except that colors after the first color have <strong>stop point</strong> numbers, ie the point where the color stops. These numbers can take be <strong>relative</strong> sizes (eg percentages) or <strong>absolute</strong> sizes (eg pixels) depending on what overall effect you want. In this example, a horizontal curtain effect is created with 10 folds, independent of the element width:</p>
      <pre><code>div { 
  background: <wbr><span>repeating-</span>linear-gradient(270deg, <span>#A00, #F00 <span>5%</span>, #A00 <span>10%</span></span>);
}</code></pre>
      <div class='demoboxVH demobg w' id='demorepeatlineargradient1'>Resizable</div>
      <p>To get "hard" stripes, put in extra copies of the colors with zero width. This removes the color blending effect between different colors. <i>Note <strong>fixed size</strong> option used below.</i></p>
      <pre><code>div { 
  background: <wbr><span>repeating-</span>linear-gradient(315deg, <span>#A00 0px, <span>#A00 10px, #F00 10px,</span> #F00 20px</span>);
}</code></pre>
      <div class='demoboxVH demobg w' id='demorepeatlineargradient2'>Resizable</div>
    </section>
  </section>
  
  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Radial Color Gradients</h1>
      <p>Radial gradients start at the center of an element and spread outwards to the edges. It takes a list of colors.</p>
      <pre><code>p {
  background: <span>radial-gradient(<span>red, white</span>)</span>; 
}</code></pre>
      <div class='demoboxVH demobg' id='demoradialgradient1'>Resizable</div>
      <p>If you want the gradient to stop at the sides rather than the corners, add the word <code>closest-side</code>:</p>
      <pre><code>p {
  background: <span>radial-gradient(<span>closest-side</span>, red, white)</span>; 
}</code></pre>
      <div class='demoboxVH demobg' id='demoradialgradient2'>Resizable</div>
      <p>If you want the gradient to have a cropped circular shape, add the word 'circle':</p>
      <pre><code>p {
  background: <span>radial-gradient(<span>circle</span>, red, white)</span>; 
}</code></pre>
      <div class='demoboxVH demobg' id='demoradialgradient3'>Resizable</div>
    </section>
  </section>

  <section>
    <header>CSS Background<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Repeating Color Radial Gradients</h1>
      <p>Radial gradients can also repeat using the <code>repeating-radial-gradient</code> style. In this example, a cloud like effect is created with 5 rings, independent of the element width:</p>
      <pre><code>div { 
  background: <wbr><span>repeating-</span>radial-gradient( <span>red, white <span>10%</span>, red <span>20%</span></span>); 
}</code></pre>
      <div class='demoboxVH demobg' id='demorepeatradialgradient1'>Resizable</div>
      <p>To get "hard" stripes, put in extra copies of the colors with zero width. This removes the color blending effect between different colors. <i>Note <strong>circle</strong> and <strong>fixed size</strong> options used below.</i></p>
      <pre><code>div { 
  background: <wbr><span>repeating-</span>radial-gradient( circle, <span>red, <span>red 20px, white 20px,</span> white 40px</span>); 
}</code></pre>
      <div class='demoboxVH demobg' id='demorepeatradialgradient2'>Resizable</div>
    </section>
  </section>
    
  <section class='key' id='tp-cssshadow'>
    <header>CSS Shadows<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Box and Text Shadow (or Glow)</h1>
      <div id='demoshadow'>
        <p><img src='app/img/rmit-orb-small.png' alt='rmit orb image' /><span class='ib'>&ldquo; No piece of art can ever be complete</span> <span  class='ib'>without drop shadows &rdquo;</span></p>
        <p> - no one ever</p>   
      </div>
      <p>Drop shadows (darker colors) or glows (lighter colors) can be applied to:</p>
      <ul>
        <li>Inline and block elements using the <code>box-shadow</code> property</li>
        <li>Text using the <code>text-shadow</code> property</li>
      </ul>
      <pre><code>  #demoshadow {  box-shadow: <span>1px 1px <span>3px <span>2px</span></span></span> #333; }
#demoshadow p { text-shadow: <span>1px 1px <span>2px </span></span>    #CC9; }</code></pre>
      <ul>
        <li>The <strong>first 2</strong> numbers control the direction of the shadow. In this example, the box and text shadows are pushed right and down by 1 pixel.</li>
        <li>The <strong>3<sup>rd</sup></strong> number controls the size of the 'blur' of shadow/glow (ie the fuzzy part). In this example the box shadow is 1 pixel larger than the text shadow.</li>
        <li>The <strong>4<sup>th</sup></strong> <em>optional</em> number controls the size of the shadow/glow (ie the strong part). <i>Only box elements have this option.</i></li>
        <li>The <strong>last</strong> item indicates the color of the glow / shadow. In this example, the <strong>box-shadow</strong> is <strong>dark grey</strong> and the <strong>text-shadow</strong> is a <strong>dark yellow</strong> to match the text color.</li>
      </ul>
    </section>
  </section> 

  <section>
    <header>CSS Shadows<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Inset and Image Shadow (or Glow)</h1>
      <div id='demoshadow2'>
        <p><img src='app/img/rmit-orb-small.png' alt='rmit orb image' /><span class='ib'>&ldquo; A glow is just a shadow</span> <span class='ib'>in a lighter shade &rdquo;</span></p>
        <p>- every Disney prince and princess</p>
      </div>
      <p>The <strong>box-shadow</strong> property has one final option. Insert the word <code>inset</code> and the shadow appears inside the box.</p>
      <pre><code>#demoinsetshadow { box-shadow: <span>inset</span> 1px 1px 3px 2px #333; }</code></pre>
      <p>To give images with transparency a shadow or glow, use the <code>filter: drop-shadow()</code> style:</p>
      <pre><code> img {  <span>filter: drop-shadow( <span>1px 1px 4px #fff</span> );</span> }</code></pre>
      <p>Like the <strong>text-shadow</strong> style, the first two numbers control the direction of the shadow, the third number controls the blur, and the final number controls the color.</p>
      <p><strong>For advanced users:</strong> The filter style can also modify opacity, color and contrast; invert, blur and enhance images. <i>You don't need Photoshop to make simple modifications anymore!</i></p>   
    </section>
  </section>

  <section id='tp-csslayout' class='key'>
    <header>CSS Layout<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Display Style</h1>
      <p>The <code>display</code> style provides a large range and growing number of different layout styles. These include:</p>
      <ul>
        <li><code>inline</code>, <code>block</code>, <code>inline-block</code>, <code>run-in</code>,</li>
        <li><code>table</code>, <code>inline-table</code>, <code>table-row</code>, <code>table-cell</code>,</li>
        <li><code>none</code>, <code>initial</code>, <code>inherit</code>,</li>
        <li><code>list-element,</code></li>
        <li><code>flex</code>, <code>inline-flex</code> (both new in CSS3 with varying browser support)</li>
      </ul>
      <p>eg <code>display:<span> block</span>;</code> displays an element as a block.</p>
    </section>
  </section>  

  <section>
    <header>CSS Layout<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Block v/s Inline</h1>
      <p><strong>Block</strong> elements and <strong>Inline</strong> elements can have their display setting changed using the <code>display</code> style:</p>
      <pre><code>nav li {
  display: <span>inline</span>;
}</code></pre>
      <p>Turns list elements inside a <strong>nav</strong> element into <strong>inline</strong> elements.</p>
      <pre><code>aside a {
  display: <span>block</span>;
}</code></pre>
      <p>Turns hyperlinks inside an <strong>aside</strong> element into <strong>block</strong> elements.</p>
    </section>
  </section>    
  
  <section>
    <header>CSS Layout<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Inline Block</h1>
      <p id='demoinlineblock'><span>Content inside <strong>inline elements</strong> can split onto two lines,</span> and <span><strong>block elements</strong> are put on their own line.</span> <span><strong>Inline-Blocks</strong> allow an element to keep its <strong>internal</strong> layout fixed as a block</span> but <span>leaves it free to "float" along with content in its parent element</span> as if it is <span>inline "on the outside".</span> For example, in this paragraph, <span>there are a few <strong>&lt;spans&gt;</strong></span> that can be changed into <span><strong>block</strong> and <strong>inline-block</strong> elements.</span></p>
      <pre><code>span {
  display: <span>inline-block</span>;
}</code></pre>
      <p>Change spans in the top paragraph to <select oninput="$('#demoinlineblock span').css('display',this.value).css('border','2px solid orange').css('margin','1px').css('background-color','#FED');">
        <option value='inline'>Inline</option>
        <option value='block'>Block</option>
        <option value='inline-block'>Inline-Block</option>
      </select></p>
      <p><strong>Handy Tip:</strong> Hyperlinks styled as <strong>inline-blocks</strong> in a horizontal navigation bar makes sure that the link text does not break across lines:</p>
      <pre><code>nav a {
  display: <span>inline-block</span>;
}</code></pre>
    </section>
  </section>  

  <section>
    <header>CSS Layout<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Display v/s Visibility</h1>
      <p>There is a difference between <code>display: <span>none</span>;</code> where the element is removed from the layout and <code>visibility: <span>hidden</span>;</code> where the element is made invisible but space for the element is not removed.</p>
      <pre><code>span { display: <span>none</span>; }</code></pre>
      <pre><code>span { visibility: <span>hidden</span>; }</code></pre>
      <p><span id='demodisplaynone'>The display style of the text inside this span can be changed here </span><select oninput="$('#demodisplaynone') .css('display',this.value)"><option value='inline'>Inline</option><option value='none'>None</option></select> <span id='demovisibility'>and the visibility style of the text inside this span can be changed here </span><select oninput="$('#demovisibility').css('visibility',this.value)"><option value='visible'>Visible</option><option value='hidden'>Hidden</option></select></p>
    </section>
  </section>    


  <section id='tp-cssposition' class='key'>
    <header>CSS Position<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Static and Custom Position</h1>
      <p>In previous examples we have seen that elements are laid out in the same order that they appear in the document. This order is termed the "normal document flow" and the positioning style is <strong>static</strong>.</p>
      <pre><code>div {
  position: <span>static</span>;
}</code></pre>
      <p>However, we can position elements so that they are:</p>
      <ul>
        <li><code>relative</code>, which is like <strong>static</strong>, but the element can be moved <strong>relative</strong> to its original position.</li>
        <li><code>absolute</code>, which will position the element relative to the root HTML element, or an ancestor element that has non-static position. This element will scroll with the page.</li>
        <li><code>fixed</code>, which will position the element relative to the browser window. This element won't scroll with the page.</li>
        <li><code>initial</code>, which restores the stylesheet setting.</li>
        <li><code>inherit</code>, which takes on its parent setting.</li>
      </ul>
      <p>Non-static positions will take the element out of the normal flow, so you may need to add <strong>margin</strong> and <strong>padding</strong> to make sure content clears <strong>absolute</strong> and <strong>static</strong> elements.</p>
      <p>In these slides, the <strong>&lt;nav&gt;</strong> area swaps between <strong>static</strong> and <strong>fixed</strong> position when you scroll up and down. To keep scrolling smooth and seamless, extra padding is added to the <strong>&lt;header&gt;</strong> when the <strong>&lt;nav&gt;</strong> becomes static.</p>
    </section>
  </section>    

  <section>
    <header>CSS Position<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Top, Bottom, Left, Right</h1>
      <p>In order to move <strong>non-static</strong> elements, we need to specify how far to move them. These attributes are distances, so the usual px, %, em etc units can be used.</p>
      <pre><code>top: <span>5px</span>;
bottom: <span>5em</span>;
left: <span>5%</span>;
right: <span>5vw</span>;</code></pre>
      <p>Note that you just need to pick one or two of these: <strong>top</strong> and <strong>bottom</strong> control vertical placement (ie don't chose both); <strong>left</strong> and <strong>right</strong> control horizontal placement (ie don't chose both).</p>
      <p><span id='demoposition'>Adjust this span element's position</span>
        <select id='selectposition' onchange="$('#demoposition').css('position',this.value);">
          <option value='static'>static (default)</option>
          <option value='relative'>relative</option>
          <option value='absolute'>absolute</option>
          <option value='fixed'>fixed</option>
        </select>
        <select onchange="$('#demoposition').attr('style','position: '+$('#selectposition').val()+'; '+this.value);">
          <option value='top: 0px;'>top: 0px</option>
          <option value='top: 15px;'>top: 15px</option>
          <option value='bottom: 0px;'>bottom: 0px</option>
          <option value='bottom: 15px;'>bottom: 15px</option>
          <option value='left: 0px;'>left: 0px</option>
          <option value='left: 15px;'>left: 15px</option>
          <option value='right: 0px;'>right: 0px</option>
          <option value='right: 15px;'>right: 15px</option>
        </select>
      </p>
    </section>
  </section>  

  <section>
    <header>CSS Position<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Z-Index</h1>
      <p>To arrange <strong>non-static</strong> positioned elements in 3D, elements need to have a <strong>z-index</strong> integer value. The higher the number, the more "on top" it is, with <strong>0</strong> being the default value. Values can also be negative if you want elements to be hidden behind <strong>static</strong> elements.</p>
      <pre><code>div.card {
  z-index: <span>0</span>;
}</code></pre>
      <p>Each of the following draggable cards has a z-index (printed on the card).</p>
      <p>Can you find the <code>z-index: <span>-1</span></code> card?</p>
      <div style='position: relative; '>
      <?php 
        for($i=-1; $i<10; $i++) {
          echo "<div class='draggable card' style='left:".($i*25+25)."px; z-index:$i;'>$i</div>";
        }   
      ?></div>
    </section>
  </section>

  <section>
    <header>CSS Layout<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Flex Layout</h1>
      <p>Before the <code>flex</code> layout model was created in CSS3, a simple, intuitive and dependable layout model was difficult to create.</p>
      <p>Child elements inside a parent element styled with <code>display: <span>flex</span>;</code> will shrink and grow to fit the height and width of the parent container. When <code>flex: <span>##</span>;</code> is employed in child elements, each element will take into account widths of other <strong>siblings</strong> and rescale in proportion with any other siblings using the <strong>flex</strong> style.</p>
      <p>In the example below, the <strong>&lt;aside&gt;</strong> has a fixed width, but the <strong>&lt;main&gt;</strong> element will always be 5 times wider then the <strong>&lt;nav&gt;</strong> element.</p>
      <div style='background-color:#bbb; text-align:center'>&lt;header&gt;</div>
      <div style='display: flex; text-align:center'>
        <div style='order: 3; width:150px; background-color:#ffe'>&lt;aside&gt;</div>
        <div style='order: 2; flex:5; text-align: left; background-color:#efe;'>
<?php he("<div>&lt;header&gt;</div>\n
<div style='display:flex; text-align:center'>\n
  <div style='order:3; width:150px;'> &lt;aside&gt; </div>\n
  <div style='order:2; flex:5;'> &lt;main&gt; </div>\n
  <div style='order:1; flex:1;'> &lt;nav&gt; </div>\n
</div>\n
<div>&lt;footer&gt;</div>"); ?></div>
        <div style='order: 1; flex:1; background-color:#def'>&lt;nav&gt;</div>
      </div>
      <div style='background-color:#bbb; text-align:center'>&lt;footer&gt;</div>
      <p>Finally, note that the order of child elements can be adjusted using <code>order: <span>##</span>;</code>so that they do not need to be laid out in the same order as they appear in the code. In the above example, the <strong>&lt;nav&gt;</strong> element appears after the <strong>&lt;aside&gt;</strong> element in the code, but is rendered before either the <strong>&lt;nav&gt;</strong> or <strong>&lt;nav&gt;</strong> elements.</p>
      <blockquote>As discussed, the flex model is new, has undergone a number of revisions, but promises to be a better solution for resolving page layout issues in the near future.<br>It will not be covered in any great detail in this course, but if interested, please have a look at this indepth tutorial which is regularly maintained:<br><?php a('https://css-tricks.com/snippets/css/a-guide-to-flexbox'); ?></blockquote>
    
    </section>
  </section>   

</article>