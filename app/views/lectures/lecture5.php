<?php
  $counter=1;
  $slideText='Lect 5, P.';
?>
<article id='slides-lecture-5'>

  <section class='title'>
    <header>Welcome!<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Web Programming
        <br>COSC2413/2426/2453</p>
      <p>Lecture 5</p>
    </section>
  </section>

  <section class='title'>
    <header>Lecture 5.1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 1: Human Computer Interaction (HCI)</p>
    </section>
  </section>

  <section class='key'>
    <header>HCI<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-usability'>Creating Usable Websites</a></li>
        <li><a href='#tp-accessibility'>Accessibility Concerns</a></li>
        <li><a href='#tp-audiences'>Targeting Audiences</a></li>
        <li><a href='#tp-develprocess'>Development Process</a></li>
      </ol>
    </section>
  </section>

  <section class='key' id='tp-usability'>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Why Care About Users?</h1>
      <p><img class='fl2' src='app/img/darn-these-hooves.jpg' alt='Cow pilots with clumbsy hooves, unable to fly a plane built by racoons' /></p>
      <p>
        <ul>
          <li>What is wrong?</li>
          <li>Why is it wrong?</li>
          <li>What can be done to prevent these kinds of mismatches?</li>
        </ul>
      </p>
    </section>
  </section>

   <section class='key' id='tp-usability'>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Human Computer Interaction</h1>
      <p>HCI is the study of interaction between human and computers.</p>
      <p><img class='fl3' src='http://titan.csit.rmit.edu.au/~e57478/wp/L5/hci.png' alt='HCI cycle: design, implementation, evaluation' /></p>
    </section>
  </section>

  <section class='key' id='tp-develprocess'>
    <header>Development Process<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Why Have A Design Process?</h1>
      <p>It has been estimated that over 60% of large software projects go over cost, often because:</p>
      <ul>
        <li>Clients often do not understand their own requirements.</li>
        <li>As development progresses, overlooked tasks and requirements become obvious.</li>
        <li>As developement progresses, clients request changes, unaware of the extra work required.</li>
        <li><strong>There is insufficient client-developer-user communication and understanding.</strong></li>
      </ul>
      <p>When it comes to initial planning and design, "pay a little more now, or pay a lot more later" can come back to haunt both the client and the developer. If the developer is not careful, a losely worded contract can turn what looked like a profitable venture into an ongoing loss-making saga.</p>
    </section>
  </section>  
  
  <section class='key' id='tp-develprocess'>
    <header>Development Process<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Why An Interface Design Process?</h1>
      <img class="fl4" src="http://titan.csit.rmit.edu.au/~e57478/wp/L5/design.jpg" alt="design fail image" /><!-- http://guff.com/75-you-had-one-job-moments -->
      <p>It's very easy to jump into detailed design that:</p>
      <ul>
        <li>Is founded on incorrect requirements.</li>
        <li>Has inappropriate dialogue flow.</li>
        <li>Is not easily used.</li>
        <li>Is never tested until it is too late.</li>
      </ul>
      <p>Note that usability is vital in many areas, not just the web, and not just computer science!</p>
    </section>
  </section>  

  <section class='key' id='tp-develprocess'>
    <header>Interfaces<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Designing Interfaces</h1>
      <p>The design of an interface requires understanding of <em>users</em>, and their <em>tasks</em>.</p>
      <p>Designing with users:
      <ul>
        <li>Develop task examples, together with users</li>
        <li>Build prototypes</li>
        <li>Evalute through observation of users</li>
      </ul>
      </p>
      <p>
        Designing visual interfaces goes beyond screen layout and use of pretty graphics:
        <ul>
          <li>Representations and metaphors</li>
          <li>Placement of interface components on a screen</li>
        </ul>
      </p>
    </section>
  </section>  

  <section class='key' id='tp-develprocess'>
    <header>User-Centred System Design<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>User-Centred System Design</h1>
      <p>Design should be based on a user's:
      <ul>
        <li>abilities</li>
        <li>context</li>
        <li>tasks</li>
        <li>real needs</li>
      </ul>
      </p>
      <p>It's important to consider all potential users, e.g. novice, expert,...</p>
    </section>
  </section>  

  <section class='key' id='tp-audiences'>
    <header>Targeting Audiences<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Know Your Audience</h1>
      <p>Client and developers often have firm opinions about what their clients need and want and mis-judge the way their customers will use the website, but do they involve the clients in the dialog?</p>
      <p class='c'><img class='c' src='app/img/xkcd_university_website.png' /></p>
      <p><strong>Stakeholders</strong> is a term used to refer to all people involved with a project, including end-users (ie customers). As much as possible, involve all stakeholders in designing, testing and evaluating a new system.</p>
    </section>
  </section>  
  
  <section>
    <header>Targeting Audiences<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Audience Diversity</h1>
      <p>Consider the RMIT Website. What types of audience (ie customers) will want to use the website?</p>
      <p class='c'><img src='app/img/RMITwebsite.png' /></p>
      <details>
        <summary>There is more than one ...</summary>
        <ul>
          <li>Current Students</li>
          <li>Prospective Students</li>
          <li>Current Staff</li>
          <li>Prospective Staff</li>
          <li>Australian Government Auditors</li>
          <li>International University Rankers, eg <?php a('http://www.topuniversities.com/'); ?></li>
        </ul>
      </details>
    </section>
  </section>  
  
  <section>
    <header>Targeting Audiences<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Catering to Different Audiences</h1>
      <p>Often a webpage will appeal to one type of audience, but will alienate another. Consider how a charterboat such as the <?php a('http://www.victoriastar.com.au/','Victoria Star'); ?> appeals to university students wanting to host a "night-club" like party without alientating those looking to host their wedding or corporate events.</p>
      <p>A generally accepted design pattern is to have:</p>
      <ul>
        <li><strong>General Landing Pages:</strong> pages that appeal to the widest possible audience which contain areas to more specialised "sub-areas" or "sub-pages".</li>
        <li><strong>Sub Pages / Areas:</strong> pages that are customised to appeal to a particular audience.</li>
      </ul>
      <p>This process can be repeated for sub-sub groups, particularly in large organisations. For example, <?php a('https://www.crownmelbourne.com.au','Crown Casino'); ?> has a diverse and complex business structure and its website channels customers through a series of general and specialised landing pages before directing them to a final suitable page.</p>
    </section>
  </section>  
  

  
  <section class='key' id='tp-usability'>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Web Design</h1>
      <p>When building a website, you can fall into the trap of developing a website that looks good and works well on your machine <strong><i>and just your machine!</i></strong></p>
      <p>
        <ul>
          <li>Not all users will have the same computer and screen as you.
          <ul>
            <li>Should test your website to see how it adapts to different size screens.</li>
            <li>Resize your browser window to be wider and narrower to simulate different screen sizes and test your work in more than one browser.</li>
          </ul>
          <li>Users may be on the move, viewing your page on a mobile device with limited access to the internet.</li>
          <ul>
            <li>Make sure that your links and bottons are large enough to click and that images and multimedia resources are not larger than they need to be.</li>
          </ul>
          <li>Be aware that space at the top of a page, commonly referred to as <strong>above the line</strong> is far more valuable than space <strong>below the line</strong>, ie which can only be reached by scrolling.</li>
          <ul>
            <li>Make sure that the most important material is near the top of the page.</li>
            <li>Users will only scroll down if you have captured their attention and they have a sense that there is useful information to be found.</li>
          </ul>
        </ul>
        </p>
    </section>
  </section>

  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Website Page Design</h1>
      <p>Each web page should be consistently designed across a website. Each page should:</p>
      <ul>
        <li>Have a banner with title and ideally a logo.</li>
        <li>Contain an indication of which page the user is on.</li>
        <li>Have easily accessible buttons for navigation.</li>
        <li>Have a footer which contains contact details, privacy &amp; policy links.</li>
        <li>Contain data (can be meta-data) about the author, creation date and last revision date.</li>
      </ul>
      <p>Having a webpage template and common external style sheets will help you achieve this.</p>
    </section>
  </section>
  

  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Navigation Bars</h1>
      <p>A navigation bar is a collection of links to major pages on the site. They should, in general, should look and behave the same way on every page.</p>
      <ul>
        <li>Navigation bars can be placed at the top, bottom, or side of each page, and should include links to the essential pages of the site.</li>
        <li>A user may come to your site on any page from a search engine result or bookmark. Make sure you have a link to the home page on every page.</li>
      </ul>
      <p>In the assignment, you are asked to style your navigation links to look button-like.</p>
    </section>
  </section>
  
  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Main Content Area</h1>
      <p>People read differently online: on the web, readers tend to skim.</p>
      <p>It's important to keep paragraphs short: make them easy to understand, but not so short that they say nothing.</p>
      <p>Similarly, it's important to make your text as readable as possible:</p>
      <ul>
        <li>Avoid marketing terminology and advertising copy.</li>
        <li>Use objective language.</li>
        <li>Keep your audiences in mind (there's a theme here!)</li>
      </ul>
    </section>
  </section>
  
  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Robin Williams' Page Design</h1>
      <p>Frequently used principles for designing a page are:</p>
      <ul class='crap'>
        <li><strong>Contrast:</strong> different elements should be very different.</li>
        <li><strong>Repetition:</strong> repeating elements should have the same design layout.</li>
        <li><strong>Alignment:</strong> everything on a page should be there for a reason and have a visual connection to something else.</li>
        <li><strong>Proximity:</strong> related elements should be close together.</li>
      </ul>
      <p>These principles can be applied by computer scientists, regardless of your ability as a graphic artist. For more information, visit <?php a('http://www.ratz.com/robin'); ?>.</p>
    </section>
  </section>  

  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Contrast</h1>
      <p>Different elements should be very different.</p>
      <ul>
        <li>Differences in colour, size, or shape can be used to distinguish one text (or images) from other text (or images).</li>
        <ul>
            <li>That is, if two elements are not intended to be treated as being "the same", then don't make them look similar.</li>
        </ul>    
        <li>Examples for text include changing the formatting  to <strong>bold</strong>, <i>italic</i>, or CAPS.</li>
        <ul>
          <li>But this only works if used sparingly...</li>
        </ul>
        <li>For a page overall, consider whether the difference between areas (e.g. content, ads, navigation areas, user comments, etc.) are easily distinguishable.</li>
        <li>Contrast can also be used to give <a href="http://titan.csit.rmit.edu.au/~e57478/wp/L5/contrast.html" target="_blank">emphasis to certain page elements</a>.</li>
      </ul>
      </section>
  </section>  

  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Repetition</h1>
      <p>Repeating visual elements of a page lends the content unity and cohesiveness, and makes it easier for a user to understand what is being presented.</p>
      <ul>
        <li>Content that appears on multiple pages (e.g. a navigation bar) should appear in the same place, and look the same, on each page.</li>
        <ul>
          <li>Improves readability, as the user can easily identify different types of content.</li>
        </ul>
        <li>Format text consistently.</li>
        <ul>
          <li>If sub-section headings are in bold and 14pt font, then all sub-section headings should look like this.</li>
        </ul>
        <li>While it's sometimes tempting to be creative, usually it's worth striving for consistency with conventions established by other websites.</li>
        <ul>
          <li>Jakob Nielsen's "Law" of the Web User Experience: "users spend most of their time on other websites".</li>
        </ul>
      </ul>
      </section>
  </section>  

  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Alignment</h1>
      <p>Aligning layout elements has a direct impact on the readability and comprehension of page content.</p>
      <ul>
        <li>Typically documents should have a small number of vertical baselines.</li>
        <ul>
          <li>For example, standard paragraphs, indentation levels of lists, etc.</li>
        </ul>
        <li>Alignment directly conveys notions of structure.</li>
        <ul>
          <li>Things that are related, or are of the same "importance", should have the same alignment.</li>
        </ul>
        <li>Alignment is also important for things such as forms. Having form inputs aligned correctly can make the form much easier to use.</li>
      </ul>
      </section>
  </section>  

  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Proximity</h1>
      <p>Things that are related, or logically belong together, should be located near each other.</p>
      <ul>
        <li>Also called the <em>Gestalt Principle</em> -- humans perceive relationships between things that are closer together.</li>
        <li>It it usually helpful to group related items together.</li>
        <ul>
          <li>Consider menus in software applications -- commands are not in one giant list, but rather grouped by related functionaliy.</li>
        </ul>
      </ul>
      </section>
  </section>  

  <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Use of Images</h1>
      <p>Graphics can dramatically enhance the look of a web site, especially when they have a consistent "look and feel", or are developed as part of an integrated site theme.</p>
      <p>Whilst images should always supplement text, a well composed picture can be more effective than a few paragraphs of sales and marketing text. Consider the picture below:</p>
      <p class='c'><img src='app/img/cinema.jpg' alt='People watching a film in a cinema with 3D glasses'/></p>
      <ul>
        <li>How much information is communicated instantly to the user?</li>
        <li>How many words would it take you to describe everything that you see?</li>
        <li>How likely is it that a user would read all those words?</li>
      </ul>
    </section>
  </section>
  
   <section>
    <header>Creating Usable Websites<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Site Maps, Indexing Pages, and Search</h1>
      <p>The assignments in this course focus on the needs of a small business; but for larger websites, site maps, index pages and search facilities improve accessibility.</p>
      <p><strong>Site maps</strong> act as a table of contents for your web site. Like a table of contents in a book, they need not list every page on the site, but should provide the visitor with an overview of the main areas and sub-areas within the site.</p>
      <p><strong>Indexing pages</strong> are like a site map, but links are laid out in alphabetical order, not in a logical order. It can be helpful for users looking for a particular issue or concept, particularly useful in technical websites.</p>
      <p>Finally, a <strong>search facility</strong> allows users to locate information within your site. This website's search box allows you to search topics that you might be having trouble understanding or locate a specific slide. Every slide with your search phrase will appear in the order that they are presented in this course.</p>
    </section>
  </section> 
  
  <section class='key' id='tp-accessibility'>
    <header>Accessibility Concerns<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Disability Discrimination Act</h1>
      <p>The Disability Discrimination Act makes it against the law to discriminate against someone on the basis of disability, including in the provision of goods, services and facilities.</p>
      <blockquote>It's not just a good idea. It's the law!</blockquote>      
      <p>The definition of disability is wide, but web developers need to be concerned with accessability needs related to:
      <ul>
        <li><strong>Physical:</strong> Does your website discriminate those who cannot use a mouse or a keyboard? Make sure that users can use the tab key to navigate form elements, using <code>tabindex=<span>{number}</span></code> in input elements if need be.</li>
        <li><strong>Visual:</strong> Does your website have good contrast between background and foreground? Does it use readable fonts for large blocks of text? This is good practice for all users, but when catering for users with visual impairment and color blindness this is <strong>non-negotiable</strong>. A good website for theme generators and color blindness simulation can be found here: <?php a('http://paletton.com'); ?>.</li>
        <li><strong>Intellectual:</strong> When writing, make sure that your sentences are simple and concise. <span title='If it is not obvious, I am being an onanist to demonstrate a point!'>Some may pontificate on whether it is axiomatic to be propitious or eleemosynary when scrivening, others will be happy to take your customers who are alienated by your website's overt obscurification of content.</span></li>
      </ul>
    </section>
  </section>
  
  <section>
    <header>Accessibility Concerns<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Your Responsibility</h1>
      <p>The Disability Discrimination Act does not require that web pages be restricted only to plain black and white Arial text, however, you must supply an effective, accessible alternative unless this is not reasonably possible.</p>
      <p>It is technically feasible to remove many barriers to equal access for people in a way which does not detract from the usefulness or attractiveness of web pages to others. For example:</p>
      <ul>
        <li>Provide a <strong>descriptive</strong> alt attribute in image tags.<br><code>alt='<span>cinema image</span></code>' <i>- Bad!</i><br><code>alt='<span>Young couple enjoying a 3D movie whilst eating popcorn</span>'</code> <i>- Good!</i></li>
        <li>Use a sensible amount of padding and margin around elements: not too little, not too much.</li>
        <li>Colour schemes should have good contrast. <i><strong>Do not create “rainbow” websites!</strong></i></li>
        <li>Supply a transcript or description for audio and video files, eg <?php a('http://www.abc.net.au/7.30/content/2015/s4431839.htm','ABC 7.30 Report'); ?>.</li>
      </ul>
    </section>
  </section>  
  
  <section>
    <header>Development Process<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Agile and Waterfall Development Models</h1>
      <p>Usability engineering is software engineering.</p>
      <h2>Waterfall model (more rigid). Project has sequential stages:</h2>
      <ul>
        <li>Analysis &amp; design</li>
        <li>Implementation &amp; testing</li>
        <li>Evaluation &amp; maintenance</li>
      </ul>
      <p>Suitable for large projects where clients and developers need to fully understand requirements before undertaking work. Protects developers against any "feature creep" and a disasterous budget blowout.</p>
      <h2>Agile model (more flexible):</h2>
      <ul>
        <li>Project has sequential stages</li>
        <li>Work is delivered in regular weekly / monthly cycles.</li>
      </ul>
      <p>Often smaller projects with a good client-developer partnership benefit from an Agile approach.</p>
    </section>
  </section>  

   <section>
    <header>HCI Demo: How usable is the RMIT website?<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Consider the following Use-Case example</h1>
      <p>The semester is over, your assignments marks were good, and a half-decent mark on the exam is all you need to pass the course.</p>
      <p>When you sit the exam, you feel confident that you did well and expect your final grade to be a credit or distinction. You decide to take a short holiday.</p>
      <p>When you return, you discover that you failed the course, which means your exam grade must be very low.</p>
      <p>Before you contact the lecturer, you decide that you want to know what the correct procedure is before querying your grade, so you turn to the RMIT website.</p>
      <h2>Working in pairs</h2>
      <p>Go to <a href="http://www.rmit.edu.au">www.rmit.edu.au</a>.</p>
      <ul>
        <li>Both of you decide how to navigate the site and which links to click.</li>
        <li>One person does the actual link clicking.</li>
        <li>The other records the number of clicks. <i>Make sure you count any backspaces as a click!</i></li>
      </ul>
      <details>
        <summary>How many clicks did it take you to find what you needed? Did you find what you needed?</summary>
        <ol>
          <li>If something happened to you during the exam (ie before your holiday) and you need to apply for special consideration, here is a handy <a href="http://www1.rmit.edu.au/browse;ID=g43abm17hc9w">link</a> and <?php a('http://mams.rmit.edu.au/v5wa9w985ytf1.gif','flowchart'); ?>.</li>
          <li>After your holiday, find out when the exam review is being held.</li>
          <li>Contact the lecturer if it has not been announced and ask to see your paper.</li>
          <li>If you spot any potential mistakes, you can ask to have the points of contention reviewed. <i>NB: a re-mark of an exam could potentially result in a higher mark, or a lower mark</i>.</li>
          <li>If this does not work, start an <?php a('http://www1.rmit.edu.au/students/appeals','appeals process'); ?>.</li>
          <li>Talk to the <?php a('http://www.su.rmit.edu.au/help/appeal-against-assessment','Student Union'); ?>.</li>
        </ol>
      </details>
      
    </section>
  </section>  

  <section>
    <header>HCI - Past Assignment Review<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>The Hobbit: Battle of the 5 Armies</h1>
      <p>In 2014, we set an assignment to develop a website in time for the release of the final chapter in Peter Jackson's "ever so slightly stretched out" adaption of J.R.R Tolkien's The Hobbit.</p>
      <p class='c'><img src='app/img/battleof5armies.png' alt='Extremely poor rendition of Bilbo and Gandalf in foreground, with Smaug getting hit by an arrow in the distance' /></p>
      <p>A HCI review of assignment can be found here: <?php a('https://docs.google.com/document/d/1AN9zYZkdkMTHghW_pDQYiO4j66KMTzr6gkVEPhUAhFI','Marking Notes, Assignment 1, 2014'); ?></p>
      <p></p>
    </section>
  </section>    
  
</article>