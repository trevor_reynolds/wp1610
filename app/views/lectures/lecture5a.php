<?php
  $counter=1;
  $slideText='Lect 5, P.';
?>
<article id='slides-lecture-5'>

  <section class='title'>
    <header>Welcome!<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Web Programming
        <br>COSC2413/2426/2453
        <br>Semester 1, 2016 +</p>
      <p>Lecture 5</p>
    </section>
  </section>

  <section class='key' id='tp-w4reflections'>
    <header>Week 5 Reflections<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Lecture Topics</h1>
      <p>Last week [[ more words here ]].</p>
      <p>What we will look at today is [[ more words here ]].</p>
      <p>Finally, we will look at [[ more words here ]].</p>
    </section>
  </section>

  <section>
    <header>Week 5 Reflections<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Tute, Lab and Assignment Related Activities</h1>
      <p>In tutes last week, you covered [[ more words here ]].</p>
      <p>In labs, you should have [[ more words here ]].</p>
    </section>
  </section>

  <section class='title'>
    <header>Lecture 6.1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 1:  [[ Content to Follow ]]</p>
    </section>
  </section> 
  
</article>