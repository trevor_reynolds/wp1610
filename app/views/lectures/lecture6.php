<?php
  $counter=1;
  $slideText='Lect 6, P.';
?>
<article id='slides-lecture-6'>

  <section class='title'>
    <header>Welcome!<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Web Programming
        <br>COSC2413/2426/2453</p>
      <p>Lecture 6</p>
    </section>
  </section>

  <section class='title'>
    <header>Lecture 6.1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 1: Javascript, Client Side Programming</p>
    </section>
  </section>

  <section class='key'>
    <header>Javascript - Client Side Programming<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-jsintro'>Introduction to Javascript</a></li>
        <li><a href='#tp-jselements'>Javascript Elements</a></li>
        <li><a href='#tp-jsoperates'>Operators</a></li>
      </ol>
    </section>
  </section>

  <section class='key' id='tp-jsintro'>
    <header>Introduction to Javascript<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>What is JavaScript?</h1>
      <p>JavaScript is a scripting language which allows you to add interactivity to your web pages.</p>
      <p>Originally developed by Brendan Eich at Netscape in 1995 under the names Mocha, LiveScript, then Javascript.</p>
      <p>In the late 1990s and early 2000s, work to create a universal standard called ECMA script was stalled by Javascript’s popularity and competition from Microsoft’s jScript and Adobe’s Action Script.</p>
    </section>
  </section>

<section>
    <header>Introduction to Javascript<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Don’t Get Confused!</h1>
      <p>JavaScript and Java have vaguely similar syntax, but are two completely different languages:</p>
      <ul>
        <li>Java is a full-featured compiled programming language developed by Sun Microsystems.</li>
        <li>JavaScript was developed by Netscape and was renamed "Javascript" as part of a licensing agreement with Sun Microsystems.</li>
      </ul>
      <p>In 2006, Brendan Eich offered his opinion about Javascript being superseded by ECMAscript:</p>
      <p><q><i>Some insist on using .es, but .es is not likely to become popular. ECMAscript was always an unwanted trade name that sounds like a skin disease.</i></q></p>
      <p>Today, Javascript is the default client-side scripting language in HTML5</p>
    </section>
  </section>

<section>
    <header>Introduction to Javascript<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>What does JavaScript do?</h1>
      <p>JavaScript is a client-side scripting language, that is, it runs on the client machine, typically, in your web browser!</p>
      <p>JavaScript is interpreted (evaluated line by line as it is run).</p>
      <p>When a page loads or unloads, or as a user interacts with controls or any elements on a web page, an <strong>event</strong> is generated and an <strong>event-handler</strong> can run some code or call a javascript function.</p>
      <p>Like CSS, javascript can be:</p>
      <ul>
        <li>Written <strong>inline</strong> to run on the fly.</li>
        <li><strong>Embedded</strong> into a page (usually the head element).</li>
          <pre><code>&lt;script&gt;<span> ... your javascript code here ... </span>&lt;/script&gt;</code></pre>
        <li>Stored in an <strong>External</strong> file, which can be linked to many pages (no &lt;script&gt; tags required in this case).</li>
          <pre><code>&lt;script src="myScript.js"&gt;&lt;/script&gt;</code></pre>
      </ul>
      <p>Some browsers will not have javascript enabled. You should put in a helpful message for these users (ie a prompt to run it on or alternative content</p>
      <pre><code>&lt;noscript&gt;<span> ... helpful message for those without javascript enabled ... </span>&lt;/noscript&gt;</code></pre>
    </section>
  </section>

  <section>
    <header>Introduction to Javascript<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>What can I use JavaScript for?</h1>
      <p>Typical uses of JavaScript include:</p>
      <ul>
        <li>Form validation</li>
        <li>Pop-up boxes</li>
        <li>Opening a new browser window (or several)</li>
        <li>User behavior tracking (eg Google Analytics)</li>
        <li>Games and Animation</li>
        <li>Banner advertising</li>
        <li>Simple calculators (home loan, blood alcohol)</li>
        <li>Generating AJAX requests to the server</li>
      </ul>
      <p>You have already seen a few examples in these slides:</p>
      <ul>
        <li><strong>Cute Kitten:</strong> interacting with the kitten activates audio elements.</li>
        <li><strong>Quiz Boxes:</strong> selecting an answer activates helpful (or snide) feedback.</li>
        <li><strong>RGB &amp; HSL sliders:</strong> adjusting the slider changes the background color of elements, simulating a pixel channel or screen color change.</li>
        <li><strong>Menu Search:</strong> makes any slide with your phrase visible and hides all other slides.</li>
        <li><strong>Font Adjust:</strong> makes the <strong>&lt;main&gt;</strong> element font and child element fonts larger and smaller (ie most if not all fonts in the slides).</li>
      </ul>
      <p><i>Why not use the search feature to find some of those slides? Just remember to clear the search field and you will be returned here ... ok, near here (still working on that).</i></p>
    </section>
  </section>

  <section>
    <header>Introduction to Javascript<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>JavaScript Example</h1>
      <p>In this example:</p>
      <ul>
        <li>The <strong>external</strong> javascript file <strong>tools.js</strong> is included with a script tag. Many HTML documents can include this file, so code that will be reused can be placed here.</li>
        <li>A function called <strong>displayDate()</strong> is <strong>embedded</strong> in the <strong>&lt;head&gt;</strong> element and can be called anywhere in this page (and only this page.</li>
        <li>Some <strong>inline</strong> javascript prints out the current year, with a  <strong>&lt;noscript&gt;</strong> fallback.</li>
      </ul>
      <details class='qna'><summary><strong>Question:</strong> In the code below, what is printed when javascript is unavailable?</summary>Have a look for a reference to death and 70 years.</details>
      <div class='trev-tryit'>
<textarea rows=15 onblur='tryItBuffer(this.value)'><!doctype html>
<html>
  <head>
    <script type="text/javascript" src="tools.js"></script>
    <script type="text/javascript">
     function displayDate() {
       document.getElementById("date").value=Date();
     }
   </script>
 </head>
 <body>
   <h1>What's the Date?</h1>
   <p><input type="text" id="date" name="date" value="Click the button" />
   <button onclick="displayDate()">Display Date</button></p>
   <p>&amp;copy; Copyright Date Awareness Inc 1990 - 
     <script type="text/javascript">
       var d = new Date();
       document.write(d.getFullYear());
     </script>
     <noscript>Date of my death, plus 70 years ...</noscript>
   </p>
   <h6>Ask us about the benefits of dynamic dating</h6>
  </body>
</html></textarea><br><input type='button' value='Try it'/> 
    </section>
  </section>  
  
  <section class='key' id='tp-jselements'>
    <header>Javascript Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Statement Blocks</h1>
      <p>A block of statements starts and ends with curly braces <code>{ ... }</code>. Statement blocks can be nested inside other statement blocks</p>
      <pre><code>if(isHappy == true) 
{
  <span>alert("<span>I’m happy!</span>");
  var answer = confirm("<span>Are you happy?</span>");
  if (<span>answer == true</span>) 
    document.write("<span>You are happy too.</span>");</span>
}</code></pre>
      <p>Many languages use the curly braces, although <strong>Python</strong> is one of a few languages that relies on indentation to set statement blocks.</p>
    </section>
  </section>
    
  <section>
    <header>Javascript Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Comments</h1>
      <p>To put in comments, javascript allows short comments that occur on one line or at the end of a line. These are denoted with <code>//</code>:</p>
      <pre><code>// <span>This is a comment on one line or the end of a line</span></code></pre>
      <p>Or block comments that begin with <code>/*</code> and end with <code>*/</code>:</p>
      <pre><code>/* <span>This is a comment 
   on more than
   one line</span> */</code></pre>
    </section>
  </section>
    
  <section>
    <header>Javascript Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Identifiers</h1>
      <p>Identifiers are the names of your variables, functions, arrays and objects. They must start with a letter or underscore and can contain letters and digits 0–9. Identifiers should be meaningful.</p>
      <p>These are bad:</p>
      <pre><code>var <span>a1</span> = 5; 
var <span>array4Something</span> = ['Morning','Afternoon','Evening'] 
function <span>x3</span>() { ... } 
</code></pre>
      <p>These are good:</p>
      <pre><code>var <span>numAttempts</span> = 5; 
var <span>daySegments</span> = ['Morning','Afternoon','Evening'] 
function <span>evaluateAttempt</span>() { ... } </code></pre>
      <p>Identifiers are unique within a page. Make sure that you don't do this, unless you like infinite loops and fast spinning computer fans:</p>
      <pre><code>for (var <span>i</span>=10; <span>i</span>&lt;12; <span>i</span>++) {
  for (var <span>i</span>=0; <span>i</span>&lt;2; <span>i</span>++) {
    console.log(<span>i</span>); 
  }
  console.log(<span>i</span>);
}</code></pre>
    </section>
  </section>

  <section>
    <header>Javascript Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Data Types</h1>
      <p>Javascript is a weakly typed language. When declaring a new variable, it will pick what type it thinks best. You should use the keyword <code>var</code> when creating a new variable.</p>
      <dl>
        <dt>Variable Types:</dt>
        <dd><strong>Number</strong> integer, float, etc</dd>
        <dd><strong>String</strong> a group of 0, 1 or more text characters</dd>
        <dd><strong>Boolean</strong> true or false</dd>
        <dd><strong>Object</strong> an array, object or unfortunately <strong>null</strong></dd>
        <dd><strong>Function</strong> a variable can refer to a function, that is a block of code</dd>
        <dd><strong>Undefined</strong> if it doesn't exist or is not set</dd>
      </dl>
      <pre><code>var aNumber = <span>5</span>;
var aString = <span>'5'</span>;
var anotherString = <span>"5"</span>;
var aBoolean = <span>true</span>;
var anObject = <span>[1,2,3,4,5]</span>;
var aFunction = <span>function sayHello(){ ... }</span>;
var aStudentObject = <span>{id:'<?php echo SID; ?>', type:'Ok I guess', <span>...</span> };</span>
</code></pre>
      <p><i>We will look at objects and arrays in more detail next week.</i></p>
    </section>
  </section>

  <section>
    <header>Javascript Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>User Functions</h1>
      <p>Javascript has many inbuilt functions, and developers can write their own functions too.</p>
      <p>Functions contain a sub-program or sub-routine that will not run until called. Functions can be called by other functions and by <strong>event handlers</strong>, such as when a page loads or when a user generated an event by interacting with the page. <i>In an earlier slide, we saw a canvas object track a users mouse position and place virtual paint onto itself.</i></p>
      <p>You can pass optional parameters, or an event object that is generated by the page element that called the function.</p>
      <p><strong>Good programming tip: </strong>A function should return something useful (eg a value or sting) or it should return a boolean (ie true or false) to let the caller know that the function executed successfully or failed.</p>
      <pre><code>function suitableFunctionName ( <span>optional parameters</span> ) {
  <span>// code goes here
  // last line: return something, true or false</span>
}
</code></pre>
    </section>
  </section>

  <section>
    <header>Javascript Elements<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Development: Logging and Alerting</h1>
      <p><img class='fr' src='app/img/console-inspect.png' alt='context menu, inspect developer area'> Unlike Java and C, if there are any "compiler" errors, a user is not alerted in an obvious way; javascript will just stop running.</p>
      <p>Modern day browsers have a developer area (Safari: you must enable <em>developer mode</em> in preferences) that can be accessed when right clicking in the browser and selecting "inspect" or "inspect element". Errors can be seen in the Console area.</p>
      <p onclick="console.log('A message and/or a variable value');">To print an error to the console <strong>without halting the code execution</strong>, call the <code>console.log()</code> function. For example, if you click on this paragraph, a message will be logged to the console.</p>
      <p onclick="alert('A message and/or a variable value');">Sometimes you will want debug whilst <strong>halting the code execution</strong>. You can do this by calling the <code>alert()</code> function. For example, if you click on this paragraph, an alert box will pop up.</p>
    </section>
  </section>    
    
    
  <section class='key' id='tp-jsoperates'>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Math Operators</h1>
      <p>Math operators allow you to perform arithmetic operations on variables that are a type of number:</p>
      <dl>
        <dt>Basic Maths:</dt>
        <dd><code>+</code> add / plus</dd>
        <dd><code>-</code> subtract / minus</dd>
        <dd><code>*</code> multiply / times</dd>
        <dd><code>/</code> divide / ... share?</dd>
        <dd><code>%</code> modulus / remainder</dd>
      </dl>
      <pre><code>var what_is = 19 <span>{operator}</span> 5;</code></pre>
      <div class='iqb'>What is 95? [ <span class='r' id='mathquiz1'></span> ]<br>
        <select oninput='$("#mathquiz1").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect.'>19 + 5</option>
          <option value='Incorrect.'>19 - 5</option>
          <option value='Correct'>19 * 5</option>
          <option value='Incorrect'>19 / 5</option>
          <option value='Incorrect'>19 % 5</option>
        </select>
      </div>
      <div class='iqb'>What is 24? [ <span class='r' id='mathquiz2'></span> ]<br>
        <select oninput='$("#mathquiz2").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Correct.'>19 + 5</option>
          <option value='Incorrect.'>19 - 5</option>
          <option value='Incorrect'>19 * 5</option>
          <option value='Incorrect'>19 / 5</option>
          <option value='Incorrect'>19 % 5</option>
        </select>
      </div>
      <div class='iqb'>What is 14? [ <span class='r' id='mathquiz3'></span> ]<br>
        <select oninput='$("#mathquiz3").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect.'>19 + 5</option>
          <option value='Correct.'>19 - 5</option>
          <option value='Incorrect'>19 * 5</option>
          <option value='Incorrect'>19 / 5</option>
          <option value='Incorrect'>19 % 5</option>
        </select>
      </div>
      <div class='iqb'>What is 4? [ <span class='r' id='mathquiz4'></span> ]<br>
        <select oninput='$("#mathquiz4").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect.'>19 + 5</option>
          <option value='Incorrect.'>19 - 5</option>
          <option value='Incorrect'>19 * 5</option>
          <option value='Incorrect'>19 / 5</option>
          <option value='Correct'>19 % 5</option>
        </select>
      </div>
      <div class='iqb'>What is 3.8? [ <span class='r' id='mathquiz5'></span> ]<br>
        <select oninput='$("#mathquiz5").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect.'>19 + 5</option>
          <option value='Incorrect.'>19 - 5</option>
          <option value='Incorrect'>19 * 5</option>
          <option value='Correct'>19 / 5</option>
          <option value='Incorrect'>19 % 5</option>
        </select>
      </div>
      <p>Most languages also have an exponent operator, but javascript does not. Instead it has a <strong>Math</strong> library that allows you to perform more complex mathematical operations, for example:</p>
      <pre><code>var tenCubed = Math.<span>pow</span>(10,3);</code></pre>      
    </section>
  </section>

  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Grouping Operations into a Block</h1>
      <p>For those unaware of <?php a('https://www.mathsisfun.com/operation-order-pemdas.html','PEMDAS')?>, the order of operations is not always <strong>left to right</strong> and multiple operations in one statement can produce an unwanted result. For example:</p>
      <pre><code>$whatIs105 = 7 + <span>3 * 10</span> + 5;</code></pre>
      <p>will produce <code>42</code> and not <code>105</code>. This is because the order of operations performs the later <code>*</code> operation before the earlier <code>+</code> operation.</p>
      <p>To ensure that the operations are done in our desired <strong>left to right</strong> order, we must use <strong>parentheses</strong> (ie the 'P' in PEMDAS) to wrap the section we want evaluated first. Operations contained inside parenthesis have <strong>precedence</strong> over operations outside parenthesis.</p>
      <pre><code>$whatIs105 = <span>( 7 + 3 )</span> * 10 + 5;</code></pre>
      <p>For more information on the numbers <strong>42</strong> and <strong>105</strong>, please visit <?php a('https://en.wikipedia.org/wiki/42_(number)'); ?> and <?php a('https://en.wikipedia.org/wiki/105_(number)'); ?></p>
    </section>
  </section>      
    
  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Math Shortcut Operators</h1>
      <p>Most programming languages support <strong>shortcut</strong> operators when the variable is part of the operation. For example:</p>
      <p><code>what_is <span>=</span> what_is <span>+</span> 5;</code> is the same as <code>what_is <span>+=</span> 5;</code></p>
      <p>And the same pattern works for <code><span>-</span>=</code>, <code><span>*</span>=</code>, <code><span>/</span>=</code> and <code><span>%</span>=</code>.</p> 
      <p>When adding or subtracting 1 from a variable:</p>
      <p><code>what_is <span>=</span> what_is <span>+</span> 1;</code> is the same as <code>what_is<span>++</span>;</code> <strong>(incrementing)</strong></p>
      <p><code>what_is <span>=</span> what_is <span>-</span> 1;</code> is the same as <code>what_is<span>--</span>;</code> <strong>(decrementing)</strong></p>
    </section>
  </section>      
    
  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Post &amp; Pre Increment</h1>
      <p>There is a subtle difference between <code><span>+</span>=</code> and <code>=<span>+</span></code>, and <code><span>++</span>what_is</code> and <code>whatis<span>++</span></code>. When assigning or printing a variable in a statement block or a loop (we will look at loops soon), <strong>when</strong> the variable is updated is different. For example:</p>
      <pre><code>what_is = <span>0</span>;
while(what_is &lt; <span>5</span>)
  alert(what_is<span>++</span>);
  
what_is = <span>0</span>;
while(what_is &lt; <span>5</span>)
  alert(<span>++</span>what_is);</code></pre>
      <button onclick='what_is=0; while(what_is < 5) alert(what_is++);'>Test what_is++ loop</button> <button onclick='what_is=0; while(what_is < 5) alert(++what_is);'>Test ++what_is loop</button>
      <p>As you can see, when you alert:</p>
      <ul>
        <li><code>whatis<span>++</span></code>: <code>what_is</code> is shown first and <strong>incremented</strong> by 1 afterwards (post-increment),</li>
        <li><code><span>++</span>whatis</code>: <code>what_is</code> is <strong>incremented</strong> by 1 first and shown afterwards (pre-increment).</li>
      </ul>
    </section>
  </section>

  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>String Operator(s)</h1>
      <p>Javascript allows you to join strings together using the string concatenation operator <code>+</code>:</p>
      <pre><code>var $helloWorld = 'Hello' <span>+</span> ' ' <span>+</span> 'World!'</code></pre>
      <p>There are many string properties and functions built into javascript string objects which can be accessed using the <code>.</code> character. Here are a few examples:</p>
      <dl>
        <dt>Explore:</dt>
        <dd><code>aString.<span>length</span></code> returns length of the string</dd>
        <dd><code>aString.<span>charAt</span>(posn)</code> returns the character found at a particular position</dd>
        <dd><code>aString.<span>indexOf</span>(subString)</code> returns position of subString within aString (or -1 if not a substring)</dd>
        <dd><code>aString.<span>includes</span>(subString)</code> similar to indexOf(), only a boolean true/false is returned</dd>
        <dt>Manipulate:</dt>
        <dd><code>aString.<span>trim</span>()</code> returns a string with whitespace removed from both ends of the string</dd>
        <dd><code>aString.<span>toUpperCase</span>()</code> returns an uppercase version of the string</dd>
        <dt>Disect:</dt>
        <dd><code>aString.<span>substr</span>(start, num)</code> returns a substring from specified position, extracting num characters</dd>
        <dd><code>aString.<span>split</span>(delimiter)</code> returns an array of strings split on a particular delimiter (ie character or substring)</dd>
      </dl>    
      <p>There is even a <code><span>concat</span>()</code> function if your keyboard doesn't have any working <code>+</code> keys on it:</p>
      <pre><code>$helloWorld = 'Hello'.<span>concat</span>(' ').<span>concat</span>('World!')</code></pre>
    </section>
  </section>

  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Logical Operators</h1>
      <p>Javascript has three logical operators:</p>
      <ul>
        <li><code>apples <span>&amp;&amp;</span> oranges</code> or <code>apples <span>and</span> oranges</code><br><strong>true</strong> is returned if both are true, else <strong>false</strong> is returned.</li>
        <li><code>apples <span>||</span> oranges</code> or <code>apples <span>or</span> oranges</code><br><strong>true</strong> is returned if either is true, else <strong>false</strong> is returned.</li>
        <li><code>!apples</code><br><strong>true</strong> is returned if <strong>apples</strong> is false, else <strong>false</strong> is returned.</li>
      </ul>
      <div class='iqb'>Evaluate <code>!(!apples or !oranges)</code> in another way [ <span class='r' id='demorganquiz1'></span> ]<br>
        <select oninput='$("#demorganquiz1").html(this.value)'>
          <option value=''>Please select</option>
          <option value='Incorrect.'>!apples and !oranges</option>
          <option value='Incorrect.'>!(apples or oranges)</option>
          <option value='Depends on the value of pineapple'>pineapple?</option>
          <option value='I think you missed the "in another way" bit'>!(!apples and !oranges)</option>
          <option value='Correct. Simplify your logic whenever possible'>apples and oranges</option>
        </select>
      </div>
    </section>
  </section>
  
  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Comparison Operators</h1>
      <p>To test for equality or inequality of values:</p>
      <ul>
        <li><code>5 <span>==</span> 5</code></li>
        <li><code>5 <span>!=</span> 4</code></li>
        <li><code>5 <span>==</span> "5"</code></li>
      </ul>
      <p>To test for equality or inequality of values AND type:</p>
      <ul>
        <li><code>5 <span>===</span> 5</code></li>
        <li><code>5 <span>!==</span> "5"</code></li>
      </ul>
      <p>And of course 
        <code><span>&lt;</span></code> (less than),
        <code><span>&lt;=</span></code> (less than or equal),
        <code><span>&gt;</span></code> (greater than),
        <code><span>&gt;=</span></code> (greater than or equal)
    </section>
  </section>

<section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Bitwise Operators</h1>
      <p>For specialist applications such as those involving <strong>flag checks</strong>, you can operate directly on the bits of <strong>32 bit number</strong> variables. A 32 bit number is returned.</p>
      <ul>
        <li><code>apples <span>&amp;</span> oranges</code><br>Compare each bit from both variables, return a <strong>1</strong> if both bits are 1, <strong>0</strong> otherwise.</li>
        <li><code>apples <span>|</span> oranges</code><br>Compare each bit from both variables, return a <strong>1</strong> if either bit is 1, <strong>0</strong> otherwise.</li>
        <li><code>apples <span>^</span> oranges</code><br>Compare each bit from both variables, return a <strong>1</strong> if bits are different, <strong>0</strong> if they are the same.</li>
        <li><code><span>~</span> apples</code><br>Flip each bit, ie return a <strong>1</strong> if the bit is 0 and <strong>0</strong> if the bit is 1.</li>
        <li><code>apples <span>&lt;&lt;</span> n</code><br>Move the bits to the <strong>left</strong> <strong>n</strong> times and return a number that is larger (value is doubled with each shift).</li>
        <li><code>apples <span>&gt;&gt;</span> n</code><br>Move the bits to the <strong>right</strong> <strong>n</strong> times and return a number that is smaller (value is halved with no remainder with each shift).</li>
      </ul>
      <p>Here is an example, only the last 8 bits are shown:</p>
      <pre><code>     105 <span>^</span> 42      <span><span> // is the same as</span></span>
01101001 <span>^</span> 00101010 <span><span>// evaluates to</span></span>
     01000011       <span><span>// is the same as</span></span>
        67          <span><span>// returned answer :-)</span></span></code></pre>
      <blockquote>This is an advanced topic and won't be covered in detail in this course.<br>You won't need these in an eCommerce website for example!</blockquote>
    </section>
  </section>
    
  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Ternary Operator</h1>
      <p>Many languages offer a <strong>ternary</strong> operator that will return one of two values depending on the outcome of a test.</p>
      <pre><code>test <span>?</span> true <span>:</span> false </code></pre>
      <p>For example:</p>
      <pre><code>var whenDoes = ( 1==2 <span>?</span> 'always' <span>:</span> 'never' )</code></pre>
      <p>This is a mini <code>if - (then) - else</code> statement. The above operator can we re-written as:</p>
      <pre><code>var whenDoes;
  if ( 1==2 ) { <span>// '?' part</span>
    whenDoes='always';
  } else { <span>   // ':' part</span>
    whenDoes='never';  
  }</code></pre>
    </section>
  </section>

  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>The typeof operator</h1>
      <p>Javascript provides a <code><span>typeof</span></code> operator that checks a variable's type and returns a <strong>string</strong> describing its type. Often knowing a variable's type is required to branch code execution, particularly recursive and "tree walking" code.</p>
      <pre><code>var whatTypeAreYou = <span>typeof</span> aVariable;</code></pre>
      <p>What will be returned when testing the following variables?</p>
      <details class='qna'><summary><code>typeof <span>""</span>;</code></summary>'string'</details>
      <details class='qna'><summary><code>typeof <span>5</span>;</code></summary>'number'</details>
      <details class='qna'><summary><code>typeof <span>5.0</span>;</code></summary>'number' (again). Javascript does not differentiate a float from an integer</details>
      <details class='qna'><summary><code>typeof <span>function () {} </span>;</code></summary>'function'. A variable can reference a function!</details>
      <details class='qna'><summary><code>typeof <span>wtv</span>;</code></summary>'undefined' as the variable has not been created.</details>
      <p>We will now create the variable <strong>wtv</strong>: <code>var <span>wtv</span>;</code></p>
      <details class='qna'><summary><code>typeof <span>wtv</span>;</code></summary>'undefined' as the variable has not been set.</details>
      <p>We will now set the variable <strong>wtv</strong>: <code><span>wtv</span> = null;</code></p>
      <details class='qna'><summary><code>typeof <span>wtv</span>;</code></summary>'object', which many believe is a mistake or fault in the standard.</details>
      <details class='qna'><summary><code>typeof <span>[3, 1, 4]</span>;</code></summary>'object', as any array is considered to be an object.</details>
      <details class='qna'><summary><code>typeof typeof <span>[3, 1, 4]</span>;</code></summary>'string', as the string 'object' is returned by the first <strong>typeof</strong> operator and passed to the second <strong>typeof</strong> operator.</details>
    </section>
  </section>

  <section>
    <header>Operators<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Type Conversion Functions</h1>
      <p>At some point, you will need to convert a variable from one type to another, for example:</p>
      <pre><code>"5" <span>+</span> "3" <span>=</span> "53"; <span><span>// not 8</span></span></code></pre>
      <p>You can use the following built-in javascript functions:</p>
      <pre><code>var aNumber = <span>Number</span>(aString);
var aString = <span>String</span>(aNumber);
var aFloat  = <span>parseFloat</span>(anInt);
var anInt   = <span>parseInt</span>(aFloat);</code></pre>
      <p>To get 8:</p>
      <pre><code>var eight = <span>Number</span>("5") + <span>Number</span>("3");</code></pre>
    </section>
  </section>    

  <section class='title'>
    <header>Lecture 6.2<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 2: Structures, Objects &amp; Events</p>
    </section>
  </section>    
    
   <section class='key'>
    <header>Structures, Objects &amp; Events<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-jsctrlstrc'>Control Structures</a></li>
        <li><a href='#tp-jshandevnt'>Handling Events</a></li>
        <li><a href='#tp-jsbuiltin'>Built In Objects</a></li>
        <li><a href='#tp-jsdom'>Document Object Model (DOM)</a></li>
        <li><a href='#tp-jsformval'>Form Validation</a></li>
      </ol>
    </section>
  </section>

 <section  class='key' id='tp-jsctrlstr'>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Controlling Program Flow</h1>
      <p>Control structures help you control the flow of your program. In addition, we have structures which allow us to repeat actions. These are called loops.</p>
      <dl>
        <dt><code>if - then - else</code></dt>
        <dd>This block allows us to make a decision and the else is optional.</dd>
        <dt><code>switch - case - break</code></dt>
        <dd>Similar to if - then - else block, more efficient when checking a single variable for many different values.</dd>
        <dt><code>while</code> and <code>do - while</code></dt>
        <dd>A simple loop that performs a condition check and allows the block to repeat</dd>
        <dt><code>for</code> and <code>for - in</code></dt>
        <dd>Similar to while loop, only incrementing is built in to the condition check</dd>
      </dl>
    </section>
  </section>

  <section>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Simple If Then Else</h1>
      <p>This block can be simple, with an optional <strong>else</strong> clause:</p>
      <pre><code>if ( <span>condition check</span> ) {
  <span><span>// code to execute if condition evaluates to <strong>true</strong></span></span>
} </code></pre>
      <p>and</p>
      <pre><code>if ( <span>condition check</span> ) {
  <span><span>// code to execute if condition is <strong>true</strong></span></span>
} else {
  <span><span>// code to execute if condition is <strong>false</strong></span></span>
} </code></pre>
    </section>
  </section>

  <section>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Nested If Then Else</h1>
      <p>Blocks can be nested inside each other when performing more than one check:</p>
      <pre><code>if ( <span>first condition check</span> ) {
  <span><span>// code to execute if <strong>first</strong> condition is <strong>true</strong></span></span>
} else if ( <span>second condition check</span> ) {
  <span><span>// code to execute if <strong>only second</strong> condition is <strong>true</strong></span></span>
} else { 
  <span><span>// code to execute if <strong>both</strong> conditions are <strong>false</strong></span></span>
}</code></pre>
      <p>and</p>
      <pre><code>if ( <span>first condition check</span> ) {
  if ( <span>second condition check</span> ) {
    <span><span>// code to execute if <strong>both</strong> conditions are <strong>true</strong></span></span>
  } else  {
  <span><span>// code to execute if <strong>only first</strong> condition is <strong>true</strong></span></span>
  }
} else { 
    <span><span>// code to execute if <strong>first</strong> condition is <strong>false</strong></span></span>
}</code></pre>
      <p>When performing more than two checks, code can become complex and it is very easy to mis-nest the blocks and get an incorrect outcome.</p>
    </section>
  </section>

  <section>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Switch Case Block</h1>
      <p>This block is suitable when testing a variable for multiple values. Each mini "case-block" must be terminated with a <strong>break</strong> statement or it will run into the next case. An optional <strong>default</strong> option is to be run if no value is listed in the block:</p>
      <pre><code>switch ( <span>aVariable</span> ) {
  case <span>value 1</span>:
    <span><span>// code to execute a variable has value #1</span></span>
    break; 
  case <span>value 2</span>:
    <span><span>// code to execute a variable has value #2</span></span>
    break; 
  ...
  case <span>value 105</span>:
    <span><span>// code to execute a variable has value #105</span></span>
    break; 
  default:
    <span><span>// code to execute if value not listed</span></span>
}</code></pre>
    </section>
  </section>

  <section>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Run-on Switch Case Block</h1>
      <p>If the same code is to be executed for a set of values, the break statement can be ommitted to allow "run-on" execution.</p>
      <pre><code>switch ( <span>dayName</span> ) {
  case <span>"Monday"</span>:
  case <span>"Tuesday"</span>:
  case <span>"Wednesday"</span>:
  case <span>"Thursday"</span>:
  case <span>"Friday"</span>:
    <span><span>dayType = "Weekday";</span></span>
    break; 
  case <span>"Saturday"</span>:
  case <span>"Sunday"</span>:
    <span><span>dayType = "Weekend";</span></span>
    break; 
  default:
    <span><span>dayType = "Invalid";</span></span>
}</code></pre>
      <p>Select Day:
        <select onchange='whichDay(this)'>
          <option value=''><i>Please Select</i></option>
          <option value='Monday'>Monday</option>
          <option value='Tuesday'>Tuesday</option>
          <option value='Wednesday'>Wednesday</option>
          <option value='Thursday'>Thursday</option>
          <option value='Friday'>Friday</option>
          <option value='Saturday'>Saturday</option>
          <option value='Sunday'>Sunday</option>
          <option value='Funday'>Funday</option>
        </select><span class='error' id='thisDayL6'></span></p>
      <p>These blocks can be nested when checking multiple values, but code can become very complex very quickly.</p>
    </section>
  </section>

  <section>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>While and Do - While Loops</h1>
      <p>A while loop will perform a check and run code if the check is returned true. The loop should be designed so the condition being checked changes in the loop. For example:</p>
      <pre><code>var sum1to99 = 0;
var count = 1;      
var stopCount = 100;      
while ( <span>count &lt; stopCount</span> ) {
  sum1to99 += count;
  count++;
}</code></pre>
      <p>will add up all of the numbers from 1 to 99 and stop when count is incremented past 99.</p>
      <p>A do - while loop is very similar except that the check is performed at the end of each loop, not the start. That is, you are guarenteed to execute the code in the loop at least once:</p>
      <pre><code>var sum1to99 = 0;
var count = 1;      
var stopCount = 100;      
<span>do</span> {
  sum1to99 += count;
  count++;
} while ( <span>count &lt; stopCount</span> )</code></pre>
    </section>
  </section>

  <section>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>For Loops</h1>
      <p>For loops combine an incrementer and a condition check into one area:</p>
      <pre><code>var stopCount = 100;
for ( var count = 1; <span>count &lt; stopCount</span>; count++ ) {
  sum1to99 += count;
}</code></pre>
      <p>The incrementor can go up or down, or even increment in an exotic way. For example, to compute the natural note frequencies in an octave:</p>
      <pre><code>var oA = 440;
for ( var <span>freq = oA</span>; <span>freq &lt; oA*2</span>; <span>freq *= Math.pow(2,1/12)</span> ) {
  document.write(freq+'&lt;br&gt;');
}</code></pre>
      <p><i>No ... this second example won't be on the exam!</i></p>
      
    </section>
  </section>

  <section>
    <header>Control Structures<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>For Loops</h1>
      <p>For loops are particularly useful when processing each element inside an indexed array, for example when building up a response string to print out day names and what type of day it is:</p>
      <pre><code>var weekDays = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Funday','Sunday'];
var <span>dayMessage</span> = <span>''</span>;

for ( var dayNum = 0; dayNum &lt; weekDays.length; dayNum++ ) {
  <span>dayMessage</span> = weekDays[dayNum] + <span>' is a '</span>;
  switch ( weekDays[dayNum] ) {
    case "Monday":
      <span>dayMessage</span> += <span>'hateful '</span>;
    case "Tuesday":
    case "Wednesday":
    case "Thursday":
    case "Friday":
      <span>dayMessage</span> += <span>'week day'</span>;
      break; 
    case "Saturday":
    case "Sunday":
      <span>dayMessage</span> += <span>'weekend day'</span>;
      break; 
    default:
      <span>dayMessage</span> += <span>'... what day is that?'</span>;
  }
  document.write(<span>dayMessage</span> + '&lt;br&gt;');
}</code></pre>
      <p><i>We will look at the for - in loop next week when we cover arrays.</i></p>
    </section>
  </section>    
    
  <section class='key' id='tp-jshandevnt'>
    <header>Handling Events<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Events and Event Handlers</h1>
      <p>Events are actions that occur in a web browser (or other user agent). By convention, event handler attributes begin with the word <code>on</code> (see below).</p>
      <p>Practically every element in a webpage can respond to an event, although <code>load</code> and <code>unload</code> events are restricted to <strong>&lt;body&gt;</strong>, <strong>&lt;img&gt;</strong> and multimedia elements.</p>
      <pre><code>&lt;body <strong>on</strong>load='<span>/* code to run when page loads */</span>'&gt;</code></pre>
      <p>The most common events to implement are to detect and respond to user interaction with the mouse and keyboard.</p>
      <p onclick='alert("Mouse click detected");'>For example, to attach a <strong>mouse click</strong> event handler to this paragraph, add a new attribute called <code>onclick</code> and write a short code snippet inside, eg</p>
      <pre><code>&lt;p <strong>on</strong>click='<span>alert("Mouse click detected");</span>'&gt;
  For example ... snippet inside, eg
&lt;/p&gt;</code></pre>
    </section>
  </section>  

  <section>
    <header>Handling Events<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Objects: this and event</h1>
      <p>Javascript will recognise two words "by default" as references to objects involved with the generation of an event.</p>
      <ul>
        <li><code>this</code>: a reference to the originating element, ie from where the event was generated.</li>
        <li><code>event</code>: a reference to a more comprehensive event object created when the event was generated.</li>
      </ul>
      <p id='thisevent' onclick='alert(this.id+": "+this.innerHTML); console.log(event);'>For example, when you click on this paragraph which has the id "thisevent", information about <strong>this paragraph</strong> will be presented in the alert box and information about the <strong>mouse event</strong> will be logged to the console.</p>
      <pre><code>&lt;p id='<span>thisevent</span>' <strong>on</strong>click='<span>alert(this.id+": "+this.innerHTML); console.log(event);</span>'&gt;
  For example ... snippet inside, eg
&lt;/p&gt;</code></pre>
      <p>The event object generated by an event contains a great deal of information including:</p>
      <ul>
        <li><code>event.<span>type</span></code>: What type of event has been generated.</li>
        <li><code>event.<span>target</span></code>: The originating element (equivalent to <code>this</code> above).</li>
        <li><code>event.<span>currentTarget</span></code>: The calling element</li>
        <li><code>event.<span>clientX</span></code>, <code>event.<span>screenY</span></code>, <code>event.<span>pageX</span></code>, <code>event.<span>pageY</span></code>The various locations of a click event, ie relative to the window, screen and page.</li>
        <li><code>event.target.<span>innerHTML</span></code>: The <strong>content</strong> of a paired item (eg a paragraph or list item).</li>
        <li><code>event.target.<span>value</span></code>: the value attribute of an element, ie most form elements.</li>
      </ul>
    </section>
  </section>  

  <section>
    <header>Handling Events<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Event Handling Functions</h1>
      <p id='inspectEventDemo' onclick='inspectEvent(this,event);'>To make HTML code more managable, you can set the event handler to call a function instead. For example, if you click on this paragraph, a function called <code>inspectEvent()</code> will run and log the event information to the console:</p>
      <pre><code>&lt;p id='inspectEventDemo' <strong>on</strong>click='<span>inspectEvent();</span>'&gt;To make ... to the console:&lt;/p&gt;</code></pre>
      <pre><code>function inspectEvent() { 
  alert('Event of type "'+'<span>event.type</span>'+'" detected:\n- '
  +<span>event.currentTarget.id</span>+' (originating element)\n- '
  +<span>event.target.id</span>+' (calling element)\nView the console for more information');
  console.log(<span>event</span>);
}</code></pre>
      <p><i>NB: Whilst <code>this</code> is a reserved word and cannot be accidentally declared as a variable, <code>event</code> is a variable that can be over-written. That said, javascript will treat the word <code>event</code> as a "de-facto" reserved word, so you can refer to the generated event inside a function "auto-magically" as we can see in the example above.</i></p>
    </section>
  </section>      
    
  <section>
    <header>Handling Events<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Mouse Events</h1>
      <p>The following list items will generate events that log information to the console. Each list item calls the <strong>inspectEvent()</strong> function we saw earlier.</p>
      <ul>
        <li onclick='inspectEvent(event);'><code>click</code>: A simple primary mouse button detection event.</li>
        <li ondblclick='inspectEvent(event);'><code>dblclick</code>: As above, but a double click detection event.</li>
        <li onmousedown='inspectEvent(event);'><code>mousedown</code>: When any mouse button is pressed and which mouse button can be determined from a passed event.</li>
        <li onmouseup='inspectEvent(event);'><code>mouseup</code>: When a mouse button is released. </li>
        <li onmouseover='inspectEvent(event);'><code>mouseover</code>: When the pointer enters an element. </li>
        <li onmouseout='inspectEvent(event);'><code>mouseout</code>: When the pointer leaves an element. </li>
        <li onmousemove='inspectEvent(event);'><code>mousemove</code>: When the pointer is moved, its position can be determined from a passed event. <i>You may wish to disable further alert boxes!</i></li>
      </ul>
    </section>
  </section>    
    
  <section>
    <header>Handling Events<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Keyboard and Related Events</h1>
      <p>Elements that accept user input, such as <strong>text boxes</strong>, <strong>textareas</strong> etc can handle keyboard and events below.</p>
      <ul>
        <li><input onkeypress='inspectEvent(event);'  placeholder='onkeypress' /> When a keyboard key is pressed, but not ALT, CMD, CTRL, ESC, FN, SHIFT, TAB keys etc).</li>
        <li><input onkeydown='inspectEvent(event);'  placeholder='onkeydown' /> As above, but any key; specifically when pressed down.</li>
        <li><input onkeyup='inspectEvent(event);'  placeholder='onkeyup' /> When any key is released.</li>
        <li><input onblur='inspectEvent(event);'  placeholder='onblur' /> When an element loses the focus.</li>
        <li><input onchange='inspectEvent(event);'  placeholder='onchange' /> When the field loses the focus <strong>and</strong> when the contents have been changed.</li>
        <li><input oninput='inspectEvent(event);'  placeholder='oninput' /> A new HTML5 event that is better designed and detects input events across a wider range of elements and devices (eg touch screens).</li>
        <li><input onselect='inspectEvent(event);'  placeholder='onselect' /> When a selection has been made inside the element <i>- look for selectionStart and selectionEnd inside target!</i></li>
        <li><input onfocus='inspectEvent(event);'  placeholder='onfocus' /> When an element gets the focus <i>- only one element can have the focus warning: this produces an infinite loop, test this one last!</i></li>
      </ul>
      <p>Other elements, such as <strong>select boxes</strong>, <strong>radio buttons</strong> and <strong>checkboxes</strong> don't respond to keyboard events, but do respond to many of the other events above.</p>
     </section>
  </section>    
    
  <section>
    <header>Handling Events<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Form Validation Event</h1>
      <p>When a user attempts to submit form data to a processing script, a <strong>submit</strong> event is generated and can be handled by a form's <strong>onsubmit</strong> handler.</p>
      <p>Any function that returns <strong>false</strong> will block submission of form data and not submit, saving the server the trouble of having to process invalid form data.</p>
      <pre><code>&lt;form action='...' method='...' <span>onsubmit='<strong>return</strong> formCheck()'</span>&gt;</code></pre>
      <p><i>The return keyword is very important and essential to block submission!</i></p>
      <p>We will look at an example of a form validation script shortly.</p>
   </section>
  </section> 

  <section class='key' id='tp-jsbuiltin'>
    <header>Built In Objects<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Introduction</h1>
      <p>To carry out more complex JavaScript actions, we can take advantage of javascript's built-in objects. We have already seen the <strong>Math</strong>, <strong>Number</strong> and <strong>Date</strong> objects and some of their methods:</p>
      <pre><code>if ( <span>isNaN(total)</span> ) 
  alert('That\'s not a number!');
else 
  alert('You owe $' + <span>total.toFixed(2)</span>);</code></pre>
      <pre><code>document.write('The time is ' + <span>Date.now()</span>);</code></pre>
      <p>We will look at the <strong>RegExp</strong> object and its methods in detail shortly.</p>
   </section>
  </section> 

  <section>
    <header>Built In Objects<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Browser Object Model (BOM)</h1>
      <p>Many properties of the user's browser is available to a developer. They are grouped under the <strong onclick='console.log(window);'>Window</strong> object:</p>
      <ul>
        <li onclick='console.log(document);'><strong>Document</strong>: contains references to all elements in the page. We will look at this object in great detail shortly.</li>
        <li onclick='console.log(history);'><strong>History</strong>: contains a list and count of the users visited urls.</li>
        <li onclick='console.log(navigator);'><strong>Navigator</strong>: contains information on the browser, such as vendor, version, platform, geolocation etc.</li>
        <li onclick='console.log(location);'><strong>Location</strong>: contains information about the server that is serving the current page such as name, protocol etc.</li>
        <li onclick='console.log(screen);'><strong>Screen</strong>: contains information about the user's screen such as size, orientation, color depth etc.</li>
      </ul>
      <p><i>Click on the above elements to log them to the console!</i></p>
   </section>
  </section> 

  <section>
    <header>Built In Objects<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Accessing BOM</h1>
      <p>BOM objects properties and methods are structured in a branching tree-like structure. That is, some methods and properties are located inside other properties. All can be accessed using standard object referencing notation or by treating the object as an associative array:</p>
      <pre><code>alert(<span>screen.width</span>);
alert(<span>screen['<span>width</span>']</span>);
alert(<span>screen.orientation.angle</span>);
alert(<span>screen['<span>orientation</span>']['<span>angle</span>']</span>);</code></pre>
      <p>Many of the properties can be modified, as this fun website demonstrates: <?php a('http://stewd.io/pong/'); ?>.</p>
      <pre><code>window.open();
<span>...</span>
window.moveTo( xPos, yPos );
<span>...</span>
window.close();</code></pre>
      <p>Whilst some programming is performed using jQuery (a compact version of javascript), windows are opened, moved in response to keyUp events (arrow keys) and closed when going off screen.</p>
   </section>
  </section> 

  <section class='key' id='tp-jsdom'>
    <header>Document Object<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Document Object Model (DOM)</h1>
      <p>The document object is a map of all the elements on your page. For example, the following returns a tree like structure of all the elements in the webpage <strong>&lt;body&gt;</strong> element:</p>
      <pre><code>console.log(<span>document.body</span>);</code></pre>
      <p>To access the <strong>&lt;main&gt;</strong> element, ie the 3rd element in the <strong>&lt;body&gt;</strong> element, you need to look at the <code>children</code> of body:</p>
      <pre><code>console.log(document.body.<span>children[2]</span>);</code></pre>
      <p>To access the 8th element inside the <strong>&lt;main&gt;</strong> (ie this lecture series on javascript), and its id, you need:</p>
      <pre><code>console.log(document.body.children[2].<span>children[7]</span>);
console.log(document.body.children[2].children[7].<span>id</span>);</code></pre>
      <p>Hopefully, you can appreciate that this is a very complicated and unmanagable way of referencing elements. Should the document ever change, the elements will be re-ordered and references will be broken.</p>
   </section>
  </section> 

  <section>
    <header>Document Object<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>getElementById() Function</h1>
      <p>The document object has a method to find an element with a particular id. You can reference the lecture 6 slides in the following way:</p>
      <pre><code>document.<span>getElementById('<span>slides-lecture-6</span>')</span>;</code></pre>
      <p><i>This is why it is important that elements never share the same id!</i></p>
      <p>In particular, you will be looking for form elements and their values in your assignments. By giving your form elements an <code>id</code> as well as a <code>name</code> attribute means that javascript can access the element. Let us assume that a form element with id <code>s1-qty</code> contains the quantity of season 1 boxsets:</p>
      <pre><code>var s1-qty = document.<span>getElementById('<span>s1-qty</span>').value</span>;</code></pre>
      <p>Let us assume that a span with id <code>s1-subTotal</code> is where we need to put the season 1 subtotal, formatted to 2 decimal places:</p>
      <pre><code>document.<span>getElementById('<span>s1-subTotal</span>').innerHTML</span> = (s1-qty * s1-price).toFixed(2);</code></pre>
   </section>
  </section> 

  <section>
    <header>Document Object<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Attributes: Name and Id</h1>
      <p>Form elements usually have a <code>name</code> and an <code>id</code> attribute.</p>
      <ul>
        <li><code>name</code>: For form submission. If there is no <strong>name</strong>, it will not be sent to the server in a form request.</li>
        <li><code>id</code>: For javascript manipulation. If there is no <strong>id</strong>, javascript will have trouble finding it.</li>
      </ul>
      <p>Usually the names and ids are the same in form elements:</p>
      <pre><code>&lt;input type='<span>...</span>' id='<span>fname</span>' name='<span>fname</span>' /&gt;</code></pre>
      <p>However, with radio buttons in the same group, names are not unique, so different ids must be used:</p>
      <pre><code>&lt;input type='<span>radio</span>' name='<span>gender</span>' id='<span>m-gender</span>' value='<span>male</span>' /&gt;
&lt;input type='<span>radio</span>' name='<span>gender</span>' id='<span>f-gender</span>' value='<span>female</span>' /&gt;
&lt;input type='<span>radio</span>' name='<span>gender</span>' id='<span>o-gender</span>' value='<span>other</span>' /&gt;</code></pre>
   </section>
  </section> 

  <section>
    <header>Document Object<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>getElementsByTagName() Function</h1>
      <p>The document object has a method to find all elements with a particular <strong>tag</strong> name. You can find all the image elements in a page in the following way and they will be returned as an indexed array:</p>
      <pre><code>var allImgs = document.<span>getElementsByTagName('<span>img</span>')</span>;</code></pre>
      <p><i>There are many examples of pages that pre-load all images with this script or something similar.</i></p>
    </section>  
  </section>

  <section>
    <header>Document Object<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>getElementsByClassName() Function</h1>
      <p>The document object has a method to find all elements with a particular <strong>class</strong> name. You can find all the elements of a particular class in a page in the following way and they will be returned as an indexed array:</p>
      <pre><code>var allErrors = document.<span>getElementsByClassName('<span>error</span>')</span>;</code></pre>
      <p><i>By selecting all elements on the page that have the class 'error', we can show or hide them.</i></p>
    </section>  
  </section>

  <section class='key' id='tp-jsformval'>
    <header>Form Validation<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Blocking Submission</h1>
      <p>When a user submits a form we’d like to make sure they have filled out fields correctly client side so that the server is not bothered by invalid requests.</p>
      <p>If the user has filled the form incorrectly, we should indicate the error to the user and specifying the correct format of the input.
HTML5’s new type and pattern attributes have made a lot of this redundant (now "built in" to the browser).</p>
      <p>For this example, we are going to create a registration form for the "Project Steve" website:</p>
      <p><?php a('http://www.skeptical-science.com/science/project-steve'); ?></p>
    </section>  
  </section>

  <section>
    <header>Form Validation<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Accessing Form Elements</h1>
      <p>Open and examine the following website as we step through the following slides:</p>
      <p><?php a('http://titan.csit.rmit.edu.au/~e54061/wp/ProjectSteve.html'); ?></p>
      <p>You will observe that the form has:</p>
      <ul>
        <li>A text field: <code>name='<span>name</span>'</code></li>
        <li>Radio buttons: <code>name='<span>scientist</span>'</code></li>
        <li>A PDF file: <code> name='<span>signDoc</span>'</code></li>
        <li>A submit button: <code> // <span>no name</span> </code></li>
      </ul>
      <p>In the form tag, you can see that the form:</p>
      <ul>
        <li>Submits data to <strong>processing.php</strong>: <code>action='<span>.../processing.php</span>'</code></li>
        <li>Uses the <strong>post</strong> method: <code>method='<span>post</span>'</code></li>
        <li>Runs javascript whenever an <strong>onsubmit</strong> event is generated: <code>onsubmit='<span>return formValidate();</span>'</code></li>
        <li>Places the server response in a new tab (or window): <code>target='<span>_blank</span>'</code></li>
      </ul>      
    </section>  
  </section>

  <section>
    <header>Form Validation<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Field and Sub-Functions in Example</h1>
      <p>There are a number of sub-fuctions present in this example:</p>
      <ul>
        <li><code>getid():</code> Developer is very lazy and wishes to avoid typing "document.getElementById()" all the time.</li>
        <li><code>clearErrors():</code> This function looks for all spans of class "error", makes them invisible and resets any form fields to a normal style.</li>
        <li><code>nameCheck():</code> Checks that the name is "Steve", <strong>or</strong> sets an error message and styles the invalid name field if not.</li>
        <li><code>scientistCheck():</code> Checks that the scientist button is  selected, <strong>or</strong> sets an error message and styles the invalid file field if not.</li>
        <li><code>fileNameCheck():</code> Checks that there is a filename and that it is has a .pdf extension, <strong>or</strong> sets an error message and styles the invalid file field if not.</li>
      </ul>
      <p>Note that each "field function" <code>nameCheck()</code>, <code>scientistCheck()</code>, <code>fileNameCheck()</code> returns true if the field contains valid user input or false if not. This helps the calling function to determine success or failure and perform a complete form validation.</p>
    </section>  
  </section>

  <section>
    <header>Form Validation<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Form Validation Function in Example</h1>
      <p>One "master" form validation function <code>formValidate()</code> first calls <code>clearErrors()</code> to reset any error messages and field styles, even if there aren't any, then calls each of the field  functions in turn.</p>
      <p>Each time a "field function" returns false, an error counter is incremented. If the error count is zero, submission of form data is allowed; however, if the error count is greater than zero, form submission to the server is blocked and the user is asked to correct their errors.</p>
      <p>Finnally, this function retirns <strong>true</strong> or <strong>false</strong> to the <strong>onsubmit</strong> event handler, which will either allow or block the form submission.</p>
    </section>  
  </section>

  <section>
    <header>Form Validation<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Final Notes and Advice</h1>
      <p>By breaking up a complex task into a master function and a number of sub functions, code can be kept neat and managable.</p>
      <p>Eventually, a true or false must "bubble up" to the form’s onsubmit event handler. This will either allow or block submission of the form (default is to allow).</p>
      <p>When programming, it is unlikely you will get everything right the first time, so you will need to debug your code!</p>
      <ul>
        <li><strong>alert(message)</strong> is good for halting the program and printing something you want to check (don’t use in large loops)</li>
        <li><strong>console.log(message)</strong> is good for logging more complex messages without halting the program.</li>
        <li>Having <strong>target="_blank"</strong> directs the output from the server to a new tab, so you can inspect both the form and the server's response page at the same time.</li>
      </ul>
      <p>When everything is working, remove these debugging aids so that users are not bothered with ugly alert boxes, or console.logs; and so that the server response replaces the content of the form page when submission is sucessful.</p>
    </section>  
  </section>
</article>