<?php
  $counter=1;
  $slideText='Lect 7, P.';
?>
<article id='slides-lecture-7'>

  <section class='title'>
    <header>Welcome!<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Web Programming
        <br>COSC2413/2426/2453</p>
      <p>Lecture 7</p>
    </section>
  </section>

  <section class='title'>
    <header>Lecture 7.1<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 1: Advanced Event Handling, Regular Expressions</p>
    </section>
  </section>

  <section class='key'>
    <header>Javascript - Client Side Programming<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-jseventhand'>Event Handling Redux</a></li>
        <li><a href='#tp-jsregex'>Regular Expressions</a></li>
      </ol>
    </section>
  </section>

  <section class='key' id='tp-jseventhand'>
    <header>Event Handling Redux<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Assigning Event Handlers</h1>
      <p>Previously, we saw that a JavaScript event handler can be set by adding an HTML attribute to an element.</p>
      <pre><code>&lt;input type='button' id='price' value='Calculate' <span>onclick='calculatePrice()'</span> /&gt;</code></pre>
      <p><input type='button' id='price' value='Calculate' onclick='calculatePrice()' /></p>
      <p>When the above button is clicked by a user, the function calculatePrice() is called.</p>
      <p>There are two additional ways for setting event handlers:</p>
      <ul>
        <li>DOM event handlers</li>
        <li>Event listeners</li>
      </ul>
    </section>
  </section>
  
  <section>
    <header>Event Handling Redux<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>DOM Event Handlers</h1>
      <p>DOM event handlers attach a function directly to an element:</p>
      <pre><code>&lt;input type='button' id='price2' value='Calculate 2' <span class='x'>onclick='calculatePrice()'</span> /&gt;</code></pre>
      <pre><code>var myButton = document.getElementById("price2");
myButton<span>.onclick</span> = <span>calculatePrice2</span>;
</code></pre>
      <p>This code assigns the function <code>calculatePrice2</code> to an element with the id <code>price2</code></p>
      <p><input type='button' id='price2' value='Calculate 2' /></p>
      <p>Notice that:
      <ul>
        <li>No parentheses are added to the function name.</li>
        <li>Only one function can be assigned to each event handler (ie "click") using this method.</li>
      </ul>
      <p>When assigning event handlers, the code should be placed <em>at the end</em> of the webpage, ie after the page has been loaded and the <strong>DOM</strong> has been constructed. For this reason, external javascript files that assign event handlers to HTML elements in these slides have been placed <em>after</em> the <strong>&lt;footer&gt;</strong>element rather than in the <strong>&lt;head&gt;</strong> element.</p>
      <p><img src="app/img/event-handler-placement.png" alt="Screenshot of console demonstrating js files being placed after the footer"/></p>
    </section>
  </section>

  <section>
    <header>Event Handling Redux<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Event Listeners</h1>
      <p>Event listeners are another way of adding event handling functionality to a web page. They are more flexible than DOM event handlers, and allow multiple event handlers of the same type to be added to an element (eg 2 click events).</p>
      <p>Simply call the <code>addEventListener()</code> method again on the same element to add another handling function.</p>
      <pre><code>&lt;input type='button' id='price3' value='Calculate' /&gt;</code></pre>
      <pre><code>var myButton = document.getElementById("price3");
myButton<span>.addEventListener</span>(<span>"click"</span>, <span>calculatePrice</span>); 
myButton<span>.addEventListener</span>(<span>"click"</span>, <span>calculatePrice2</span>); 
</code></pre>
      <p>This code assigns the functions <code>calculatePrice</code> AND <code>calculatePrice2</code> to an element with the id <code>price3</code></p>
      <p><input type='button' id='price3' value='Calculate 3' /></p>
      <p>Notice that:
      <ul>
        <li>The event name does not use the "on" prefix.</li>
        <li>No parentheses are added to the function name.</li>
        <li>Multiple functions can be assigned to each event handler (ie "click") using this method.</li>
      </ul>
    </section>
  </section>

  <section>
    <header>Event Handling Redux<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Event Propagation and Flow</h1>
      <p>HTML elements may be nested inside other elements, for example, a <code>span</code> inside a <code>div</code>.
      <pre><code>&lt;div <span>id='ef-div'</span>&gt;
  &lt;span <span>id='ef-span'</span>&gt;<span><span>Click me!</span></span>&lt;/span&gt;
&lt;/div&gt;
...
document.getElementById("ef-div").addEventListener("click", inspectEvent); 
document.getElementById("ef-span").addEventListener("click", inspectEvent);</code></pre>
      <div id='ef-div' class='pd'><span id='ef-span' class='pd'>Click Me!</span></div>
      <p>When a click is triggered on the span, this also triggers a click on the div. The order in which events are handled is called <strong>event flow</strong> and by default starts with the innermost element and propagates outwards.</p>
      <p><i>Note that the event <em>originating</em> element (<strong>event.target</strong>) is not always the same as the event <em>calling</em> element (<strong>this</strong> or <strong>currentTarget</strong>).</i></p>
      <ul>
        <li>Event <strong>bubbling</strong> (default) means that the event starts at the most specific (child) element node, and flows out towards the least specific.</li>
        <li>Event <strong>capturing</strong> means that the event starts at the least specific (parent) element.</li>
      </ul>
      <p>This direction can be changed by setting an optional <code>useCapture</code> parameter to <code>true</code>.</p>
      <pre><code>var mySpan = document.getElementById("ef-span");
mySpan.addEventListener("click", bubbleUp, <span>true</span>); 
</code></pre>
    </section>
  </section>

  <section>
    <header>Event Handling Redux<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Event Propagation Prevention</h1>
      <p>Event propagation is not always desired. Ideally you should avoid nesting elements that respond to events, but if this is not posible, you can call the passed event's <code>stopImmediatePropagation()</code> method inside the function.</p>
      <pre><code>function bubbleUp(<span>event</span>) {
  // function code goes here
  <span>event.stopImmediatePropagation()</span>;
}</code></pre>
      <p>Sometimes you will want to <strong>prevent default</strong> functionality, for example, making sure <strong>mousewheel</strong> events on the lecture 2 kitten image does not scroll the browser window. This can be achieved by calling the passed event's <code>preventDefault()</code> method at the end of the function.</p>
      <pre><code>document.getElementById("cuteKitten").addEventListener("<span>wheel</span>", function(<span>eP</span>) {
  document.getElementById("purr").play();
  <span>eP.preventDefault(); <span>// no window scrolling!</span></span>;
});</code></pre>
      <p><i>Notice here that the event object is being passed as a parameter and that the name chan be changed. This is fine, and many programmers choose not to rely on javascript's treatment of <code>event</code> as a de-facto reserved word.</i></p>
    </section>
  </section>  

  <section class='key' id='tp-jsregex'>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Introduction</h1>
      <p><strong><em>Re</em>gular <em>ex</em>pr<em>es</em>sions</strong> (regexes) allow us to write patterns (sequences of characters) that then be searched for in strings, allowing us to avoid complex string manipulation.</p>
      <p>In lecture 6, we used Javascript to perform a complex validation on two text fields</p>
      <ul>
        <li><strong>Name</strong> = Steve <i>{space} {something}</i></li>
        <li><strong>Filename</strong> = <i>{something} {dot}</i> pdf</li>
      </ul>
      <h3>Example</h3>
      <p>To match <strong>{start} Steve {space} {letters &amp; spaces} {end}</strong>, we use the following regular expression:</p>
      <p><code>^Steve [a-zA-Z ]+$</code></p>
      <p>To match <strong>{start} {letters &amp; numbers} {dot} pdf {end}</strong>, we use the following regular expression:</p>
      <p><code>^[a-zA-Z0-9]+\.pdf$</code></p>
    </section>
  </section>  

  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>HTML5 pattern attribute</h1>
      <p>In HTML5, a new <code>pattern</code> attribute was introduced for text based input elements. We can supply a regular expression pattern for the browser to check against. <i>Note that this pattern attribute only performs whole string checks, so <code>^</code> and <code>$</code> are not needed - more on this later!</i></p>
      <pre><code>&lt;input type='text' <span>pattern='Steve [a-zA-Z ]+'</span>&gt;</code></pre>
      <p>In addition, a new pseudo style <code>:invalid</code> can be set to style invalid inputs, for example just inputs with a specified pattern attribute.</p>
      <pre><code>input[<span>pattern</span>]<span>:invalid</span> {
  border:2px red solid;
  background-color:#fee;
}</code></pre>
      <h3>HTML5 Pattern Example</h3>
      <p>Full Name: <input type='text' pattern='Steve [a-zA-Z ]+' placeholder='Steve {Something}'></p>
      <details class='qna'><summary>What happens when the field is blank?</summary>No "invalid" state is detected. This may be a glitch as the pattern is not matched, but to fix this, just add a <strong>required</strong> attribute. The downside is that the field will always be red when blank, eg on first load.</details>
      <details class='qna'><summary>What happens when your name is <em>not</em> Steve?</summary>The field is red as the pattern is expecting a Steve.</details>
      <details class='qna'><summary>What happens when your name is <em>just</em> Steve?</summary>The field is red as the pattern is expecting a space and at least one letter for a surname.</details>
      <details class='qna'><summary>What happens when your name is Steven?</summary>The field is red as the pattern is expecting a space after "Steve".</details>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Javascript</h1>
      <p>In javascript, the ends of the <strong>regex</strong> are marked by the <code>/</code> character by default and <strong>not</strong> quote characters.</p>
      <p>When testing a string, we create a regex object and call its <code>test</code> method.</p>
      <pre><code>var patt = /^Steve [a-zA-Z ]+$/;
if (patt.<span>test(</span>name<span>)</span>) 
  <span><span>// set friendly message</span></span>
else
  <span><span>// set error message</span></span></code></pre>
      <h3>Javascript Regex Example</h3>
      <p>Full Name: <input type='text' oninput='isSteve(this)' placeholder='Steve {Something}'/> <span class='error' id='steveErrorL7'></span></p>
      <p>We can also replace part of a string with the <strong>string</strong>'s <code>replace</code> method to correct a user's input as they type.</p>
      <pre><code>var patt = /^(Stephen|Steven|Stephan|Stefanie)/;
name.<span>replace(</span>patt,"Steve"<span>)</span>;</code></pre>
      <h3>Javascript Replace Example</h3>
      <p>Full Name: <input type='text' oninput='makeSteve(this)' placeholder='Steve {Something}'/></p>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Start &amp; End Anchors</h1>
      <p>In the previous examples, we can see that the characters:</p>
      <ul>
        <li><code>^</code> matches the <strong>start</strong> of a string.</li>
        <li><code>$</code> matches the <strong>end</strong> of a string.</li>
      </ul>
      <p>In combination, <code>^</code> and <code>$</code> match the <em>whole string</em>.</p>
      <p>Examples:</p>
      <ul>
        <li><code>^Stephen</code> matches <strong>"Stephen Strange"</strong> but not <strong>"Doctor Stephen Strange"</strong>.</li>
        <li><code>^Stephen$</code> matches <strong>"Stephen"</strong> but not <strong>"Stephen Strange"</strong> or <strong>"Doctor Stephen Strange"</strong>.</li>
      </ul>
      <h3><strong>Checkpoint</strong> - Consider Two Strings:</h3>
      <ol>
        <li>"cause the players gonna play, play, play, play, play"</li>
        <li>"and the haters gonna hate, hate, hate, hate, hate"</li>
      </ol>
      <details class='qna'><summary>Does <code>/^cause/</code> match the above strings?</summary>(1) begins with "cause" so there is a match. There is no match for string (2).</details>
      <details class='qna'><summary>Does <code>/y$/</code> match the above strings?</summary>(1) ends with "y" so there is a match. There is no match for string (2).</details>
      <details class='qna'><summary>Does <code>/$e/</code> match the above strings?</summary>No match. At best, it may match a string with the substring <strong>"$e"</strong> somewhere inside.</details>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Character Sets</h1>
      <p>To match one of a set of characters, specify them in square brackets <code>[]</code>.</p>
      <p>Some examples:</p>
      <ul>
        <li><code>[aeiou]</code> matches a single lower-case vowel.</li>
        <li><code>[a-z]</code> matches a single lower-case character between ‘a’ and ‘z’.</li>
        <li><code>[A-Za-z0-9]</code> matches a single alphanumeric character.</li>
        <li><code>[-a-z]</code> matches a single hyphen or lower-case letter.</li>
      </ul>
      <h3><strong>Checkpoint</strong> - Consider Two Strings:</h3>
      <ol>
        <li>"cause the players gonna play, play, play, play, play"</li>
        <li>"and the haters gonna hate, hate, hate, hate, hate"</li>
      </ol>
      <details class='qna'><summary>Does <code>/p[rl]ayer/</code> match the above strings?</summary>(1) contains the word "player" so there is a match. There is no match for string (2), but if a "prayer" was present, there would be a match.</details>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Negated Matching</h1>
      <p>To "not match" or "match not", we re-use the <code>^</code> character inside the square brackets, for example <code>[^0-9]</code> matches a string if no digits are present.</p>
      <p><i>It's unfortunate that the caret character is reused for a different meaning/purpose here. This is what happens when you let geeks code without proper supervision!</i></p>
      <h3><strong>Checkpoint</strong> - Consider Two Strings:</h3>
      <ol>
        <li>"cause the players gonna play, play, play, play, play"</li>
        <li>"and the haters gonna hate, hate, hate, hate, hate"</li>
      </ol>
      <details class='qna'><summary>Does <code>/[^a-z]h/</code> match the above strings?</summary>(2) contains the word "hater" and the letter 'h' is preceded by a space character, so there is a match. There is no match for string (1) as the only letter 'h' is preceded by the letter 't'.</details>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Simple Quantifiers</h1>
      <p>To quantify occurrences, use:</p>
      <ul>
        <li><code>*</code> zero or more</li>
        <li><code>?</code> zero or one</li>
        <li><code>+</code> one or more</li>
      </ul>
      <p>For example:</p>
      <ul>
        <li><code>[aeiou]*</code> matches 0 or more vowels.</li>
        <li><code>[aeiou]?</code> matches 0 or 1 vowels.</li>
        <li><code>[aeiou]+</code> matches 1 or more vowels.</li>
      </ul>
      <details class='qna'><summary>What does <code>/colou?r/</code> match?</summary>This will match any string with the word "<strong>color</strong>" or "<strong>colo<em>u</em>r</strong>" in it, but not "<strong>colo<em>uu</em>r</strong>" or "<strong>colo<em>uuu</em>r</strong>" etc.</details>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Range Quantifiers</h1>
      <p>To quantify custom number or range of occurrences, we use the curly braces <code>{  }</code> and a comma:</p>
      <ul>
        <li><code>{0,}</code> zero or more.</li>
        <li><code>{0,1}</code> zero or one.</li>
        <li><code>{1,}</code> one or more.</li>
        <li><code>{2}</code> two, no more and no less!</li>
        <li><code>{3,4}</code> three or four.</li>
        <li><code>{5,20}</code> between five and twenty.</li>
        <li><code>{100,}</code> 100 or more.</li>
      </ul>
      <h3><strong>Checkpoint</strong> - Consider Two Strings:</h3>
      <ol>
        <li>"cause the players gonna play, play, play, play, play"</li>
        <li>"and the haters gonna hate, hate, hate, hate, hate"</li>
      </ol>
      <details class='qna'><summary>Does <code>/n{2}/</code> match the above strings?</summary>In this example, both (1) and (2) contain the word "<strong>go<em>nn</em>a</strong>" so both strings match.</details>
      <details class='qna'><summary>Does <code>/hate,{3}/</code> match the above strings?</summary>This is a bit of a trick question! (2) contains 5 "hate"s in a row, but no "hate" has 3 commas after it, so the is no match. (1) doesn't match because it does not contain any "hate,,," either.</details>
      <details class='qna'><summary>Does <code>/hated*/</code> match the above strings?</summary>This is also a bit of a trick question! Both (1) and (2) do not contain the word "<strong>hated</strong>" BUT we are looking for <strong>0 or more</strong> occurences of the letter "<strong>d</strong>" that follows the word "<strong>hate</strong>", so (2) matches and (1) does not.</details>
      <details class='qna'><summary>Many words are spelt or spelled differently in USA leaning countries and UK leaning countries. For example, are you <strong>enrolled</strong> or <strong>enroled</strong> in Web Programming? Write a regex that matches both cases.</summary>No answers for this one, this is left to the reader as an exercise ... I"M KIDDING! ... the answer is <code>/enrol{1,2}ed/</code>.</details>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Meta Characters</h1>
      <p>Some characters have special meaning and are called <strong>meta</strong> characters. Some of the following we have seen already: <code>^$*?+-[]{}()|/</code> and one we haven't: <code>.</code> which matches any character.</p>
      <p>To match the literal version of a meta character, we need to escape it first with the escape character: <code>\</code>. <i>If it is not obvious, the backslash character is also a meta character.</i></p>
      <ul>
        <li><code>\**</code> matches the <strong>*</strong> character literally, zero or more times.</li>
        <li><code>\??</code> matches the <strong>?</strong> character literally, zero or one time.</li>
        <li><code>\++</code> matches the <strong>+</strong> character literally, one or more times.</li>
        <li><code>\..</code> matches the <strong>.</strong> character literally, followed by any character.</li>
        <li><code>\\</code> matches the <strong>\</strong> character, literally.</li>
        <li><code>\/</code> matches the <strong>/</strong> character, literally.</li>
      </ul>
      <details class='qna'><summary>How would you match the string "<strong>https://</strong>"?</summary>This one is a bit messy, but necessary, as the <code>/</code> character marks the ends of the regex. You will need to use  <code>/https:\/\//</code></details>
    </section>
  </section>
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Escaping Literal Characters</h1>
      <p>Most characters have no special meaning and are called <strong>literal</strong> characters. For example, <code>/Steve Jobs/</code> matches "<strong>Steve Jobs</strong>" - <i>even the space character is a literal character!</i></p>
      <p>However, when escaped, some literal characters become meta characters and have meaning:</p>
      <ul>
        <li><code>\n</code> matches the <strong>new line</strong> character.</li>
        <li><code>\t</code> matches the <strong>tab</strong> character.</li>
        <li><code>\d</code> matches any digit, <code>\D</code> matches any non-digit.</li>
        <li><code>\w</code> matches any word char, <code>\W</code> matches any non-word char.</li>
        <li><code>\s</code> matches any whitespace character, <code>\S</code> matches any non-whitespace character.</li>
      </ul>
      <h3><strong>Checkpoint</strong> - Consider Two Strings:</h3>
      <ol>
        <li>"cause the players gonna play, play, play, play, play"</li>
        <li>"and the haters gonna hate, hate, hate, hate, hate"</li>
      </ol>
      <details class='qna'><summary>Does <code>/\D{6} /</code> match the above strings?</summary>Both strings match as there are plenty of places where 6 non-digits can be found in a row.</details>
      <details class='qna'><summary>Does <code>/ .h. /</code> match the above strings?</summary>Both strings match as there is one place in each string where a three letter word exists with 'h' in the center? Can you find it? What can <?php a('http://www.thethe.com/','the'); ?> word be?</details>
      <details class='qna'><summary>Does <code>/^[^a-z]*.ause/</code> match the above strings?</summary>This is a bit of a trick question! Initially, we are looking for something at the beginning of a string that isn't a lowercase letter, BUT 0 times is fine, so we can ignore that part. This leaves us to look for a string that begins with any character (eg "c", "😋", anything!) followed by "ause", so string (1) matches and string (2) does not.</details>
    </section>
  </section>  
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Grouping and Branching</h1>
      <p>You can put parts (or atoms) of a pattern in parentheses <code>()</code> to form complete parts of a longer expression.</p>
      <details class='qna'><summary>What will <code>/(very ){1,2}/</code> match?</summary>This will match a string that contains "<strong>very </strong>" and "<strong>very very </strong>".</details>
      <p>The pipe character <code>|</code>  allows choices or options.</p>
      <details class='qna'><summary>What will <code>/\.((com)|(edu)|(org)|(net))$/</code> match?</summary>This will match a string that ends with a ".", followed by <strong>com</strong>", "<strong>edu</strong>", "<strong>org</strong>" OR "<strong>net</strong>".</details>
      <details class='qna'><summary>What will <code>/(fruit|vampire) bat/</code> match?</summary>This will match a string that contains "<strong>fruit bat</strong>", "<strong>vampire bat</strong>" but not "<strong>cricket bat</strong>".</details>
      <p></p>
      <h3>Checkpoint - Consider Two Strings:</h3>
      <ol>
        <li>"cause the players gonna play, play, play, play, play"</li>
        <li>"and the haters gonna hate, hate, hate, hate, hate"</li>
      </ol>
      <details class='qna'><summary>Does <code>/(p|l|a|y){2}/</code> match the above strings?</summary><i>Update: This answer was corrected on 17th June 2017  - Thanks go to Samuel and Shichao for spotting the error.</i> In this example, string (1) passes because there are many places where the letters in the set {p,l,a,y} are followed by another letter in the same set. String (2) fails because there are no places where two letters from the set occur next to each other. If we changed the regex to <code>/(p|l|a|n){2}/</code>, both strings would match because of the "<strong>nn</strong>" in "<strong>go<em>nn</em>a</strong>" and a matching sequece can be found in the word <strong>an</strong>d.</details>
      <details class='qna'><summary>Does <code>/(hate, ){3}/</code> match the above strings?</summary>String (2) will match because of the repeated phrase "<strong>hate, </strong>". String (1) won't match.</details>
      <details class='qna'><summary>Does <code>/(play, ){5}/</code> match the above strings?</summary>This is a bit of a trick question! In this example, (2) fails, but (1) also fails because there are only 4 instances of "<strong>play, </strong>" in a row. To match this string, use <code>/(play, ){4}play/</code>.</details>
    </section>
  </section>    
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Modifiers</h1>
      <p>Regexps can also take <strong>special modifiers</strong> after the closing delimiter.</p>
      <ul>
        <li><code>/[a-z]/<span>i</span></code> makes the search case insensitive.</li>
        <li><code>/[a-z]/<span>x</span></code> ignores whitespace inside the string.</li>
        <li><code>/[a-z]/<span>g</span></code> searches globally, ie it doesn't stop at the first match.</li>
        <li><code>/[a-z]/<span>m</span></code> searches on multiple lines, good for searching documents, not just single lines.</li>
        <li><code>/[a-z]/<span>ixgm</span></code> does all 4!</li>
      </ul>
    </section>
  </section>  
    
  
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Worked Example - Email</h1>
      <p>The following regex is intended to detect an email address:</p>
      <pre><code>/^[a-zA-Z0-9\-_.]+@[a-zA-Z0-9\-.]+$/</code></pre>
      <p><i>Just to let you know, <code>.</code> is a meta character <em>outside</em> square brackets, but <em>inside</em> it does not need to be escaped. Some browsers will fail if you "do the right thing"!</i></p>
      <ul>
        <li><code>^</code> start of string anchor.</li>
        <li><code>[a-zA-Z0-9_\.]+</code> any of these characters inside <strong>[]</strong> one or more times.</li>
        <li><code>@</code> the literal "at" character, once.</li>
        <li><code>[a-zA-Z0-9\-\.]+</code> any of these characters inside <strong>[]</strong> one or more times.</li>
        <li><code>$</code> end of string anchor.</li>
      </ul>
      <details class='qna'><summary>How good is this regex? Any problems?</summary>This regex is not bad, but it will allow "<strong>.@.</strong>" for example. Many good, many bad, and the length is not always a good indicator of robustness. Make sure that your chosen regex does not block people, especially those that work in museums: <?php a('http://archives.icom.museum/musedoma/naming.html'); ?>.</details>
    </section>
  </section>  

  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Look Ahead</h1>
      <p>Regexs can also "look ahead" and the meta-sequence <code>?=</code> is used to do this.</p>
      <p>Normally a regex search moves the search position forwards <strong>one way</strong>, capturing characters as it goes; but a look ahead allows the search position to return for a second or third search with no character capturing.</p>
      <ul>
        <li><code>x(<span>?=</span>y)</code> matches "<strong>x</strong>", but only if it is followed by "<strong>y</strong>". It does not capture "<strong>xy</strong>".</li>
        <li><code>/be(<span>?=</span> not)/</code> matches "<strong>be</strong>" but only the ones that are followed by "<strong> not</strong>". It does not capture "<strong>be not</strong>".</li>
      </ul>
      <p>To perform a password strength check in a single regex, we can make use of look ahead. The following regex is used to check for a mix of character sets and a minimum string length.</p>
      <pre><code>/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(.{8,})$/</code></pre>
      <p>Password: <input type='text' oninput='pwStr(this)' placeholder='Must be good!'/> <span class='error' id='pwErrorL7'></span></p>
      <details class='qna'>
        <summary>Can you work out what it is and how it works?</summary>
        <p>The first three terms are look aheads and return to the start of the string. We look for <strong>anything</strong> 0 or more times, followed by ...</p>
        <ol>
          <li><code>(?=.*\d)</code> a digit somewhere</li>
          <li><code>(?=.*[A-Z])</code> an uppercase char somewhere</li>
          <li><code>(?=.*[a-z])</code> a lowercase char somewhere</li>
        </ol>
        <p>If any of these look aheads fail, the regex match fails BUT if all three pass, a final whole string check is made: <code>(.{8,})</code> looks for 8 or more characters.</p>
      </details>
    </section>
  </section>  

    
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>POSIX Classes</h1>
      <p>There are some predefined classes (called POSIX classes) that can be convenient (and sometimes make code more clear!).</p>
      <p><em><strong>NB:</strong> They are not supported in Javascript, but are supported in PHP!</em></p>
      
      <ul>
        <li><code>[[:digit:]]</code> digit character</li>
        <li><code>[[:upper:]]</code> uppercase character</li>
        <li><code>[[:lower:]]</code> uppercase character</li>
        <li><code>[[:alnum:]]</code> alpha-numeric character</li>
        <li><code>[[:punct:]]</code> punctuation character</li>
        <li><code>[[:space:]]</code> whitespace character (space, tab, line-break, …)</li>
      </ul>
      <details class='qna'>
        <summary>Can you write a regex that makes sure a string (ie a password) contains at least one uppercase char, one lowercase char, one punctuation character and one digit?</summary>
        <p>This is a complicated task for a <strong>single</strong> regex as we will have to "look ahead" for each case. An easier solution is to test the string 4 times (ie use 4 regexes) and if any fail, then the whole test fails.</p>
        <p>To get you started, <code>/[[:punct:]]]/</code> will match a string with at least one digit character BUT ... javascript does not support POSIX so you will have to smash out the keyboard ... and make sure you escape the meta characters ... erm ... no solution, this is left to the reader as an exercise <i>- damn you javascript!</i></p>
      </details>
    </section>
  </section>  
    
  <section>
    <header>Regular Expressions<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Resources</h1>
      <p>Tests and explains how your regex works:<br><?php a('https://regex101.com/'); ?></p>
      <p>A concise single page summary:<br><?php a('http://www.cheatography.com/davechild/cheat-sheets/regular-expressions/'); ?></p>
      <p>W3Schools:<br><?php a('http://www.w3schools.com/jsref/jsref_regexp_test.asp'); ?><br><?php a('http://www.w3schools.com/tags/att_input_pattern.asp'); ?></p>
    </section>
  </section>  

  <section class='title'>
    <header>Lecture 7.2<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <p>Part 2: Arrays, Objects, Web Storage</p>
    </section>
  </section>

  <section class='key'>
    <header>Javascript - Client Side Programming<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <ol>
        <li><a href='#tp-jsarray1d'>Indexed Arrays (1D)</a></li>
        <li><a href='#tp-jsarray2d'>Indexed Arrays (2D)</a></li>
        <li><a href='#tp-jsobjasoc'>Objects - Associative Arrays</a></li>
        <li><a href='#tp-jswebstor'>HTML5 Web Storage</a></li>
        <li><a href='#tp-jslocstex'>Example of localStorage</a></li>
      </ol>
    </section>
  </section>  
  
  <section class='key' id='#tp-jsarray1d'>
    <header>Indexed Arrays (1D)<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Simple Indexed Arrays</h1>
      <p>Simple indexed arrays are good for storing data, for example a simple shopping list.</p>
      <ol start="0" class='parch'>
        <li>Milk</li>
        <li>Bread</li>
        <li>Chicken</li>
        <li>Peas</li>
        <li>Chocolate</li>
        <li><i>more</i>&nbsp;Chocolate</li>
      </ol>
      <p>They are indexed numerically, starting at element 0.</p>
      <pre><code>var shopList = <span>[
  "Milk", 
  "Bread",
  "Chicken",
  "Peas",
  "Chocolate",
  "more Chocolate"
]</span>;</code></pre>
      <p><i>This code can be written on one line!</i></p>
    </section>
  </section>
  
  <section>
    <header>Arrays (1D)<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Accessing and Iterating</h1>
      <p>Individual elements can be accessed using their index:</p>
      <pre><code>var fifthElement = shopList[<span>4</span>]; <span><span>// Chocolate is the fifth element!</span></span>
shopList[<span>3</span>] = "Spam";  <span><span>// who needs peas anyway?</span></span></code></pre>
      <p>To get the length of an indexed array, you can access its <code>length</code> property.</p>
      <pre><code>var howLong = shopList<span>.length</span>; <span> <span>// ie 6</span></span></code></pre>
      <p>To iterate over an array, a for loop is often used. For example, to create an ordered list from an array, use the following code.</p>
      <pre><code>document.write('&lt;ol&gt;');
for (var i=0; i&lt;<span>shopList.length</span>; i++)
{
  document.write('&lt;li&gt;'+<span>shopList[i]</span>+'&lt;/li&gt;');
}
document.write('&lt;/ol&gt;');</code></pre>
    </section>
  </section>  
  
  <section class='key' id='#tp-jsarray1d'>
    <header>Indexed Arrays (2D)<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Table-like Arrays</h1>
      <p>Sometimes a single dimension (simple list) is not sufficient enough to store data <i>- if it were, we would not need spreadsheets!</i></p>
      <p>A spreadsheet is a good example of a 2 dimensional array. It has data that is split into cells in rows and columns and makes for a more useful shopping experience.</p>
      <table>
        <tr>
          <th>Product</th><th>Qty</th><th>Unit Price</th><th>Price</th>
        </tr>
        <tr>
          <td>Milk</td><td>1</td><td>2.00</td><td>2.00</td>
        </tr>
        <tr>
          <td>Bread</td><td>1</td><td>3.00</td><td>3.00</td>
        </tr>
        <tr>
          <td>Chicken</td><td>1</td><td>7.00</td><td>7.00</td>
        </tr>
        <tr>
          <td>Peas</td><td>500</td><td>0.01</td><td>5.00</td>
        </tr>
        <tr>
          <td>Chocolate</td><td>7</td><td>4.00</td><td>28.00</td><strong><em><i></i></em></strong>
        </tr>
      </table>
      <p>A 2D array is really a 1D array where each element is array. With that in mind, this is how create a new 2D array:</p>
      <pre><code>var shopTable = [
  <span>["Product","Qty","Unit Price","Price"]</span>,
  <span>["Milk", "1", "2.00", "2.00"]</span>,
  <span>["Bread", "1", "3.00", "3.00"]</span>,
  <span>["Chicken", "1", "7.00", "7.00"]</span>,
  <span>["Peas", "500", "0.01", "5.00"]</span>,
  <span>["Chocolate", "7", "4.00", "28.00"]</span> 
];</code></pre>
    </section>
  </section>  
  
  <section>
    <header>Indexed Arrays (2D)<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Accessing and Iterating</h1>
      <p>Individual elements in a 2D indexed array can be accessed using <strong>two</strong> indexes (first <strong>row</strong>, then <strong>column</strong>):</p>
      <pre><code>var milkPrice = shopList[<span>1</span>][<span>3</span>]; <span><span>// second row, last column</span></span>
shopList[<span>3</span>][<span>0</span>] = "Spam"; <span><span>// Don't feel like chicken tonight</span></span></code></pre>
      <p>To get the number of rows, we need to access the length property of the array.</p>
      <pre><code>var numRows = shopTable<span>.length</span><span> <span>// ie 6</span></span></code></pre>
      <p>To get the number of columns (assuming all sub-arrays are the same size), we need to access the length property of one of the sub arrays (eg the first one).</p>
      <pre><code>var numCols = shopTable<span>[0].length</span><span> <span>// ie 4</span></span></code></pre>
      <p>To iterate over a 2D array, two for loops are often used. For example, to create a table from the 2D array, use the following code.</p>
      <pre><code>document.write('&lt;table&gt;');
for (var row=0; row&lt;<span>shopTable.length</span>; row++)
{
  document.write('&lt;tr&gt;');
  for (var col=0; col&lt;<span>shopTable[row].length</span>; col++)
  {
    document.write('&lt;td&gt;'+<span>shopTable[row][col]</span>+'&lt;/td&gt;');
  }
  document.write(&lt;/tr&gt;');
}
document.write('&lt;/table&gt;');</code></pre>
    </section>
  </section>  
  
  <section class='key' id='tp-jsobjasoc'>
    <header>Objects - Associative Arrays<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Rethinking the Indexing Method</h1>
      <p>While numbered indexes are convenient, they are also limiting in some situations. In the previous example, it is not clear what the data in each cell of the array relates to.</p>
      <pre><code>var peaQty = shopTable[<span>4</span>][<span>1</span>] <span><span>// not very intuitive ...</span></span></code></pre>
      <p>How much nicer would it be if we could use just ask the array for what we wanted?</p>
      <pre><code>var peaQty = shopTable[<span>'Pea'</span>][<span>'Qty'</span>] <span><span>// this would be cool!</span></span></code></pre>
      <p>Objects allow us to use <strong>named keys</strong> in place of a <strong>numeric index</strong>. An object can therefore be used as an associative array.</p>
    </section>
  </section>  
  
  <section>
    <header>Objects - Associative Arrays<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Creating an Object</h1>
      <p>When creating objects, we use <strong>JSON</strong> (<em>J</em>ava<em>s</em>cript <em>O</em>bject <em>N</em>otation). We need to bring in the colon <code>:</code> character to seperate a <strong>key</strong> from its <strong>value</strong>, and for an array of key / values pairs we use braces <code>{}</code> instead of square brackets <code>[]</code>.</p>
      <pre><code>var shopObject = {
   <span>Milk</span>: {
    <span>Qty</span>: "1",
    <span>UnitPrice</span>: "2.00",
    <span>Price</span>: "2.00"   
   },
   <span>Bread</span>: {
    <span>Qty</span>: "1",
    <span>UnitPrice</span>: "3.00",
    <span>Price</span>: "3.00"   
   }, 
   
   <span><span>//... repeat for Chicken and Peas ...</span></span>
   
   <span>Chocolate</span>: {
    <span>Qty</span>: "7",
    <span>UnitPrice</span>: "4.00",
    <span>Price</span>: "28.00"   
   }
};</code></pre>
      <p>Both of the following statements update the object / associative array:</p>
<pre><code>shopObject['<span>Bread</span>']['<span>Qty</span>'] = "2"; <span><span>// Associative array syntax</span></span>
shopObject.<span>Bread</span>.<span>Qty</span> = "2"; <span><span>// Object syntax</span></span></code></pre>
      <p>And we can add new items if we wish:</p>
<pre><code>shopObject['<span>Icecream</span>']['<span>Qty</span>'] = "2"; <span><span>// Associative array syntax</span></span>
shopObject.<span>Icecream</span>.<span>Qty</span> = "2"; <span><span>// Object syntax</span></span></code></pre>
    </section>
  </section>  
  
  <section>
    <header>Objects - Associative Arrays<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>For-In Loop</h1>
      <p>A standard for loop can iterate over a numeric indexed array, but how do you iterate over an associative array?</p>
      <pre><code>for (var row="<span>Milk</span>"; row&lt;="<span>Chocolate</span>" ; row++) { 
  for (var col='<span>Qty</span>'; col&lt;='<span>Price</span>' ; col++) {
    document.write(shopObject[<span>row</span>][<span>col</span>]);  
  }
}
<span><span>// Trust me, the computer is totally confused!</span></span></code></pre>
      <p>Javascript provides with a <code>for in</code> loop to handle objects. The <strong>iterator</strong> is automatically assigned the <strong>key</strong> on each loop:</p>
      <pre><code>for (var <span>row</span> in shopObject) { 
  for (var <span>col</span> in shopObject[<span>row</span>]) {
    document.write(shopObject[<span>row</span>][<span>col</span>]);  
  }
}
<span><span>// Everything works!</span></span></code></pre>
<p><i>We are no longer constrained by integer keys, starting at 0!</i></p>
    </section>
  </section>  
  
  <section>
    <header>Objects - Associative Arrays<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Mixing Indexed Arrays and Objects</h1>
      <p>In truth, there is still a need for indexed arrays, for example a <strong>shopping receipt</strong> is an <strong>indexed</strong> array and contains <strong>objects</strong> as array elements.</p>
      <pre><code>var shopReceipt = [
  {
    <span>Product: "Milk"</span>,
    Qty: "1",
    UnitPrice: "2.00",
    Price: "2.00"   
  },
  {
    <span>Product: "Chocolate"</span>, 
    Qty: "20",
    UnitPrice: "4.00",
    Price: "80.00"   
  } 
];
<span><span>// Not the healthiest shop visit ...</span></span></code></pre>
      <p>In this example, the <strong>product</strong> is no longer the <strong>row</strong> key, but rather part of each receipt item (ie another <strong>column</strong>). The following <strong>definition list</strong> writes out each purchased item in a <code>&lt;dt&gt;</code> element and each of the properties in <code>&lt;dd&gt;</code> elements.</p>
      <pre><code>document.write('&lt;dl&gt;');
for (var <span>row=0</span>; row&lt;shopReceipt.length; row++) 
{
  document.write('&lt;dt&gt;Item #'+(<span>row+1</span>)+':&lt;/dt&gt;');
  for (var <span>col</span> in shopReceipt[<span>row</span>])
  {
    document.write('&lt;dd&gt;'+<span>col</span>+': '+shopReceipt[<span>row</span>][<span>col</span>]+'&lt;dd&gt;');
  }
}
document.write('&lt;/dl&gt;');</code></pre>
    </section>
  </section>
  
  <section class='key' id='#tp=jswebstor'>
    <header>HTML5 Web Storage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Storing Data Client Side</h1>
      <p>HTTP is a stateless protocol. This means that once a client has sent a request to a server, and the server has responded by sending the requested information, the connection is terminated, and the server forgets everything about the client/user.</p>
      <p>However, it can be vital to maintain state for interactive web applications. We will now look at <strong>client side</strong> storage, ie what is stored in your web browser.</p>
      <p>In Chrome, view the developer console and navigate to Application. You should see this:</p>
      <p><img src='app/img/client-storage.png' alt='screenshot of application area in console' /></p>
    </section>
  </section>
  
  <section>
    <header>HTML5 Web Storage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Cookies</h1>
      <p>Cookies are small pieces of information that are stored on a client's machine, to store information about the user. A cookie is a <strong>name - value</strong> pair, such as:</p>
      <pre><code><span>name</span> = "<span>Steve Gates</span>"</code></pre>
      <p>By default cookies are deleted when the browser is closed, but they can also be given a set expiry time (persistent cookies).</p>
      <p>Javascript has always been able to set cookies on the client's machine to remember information about each particular user, but HTML5 saw an upgrade to the size and security of client side storage, called <strong>Web Storage</strong>, and is a better way of storing information client side.</p>
      <p><i>We will look more at cookies in lecture 12</i></p>
    </section>
  </section>
  
  <section>
    <header>HTML5 Web Storage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Web Storage</h1>
      <p>There are two types of Web Storage objects:</p>
      <ul>
        <li><code>sessionStorage</code> is <em>temporarily</em> stored on the client's machine and is automatically erased when the user leaves your website. <i>For <cite>Pixar fans</cite>, remember how Dory's memory keeps getting wiped every time she swims away from Marlin near the start of <cite>Finding Nemo</cite>.</i></li>
        <li><code>localStorage</code> is stored <em>persistently</em> on the client's machine and can be retrieved when the use re-visits your website at a later date. <i>For <cite>Pixar fans</cite> fans, remember when Dory has a "total recall" moment when she meets Nemo near the end of <cite>Finding Nemo</cite>.</i></li>
      </ul>
      <p>In all cases (cookies, local and session), client side information is stored in the form of keys and values.</p>
      <ul>
        <li>Cookies are <em>always</em> sent to the server, whether the server needs them or not.</li>
        <li>Web storage data is <em>only</em> sent to the server programmatically, i.e. when the server needs it.</li>
      </ul>
    </section>
  </section>
  
  <section>
    <header>HTML5 Web Storage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Setting a key / value pair</h1>
      <p>Use the <code>setItem()</code> method to set web storage data, for example a <em>persistent</em> name, phone number, and <em>today's</em> coffee order:</p>
      <pre><code>if (typeof(<span>Storage</span>) !== '<span>undefined</span>') {
  localStorage.setItem('<span>name</span>','<span>Alice Liddell</span>');
  localStorage.setItem('<span>phone</span>','<span>0400 000 000</span>');
  sessionStorage.setItem('<span>coffee</span>','<span>Latte</span>');
} else {
  <span><span>// Sorry - you'll have to code using cookies!</span></span>
}</code></pre>
      <blockquote>Please Note: All values stored in cookies and web storage are in <em>string</em> format.<br>You must convert variable, eg numbers and booleans, when storing or retrieving them.</blockquote>
    </section>
  </section>
  
  <section>
    <header>HTML5 Web Storage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Getting a key / value pair</h1>
      <p>Use the <code>getItem()</code> method to retrieve Web Storage data, for example a <em>persistent</em> name, phone number, and <em>today's</em> coffee order:</p>
      <pre><code>var name, phone, coffee;
if (typeof(<span>Storage</span>) !== '<span>undefined</span>') {
  <span>name</span> = localStorage.getItem('<span>name</span>');
  <span>phone</span> = localStorage.getItem('<span>phone</span>');
  <span>coffee</span> = sessionStorage.getItem('<span>coffee</span>');
} else {
  <span><span>// Sorry - you'll have to code using cookies!</span></span>
}</code></pre>
    </section>
  </section>

  <section>
    <header>HTML5 Web Storage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Deleting Web Storage Data</h1>
      <p>Use the removeItem() method to unset Web Storage data:</p>
      <pre><code>var name, phone, coffee;
if (typeof(<span>Storage</span>) !== '<span>undefined</span>') {
  localStorage.removeItem('<span>name</span>');
}</code></pre>
      <p>Or reset all data:</p>
      <pre><code>localStorage.clear(); <span><span>// just localStorage</span></span>
sessionStorage.clear(); <span><span>// just sessionStorage</span></span>
Storage.clear(); <span><span>// both!</span></span></code></pre>
    </section>
  </section>
  
    <section>
    <header>HTML5 Web Storage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Alternative Notation</h1>
      <p>The two web storage objects can be set and accessed using array and object notation:</p>
      <pre><code>localStorage.<span>name</span> = "Bob D'Builder";
localStorage['<span>name</span>'] = "Bob D'Builder";

var coffee = sessionStorage.<span>coffee</span>;
var coffee = sessionStorage['<span>coffee</span>'];</code></pre>
    </section>
  </section>
  
  <section class='key' id='tp-jslocstex'>
    <header>Example of localStorage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>What's on Your Machine?</h1>
      <p>These lecture slides use <strong>localStorage</strong> to remember which slide sets you have open.</p>
      <p>At the moment, this website has stored the following keys and values in your <strong>localStorage</strong> object:</p>
      <script>
        document.write("<table><tr><th>Key</th><th>Value</th></tr>");
        for (var lsItem in localStorage)
          document.write("<tr><th>"+lsItem+"</th><td>"+localStorage[lsItem]+"</td></tr>"); 
        document.write("</table>");
      </script>
    </section>
  </section>

  <section>
    <header>Example of localStorage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Slide Interaction</h1>
      <p>When you click "Instructions" in the top menu, an item in localStorage called <code>slides-instructions</code> is either set to either "false" or "true":</p>
      <pre><code>if (document.getElementById('<span>instructions</span>').checked)
{
  localStorage['<span>slides-instructions</span>']='<span>true</span>';
  <span><span>// code to make instructions visible</span></span>
} else {
  localStorage['<span>slides-instructions</span>']='<span>false</span>';
  <span><span>// code to make instructions <em>in</em>visible</span></span>  
}</code></pre>
      <p>In a similar fashion, when you return to the website after an absence, localStorage is checked and the instructions checkbox is either set or unset to the same state that you left it in:</p>
      <pre><code>if (localStorage['<span>slides-instructions</span>']=='<span>false</span>') {
  document.getElementById('<span>instructions</span>').checked=<span>false</span>;
  <span><span>// code to make instructions <em>in</em>visible</span></span>  
} else { 
  document.getElementById('<span>instructions</span>').checked=<span>true</span>;
  <span><span>// code to make instructions visible</span></span>
}</code></pre>
    </section>
  </section>
  
  <section>
    <header>Example of localStorage<span><?php print $slideText.($counter++); ?></span></header>
    <section>
      <h1>Remember Me!</h1>
      <p>Fields can be pre-filled, for example in the case of a "remember me" type of application:</p>
      <pre><code>if (localStorage.<span>remember</span> == '<span>true</span>') {
  document.getElementById('<span>name</span>').value = localStorage.<span>name</span>;
  document.getElementById('<span>phone</span>').value = localStorage.<span>phone</span>;
}</code></pre>
      <p>The user can be forgotten if the box is unchecked:</p>
      <pre><code>if (document.getElementById('<span>remember</span>').checked !== <span>true</span>) {
  localStorage.removeItem('<span>name</span>');
  localStorage.removeItem('<span>phone</span>');
}
<span><span>// or localStorage.clear(); if not storing anything else in localStorage</span></span></code></pre>
    </section>
  </section>  
  
</article>